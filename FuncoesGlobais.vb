﻿Imports Npgsql
Imports System.Data

Module FuncoesGlobais
    Public Sub PreencheCBO(ByVal vP_Query As String, ByVal vP_DispMember As String, ByVal vP_Table As String, _
                                ByVal vP_VleMember As String, ByRef vP_CBO As ComboBox, ByRef vP_Form As Form)
        Dim vL_Conn As New NpgsqlConnection()

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
            End With

            Dim vL_DataAdp As New NpgsqlDataAdapter(vP_Query, vL_Conn)

            Dim vL_DataSet As New DataSet

            vL_DataAdp.Fill(vL_DataSet, vP_Table)


            With vP_CBO
                .DataSource = vL_DataSet.Tables(0)
                .DisplayMember = vP_DispMember
                .ValueMember = vP_VleMember
            End With

        Catch ex As Exception
            MessageBox.Show(ex.Message, vP_Form.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Sub

    Public Sub PreencheCBO_UF(ByRef vP_CBO As ComboBox, ByRef vP_Form As Form)
        Try
            With vP_CBO
                .Items.Clear()
                .Items.Add("AC")
                .Items.Add("RR")
                .Items.Add("RO")
                .Items.Add("PA")
                .Items.Add("AM")
                .Items.Add("TO")
                .Items.Add("MA")
                .Items.Add("PI")
                .Items.Add("CE")
                .Items.Add("BA")
                .Items.Add("RN")
                .Items.Add("PB")
                .Items.Add("PE")
                .Items.Add("AL")
                .Items.Add("SE")
                .Items.Add("DF")
                .Items.Add("MG")
                .Items.Add("SP")
                .Items.Add("RJ")
                .Items.Add("MS")
                .Items.Add("MT")
                .Items.Add("GO")
                .Items.Add("PR")
                .Items.Add("SC")
                .Items.Add("RS")
                .Sorted = True
            End With

        Catch ex As Exception
            MessageBox.Show(ex.Message, vP_Form.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Public Sub PreencheGrid(ByVal vP_Table As String, ByVal vP_Query As String, ByRef vP_Form As Form, ByRef vP_Grid As DataGridView)
        Dim vL_Conn As New NpgsqlConnection()

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
            End With

            Dim vL_DataAdp As New NpgsqlDataAdapter(vP_Query, vL_Conn)

            Dim vL_DataSet As New DataSet

            vL_DataAdp.Fill(vL_DataSet, vP_Table)


            With vP_Grid
                .AutoGenerateColumns = False

                .DataSource = vL_DataSet.Tables(0)

                .ReadOnly = True
            End With

        Catch ex As Exception
            MessageBox.Show(ex.Message, vP_Form.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Sub

    Public Sub MascaraValor(ByRef sender As Object)
        If sender.Text = Nothing Then
            Exit Sub
        End If

        sender.Text = Format(CDbl(sender.Text), "standard")
    End Sub

    Public Sub ValidarValor(ByRef e As KeyPressEventArgs)
        If (Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 59) Then
            If Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 44 Then
                Exit Sub
            End If

            e.KeyChar = ""
        End If
    End Sub
End Module
