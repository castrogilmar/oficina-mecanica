﻿Imports Npgsql
Imports System.Data

Public Class FConsultaProdutos
    Private Sub BUT_NOVO_PRODUTO_Click(sender As Object, e As EventArgs) Handles BUT_NOVO_PRODUTO.Click
        FCadastroProduto.ShowDialog()
        FCadastroProduto.Dispose()
    End Sub

    Private Sub FConsultaProduto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CONSULTA DE PRODUTOS E SERVIÇOS"

        PreencheCBO("select txt_grupo from oficina.tb_grupo " + " order by txt_grupo ", _
                    "Grupo", "tb_grupo", "txt_grupo", CBO_GRUPO, Me)
    End Sub

    Private Sub BUT_CONSULTA_Click(sender As Object, e As EventArgs) Handles BUT_CONSULTA.Click
        If CBO_GRUPO.Text = Nothing And TXT_DESCRICAO.Text = Nothing Then

            MessageBox.Show("Informe parâmetros para a pesquisa!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            CBO_GRUPO.Focus()
            Exit Sub
        End If

        PreencheGrid("tb_produto", "select a.cod_produto, b.txt_grupo , a.txt_produto, a.tpo_Produto, " + _
                     "coalesce(d.qt_estoque,0) vlr_estoque_atual, a.vlr_avista, vlr_custo, vlr_aprazo, txt_fantasia txt_fornecedor " + _
                     "from oficina.tb_produto a join oficina.tb_grupo b on (a.cod_grupo = b.cod_grupo) " + _
                     "join oficina.tb_fornecedor c on (a.cod_fornecedor = c.cod_fornecedor) " + _
                     "left join oficina.tb_estoque d on (a.cod_produto = d.cod_produto) " + _
                     IIf(TXT_DESCRICAO.Text.Trim <> Nothing, " where txt_produto = '" + TXT_DESCRICAO.Text.Trim + "'", ""), Me, DTG_PRODUTOS)
    End Sub

    Private Sub CBO_GRUPO_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBO_GRUPO.SelectedIndexChanged

    End Sub

    Private Sub DTG_PRODUTOS_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DTG_PRODUTOS.CellClick
        If e.ColumnIndex = DTG_PRODUTOS.Columns("cod_produto").Index And Not IsDBNull(DTG_PRODUTOS.CurrentRow.Cells("cod_produto").Value) Then
            'MessageBox.Show(DTG_GRUPOS.CurrentRow.Cells("cod_grupo").Value)

            FCadastroProduto.TXT_COD_PRODUTO.Text = DTG_PRODUTOS.CurrentRow.Cells("cod_produto").Value.ToString
            FCadastroProduto.TXT_DESCRICAO.Text = DTG_PRODUTOS.CurrentRow.Cells("txt_produto").Value
            FCadastroProduto.TXT_APRAZO.Text = DTG_PRODUTOS.CurrentRow.Cells("vlr_aprazo").Value
            FCadastroProduto.TXT_AVISTA.Text = DTG_PRODUTOS.CurrentRow.Cells("vlr_avista").Value
            FCadastroProduto.TXT_CUSTO.Text = DTG_PRODUTOS.CurrentRow.Cells("vlr_custo").Value


            FCadastroProduto.RDO_PRODUTO.Checked = IIf(DTG_PRODUTOS.CurrentRow.Cells("tpo_produto").Value = "P", True, False)
            FCadastroProduto.RDO_SERVICO.Checked = IIf(DTG_PRODUTOS.CurrentRow.Cells("tpo_produto").Value <> "P", True, False)
            FCadastroProduto.RDO_PRODUTO.Enabled = False
            FCadastroProduto.RDO_SERVICO.Enabled = False
            FCadastroProduto.BUT_EXCLUIR.Enabled = True

            PreencheCBO("select txt_grupo from oficina.tb_grupo " + "" + " where tpo_produto='" + IIf(FCadastroProduto.RDO_PRODUTO.Checked, "P", "S") + "'" + " order by txt_grupo ", _
                    "Grupo", "tb_grupo", "txt_grupo", FCadastroProduto.CBO_GRUPO, FCadastroProduto)

            PreencheCBO("select txt_fantasia from oficina.tb_fornecedor " + " order by txt_fantasia ", _
                    "Fornecedor", "tb_fornecedor", "txt_fantasia", FCadastroProduto.CBO_FORNECEDOR, FCadastroProduto)
            
            FCadastroProduto.CBO_GRUPO.SelectedIndex = FCadastroProduto.CBO_GRUPO.FindString(DTG_PRODUTOS.CurrentRow.Cells("txt_grupo").Value.ToString.Trim)
            FCadastroProduto.ShowDialog()
            FCadastroProduto.Dispose()
        End If
    End Sub

   
End Class
