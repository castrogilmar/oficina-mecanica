﻿Imports Npgsql

Public Class FCadastroSitOrdem
    Private Sub ManutencaoSituacaoOrdemServico()
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction
        Dim vL_DataRead As NpgsqlDataReader

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            With vL_Cmd
                .Connection = vL_Conn
                .CommandType = CommandType.Text
                .CommandText = "select cod_sitordem from oficina.tb_sitordem where txt_sitordem = '" + TXT_DESCRICAO.Text.Trim + "'"
                vL_DataRead = .ExecuteReader
            End With

            If vL_DataRead.HasRows Then
                MessageBox.Show("A Situação da Ordem de Serviço informada já foi cadastrada anteriormente!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                vL_Conn.Close()
                Exit Sub
            End If

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = IIf(TXT_COD_SITUACAO.Text <> Nothing, "update oficina.tb_sitordem set txt_sitordem ='" + TXT_DESCRICAO.Text.Trim +
                                        "' where cod_sitordem=" + TXT_COD_SITUACAO.Text.Trim + " RETURNING cod_sitordem",
                                       "insert into oficina.tb_sitordem (txt_sitordem) values('" + TXT_DESCRICAO.Text.Trim + "','" + "') RETURNING cod_sitordem")
                    vL_DataRead = .ExecuteReader()
                End With

                vL_Trans.Commit()
                vL_DataRead.Read()

                MessageBox.Show("Código da Situação de Ordem: " + vL_DataRead.Item("cod_sitordem").ToString.Trim, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Catch ex As Exception
                vL_Trans.Rollback()
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Sub

    Private Sub FCadastroSitOrdem_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CADASTRO SITUAÇÃO DA ORDEM DE SERVIÇO "
    End Sub

    Private Sub BUT_SALVAR_Click(sender As Object, e As EventArgs) Handles BUT_SALVAR.Click
        If TXT_DESCRICAO.Text = Nothing Then
            MessageBox.Show("Favor, informar descrição da Situação da Ordem de Serviço!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TXT_DESCRICAO.Focus()
            Exit Sub
        End If

        If MessageBox.Show("Confirma manutenção no cadastro da situação da Ordem de Serviço?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        ManutencaoSituacaoOrdemServico()
    End Sub
End Class