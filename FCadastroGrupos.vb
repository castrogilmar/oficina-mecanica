﻿Imports Npgsql
Imports System.Data

Public Class FCadastroGrupos
    Private Sub ManutencaoGrupo()
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction
        Dim vL_DataRead As NpgsqlDataReader

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            With vL_Cmd
                .Connection = vL_Conn
                .CommandType = CommandType.Text
                .CommandText = "select cod_grupo from oficina.tb_grupo where txt_grupo = '" + TXT_DESCRICAO.Text.Trim + "'"
                vL_DataRead = .ExecuteReader
            End With

            If vL_DataRead.HasRows Then
                MessageBox.Show("O grupo informada já foi cadastrada anteriormente!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                vL_Conn.Close()
                Exit Sub
            End If

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = IIf(TXT_COD_GRUPO.Text <> Nothing, "update oficina.tb_grupo set txt_grupo ='" + TXT_DESCRICAO.Text.Trim + "', tpo_produto='" + _
                                       IIf(RDO_PRODUTO.Checked, "P", "S") + "' where cod_grupo=" + TXT_COD_GRUPO.Text.Trim + " RETURNING cod_grupo", _
                                       "insert into oficina.tb_grupo (txt_grupo, tpo_produto) values('" + TXT_DESCRICAO.Text.Trim + "','" + _
                                   IIf(RDO_PRODUTO.Checked, "P", "S") + "') RETURNING cod_grupo")
                    vL_DataRead = .ExecuteReader()
                End With

                vL_Trans.Commit()
                vL_DataRead.Read()

                MessageBox.Show("Código do grupo: " + vL_DataRead.Item("cod_grupo").ToString.Trim, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Catch ex As Exception
                vL_Trans.Rollback()
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Sub

    Private Sub FCdastroGrupos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CADASTRO DE GRUPOS"
    End Sub

    Private Sub BUT_SALVAR_Click(sender As Object, e As EventArgs) Handles BUT_SALVAR.Click
        If TXT_DESCRICAO.Text = Nothing Then
            MessageBox.Show("Favor, informar descrição do grupo!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TXT_DESCRICAO.Focus()
            Exit Sub
        End If

        If MessageBox.Show("Confirma manutenção no cadastro do grupo?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        ManutencaoGrupo()
    End Sub

    Private Sub TXT_DESCRICAO_TextChanged(sender As Object, e As EventArgs) Handles TXT_DESCRICAO.TextChanged

    End Sub

    Private Sub TXT_COD_GRUPO_TextChanged(sender As Object, e As EventArgs) Handles TXT_COD_GRUPO.TextChanged

    End Sub
End Class