﻿Imports Npgsql
Imports System.Data

Public Class FConsultaGrupos
    Private Sub FConsultaGrupos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CONSULTA DE GRUPOS"
    End Sub

    Private Sub BUT_NOVO_GRUPO_Click(sender As Object, e As EventArgs) Handles BUT_NOVO_GRUPO.Click
        FCadastroGrupos.ShowDialog()
        FCadastroGrupos.Dispose()
    End Sub

    Private Sub BUT_CONSULTA_Click(sender As Object, e As EventArgs) Handles BUT_CONSULTA.Click
        PreencheGrid("tb_grupo", "select cod_grupo , txt_grupo, (case when tpo_produto = 'P' then 'PRODUTO' else 'SERVICO' end)::text tpo_grupo from oficina.tb_grupo " + _
                                                    IIf(TXT_DESCRICAO.Text.Trim <> Nothing, " where txt_grupo = '" + TXT_DESCRICAO.Text.Trim + "'", ""), Me, DTG_GRUPOS)
    End Sub

    Private Sub DTG_GRUPOS_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DTG_GRUPOS.CellClick
        If e.ColumnIndex = DTG_GRUPOS.Columns("cod_grupo").Index And Not IsDBNull(DTG_GRUPOS.CurrentRow.Cells("cod_grupo").Value) Then
            'MessageBox.Show(DTG_GRUPOS.CurrentRow.Cells("cod_grupo").Value)

            FCadastroGrupos.TXT_COD_GRUPO.Text = DTG_GRUPOS.CurrentRow.Cells("cod_grupo").Value.ToString
            FCadastroGrupos.TXT_DESCRICAO.Text = DTG_GRUPOS.CurrentRow.Cells("txt_grupo").Value

            FCadastroGrupos.RDO_PRODUTO.Checked = IIf(DTG_GRUPOS.CurrentRow.Cells("tpo_grupo").Value = "PRODUTO", True, False)
            FCadastroGrupos.RDO_SERVICO.Checked = IIf(DTG_GRUPOS.CurrentRow.Cells("tpo_grupo").Value <> "PRODUTO", True, False)
            FCadastroGrupos.ShowDialog()
            FCadastroGrupos.Dispose()
        End If
    End Sub

    Private Sub DTG_GRUPOS_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DTG_GRUPOS.CellContentClick

    End Sub
End Class