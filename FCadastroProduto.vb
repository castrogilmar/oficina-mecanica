﻿Imports Npgsql
Imports System.Data

Public Class FCadastroProduto

    Private Function ManutencaoProduto() As Boolean
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction
        Dim vL_DataRead As NpgsqlDataReader

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = "select * from oficina.fct_manutencao_produto(" + _
                                                 IIf(TXT_COD_PRODUTO.Text = Nothing, "0", TXT_COD_PRODUTO.Text) + ", " + _
                                                 "'" + IIf(RDO_PRODUTO.Checked, "P", "S") + "', " + _
                                                 "'" + CBO_GRUPO.Text + "', " + _
                                                 "'" + TXT_DESCRICAO.Text + "', " + _
                                                 Replace(Replace(TXT_APRAZO.Text, ".", ""), ",", ".") + ", " + _
                                                 Replace(Replace(TXT_AVISTA.Text, ".", ""), ",", ".") + ", " + _
                                                 Replace(Replace(TXT_CUSTO.Text, ".", ""), ",", ".") + ", " + _
                                                 "'" + CBO_FORNECEDOR.Text + "')"
                    vL_DataRead = .ExecuteReader()
                End With

                vL_Trans.Commit()
                vL_DataRead.Read()

                MessageBox.Show(vL_DataRead.Item(0).ToString.Trim, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                ManutencaoProduto = True
            Catch ex As Exception
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                vL_Trans.Rollback()
                ManutencaoProduto = False
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            ManutencaoProduto = False
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Function

    Private Function RemoveProduto() As Boolean
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction
        'Dim vL_DataRead As NpgsqlDataReader

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = "delete from oficina.tb_produto where cod_produto =" + TXT_COD_PRODUTO.Text
                    'vL_DataRead = .ExecuteReader()
                    .ExecuteNonQuery()
                End With

                vL_Trans.Commit()
                'vL_DataRead.Read()

                MessageBox.Show("Exclusão realizada com sucesso!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                RemoveProduto = True
            Catch ex As Exception
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                vL_Trans.Rollback()
                RemoveProduto = False
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            RemoveProduto = False
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Function

    Private Sub FCadastroProduto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CADASTRO DE PRODUTOS E SERVIÇOS"

        TXT_ESTOQUE_ATUAL.Enabled = False

        If TXT_COD_PRODUTO.Text = Nothing Then
            TXT_ESTOQUE_ATUAL.Text = "0"
        End If
    End Sub

    Private Sub AtualizaCBO() Handles RDO_PRODUTO.Click, RDO_SERVICO.Click
        PreencheCBO("select txt_grupo from oficina.tb_grupo " + "" + " where tpo_produto='" + IIf(RDO_PRODUTO.Checked, "P", "S") + "'" + " order by txt_grupo ", _
                    "Grupo", "tb_grupo", "txt_grupo", CBO_GRUPO, Me)

        If TXT_COD_PRODUTO.Text = Nothing Then
            PreencheCBO("select txt_fantasia from oficina.tb_fornecedor " + " order by txt_fantasia ", _
                    "Fornecedor", "tb_fornecedor", "txt_fantasia", CBO_FORNECEDOR, Me)
        End If
    End Sub

    Private Sub TXT_ESTOQUE_MIN_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TXT_ESTOQUE_MIN.KeyPress
        ValidarValor(e)
    End Sub

    Private Sub TXT_CUSTO_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TXT_CUSTO.KeyPress
        ValidarValor(e)
    End Sub

    Private Sub TXT_AVISTA_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TXT_AVISTA.KeyPress
        ValidarValor(e)
    End Sub

    Private Sub TXT_APRAZO_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TXT_APRAZO.KeyPress
        ValidarValor(e)
    End Sub

    Private Sub TXT_ESTOQUE_MIN_LostFocus(sender As Object, e As EventArgs) Handles TXT_ESTOQUE_MIN.LostFocus
        MascaraValor(sender)
    End Sub

    Private Sub TXT_CUSTO_LostFocus(sender As Object, e As EventArgs) Handles TXT_CUSTO.LostFocus
        MascaraValor(sender)
    End Sub

    Private Sub TXT_AVISTA_LostFocus(sender As Object, e As EventArgs) Handles TXT_AVISTA.LostFocus
        MascaraValor(sender)
    End Sub

    Private Sub TXT_APRAZO_LostFocus(sender As Object, e As EventArgs) Handles TXT_APRAZO.LostFocus
        MascaraValor(sender)
    End Sub

    Private Sub TXT_ESTOQUE_ATUAL_LostFocus(sender As Object, e As EventArgs) Handles TXT_ESTOQUE_ATUAL.LostFocus
        MascaraValor(sender)
    End Sub

    Private Sub BUT_SALVAR_Click(sender As Object, e As EventArgs) Handles BUT_SALVAR.Click
        If CBO_GRUPO.Text = Nothing Then
            MessageBox.Show("Informe o grupo do produto!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            CBO_GRUPO.Focus()
            Exit Sub
        End If

        If TXT_DESCRICAO.Text = Nothing Then
            MessageBox.Show("Informe a descrição do produto!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TXT_DESCRICAO.Focus()
            Exit Sub
        End If

        If TXT_ESTOQUE_MIN.Text = Nothing Then
            MessageBox.Show("Informe o estoque minimo do produto!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TXT_ESTOQUE_MIN.Focus()
            Exit Sub
        End If

        If TXT_CUSTO.Text = Nothing Then
            MessageBox.Show("Informe o valor de custo do produto!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TXT_CUSTO.Focus()
            Exit Sub
        End If

        If TXT_APRAZO.Text = Nothing Then
            MessageBox.Show("Informe o valor de venda a prazo do produto!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TXT_APRAZO.Focus()
            Exit Sub
        End If

        If TXT_AVISTA.Text = Nothing Then
            MessageBox.Show("Informe o valor de venda a vista do produto!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TXT_AVISTA.Focus()
            Exit Sub
        End If

        If CBO_FORNECEDOR.Text = Nothing Then
            MessageBox.Show("Informe o fornecedor do produto!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            CBO_FORNECEDOR.Focus()
            Exit Sub
        End If

        If MessageBox.Show("Confirma manutenção no produto?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        If ManutencaoProduto() = True Then
            Me.Close()
        End If
    End Sub

    Private Sub BUT_EXCLUIR_Click(sender As Object, e As EventArgs) Handles BUT_EXCLUIR.Click
        If RemoveProduto() = True Then
            Me.Close()
        End If
    End Sub
End Class