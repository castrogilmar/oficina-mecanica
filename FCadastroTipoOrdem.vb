﻿Imports Npgsql

Public Class FCadastroTipoOrdem
    Private Sub ManutencaoTipoOrdemServico()
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction
        Dim vL_DataRead As NpgsqlDataReader

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            With vL_Cmd
                .Connection = vL_Conn
                .CommandType = CommandType.Text
                .CommandText = "select cod_tipoordem from oficina.tb_tipoordem where txt_tipoordem = '" + TXT_DESCRICAO.Text.Trim + "'"
                vL_DataRead = .ExecuteReader
            End With

            If vL_DataRead.HasRows Then
                MessageBox.Show("O Tipo de Ordem de Serviço informada já foi cadastrada anteriormente!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                vL_Conn.Close()
                Exit Sub
            End If

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = IIf(TXT_COD_SITUACAO.Text <> Nothing, "update oficina.tb_tipoordem set txt_tipoordem ='" + TXT_DESCRICAO.Text.Trim +
                                        "' where cod_tipoordem=" + TXT_COD_SITUACAO.Text.Trim + " RETURNING cod_tipoordem",
                                       "insert into oficina.tb_tipoordem (txt_tipoordem) values('" + TXT_DESCRICAO.Text.Trim + "','" + "') RETURNING cod_tipoordem")
                    vL_DataRead = .ExecuteReader()
                End With

                vL_Trans.Commit()
                vL_DataRead.Read()

                MessageBox.Show("Código do Tipo de Ordem: " + vL_DataRead.Item("cod_tipoordem").ToString.Trim, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Catch ex As Exception
                vL_Trans.Rollback()
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Sub
    Private Sub FCadastroTipoOrdem_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CADASTRO TIPO DE ORDEM DE SERVIÇO "
    End Sub

    Private Sub TXT_COD_SITUACAO_TextChanged(sender As Object, e As EventArgs) Handles TXT_COD_SITUACAO.TextChanged

    End Sub

    Private Sub BUT_SALVAR_Click(sender As Object, e As EventArgs) Handles BUT_SALVAR.Click
        If TXT_DESCRICAO.Text = Nothing Then
            MessageBox.Show("Favor, informar descrição do Tipo de Ordem de Serviço!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TXT_DESCRICAO.Focus()
            Exit Sub
        End If

        If MessageBox.Show("Confirma manutenção no cadastro do Tipo de Ordem de Serviço?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        ManutencaoTipoOrdemServico()
    End Sub
End Class