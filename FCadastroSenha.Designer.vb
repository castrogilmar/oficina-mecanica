﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FCadastroSenha
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FCadastroSenha))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CBO_CLASSE_SENHA = New System.Windows.Forms.ComboBox()
        Me.DT_CADASTRAMENTO = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TXT_COD_FUNCIONARIO = New System.Windows.Forms.TextBox()
        Me.TXT_COD_SENHA = New System.Windows.Forms.TextBox()
        Me.BUT_SALVAR = New System.Windows.Forms.Button()
        Me.BUT_EXCLUIR = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.RDO_SERVICO = New System.Windows.Forms.RadioButton()
        Me.RDO_PRODUTO = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TXT_SENHA2 = New System.Windows.Forms.TextBox()
        Me.TXT_SENHA = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CBO_CLASSE_SENHA)
        Me.GroupBox1.Controls.Add(Me.DT_CADASTRAMENTO)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TXT_COD_FUNCIONARIO)
        Me.GroupBox1.Controls.Add(Me.TXT_COD_SENHA)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(700, 127)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'CBO_CLASSE_SENHA
        '
        Me.CBO_CLASSE_SENHA.BackColor = System.Drawing.SystemColors.Info
        Me.CBO_CLASSE_SENHA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBO_CLASSE_SENHA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBO_CLASSE_SENHA.FormattingEnabled = True
        Me.CBO_CLASSE_SENHA.Location = New System.Drawing.Point(167, 35)
        Me.CBO_CLASSE_SENHA.Name = "CBO_CLASSE_SENHA"
        Me.CBO_CLASSE_SENHA.Size = New System.Drawing.Size(97, 21)
        Me.CBO_CLASSE_SENHA.TabIndex = 51
        '
        'DT_CADASTRAMENTO
        '
        Me.DT_CADASTRAMENTO.CalendarMonthBackground = System.Drawing.SystemColors.Info
        Me.DT_CADASTRAMENTO.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DT_CADASTRAMENTO.Location = New System.Drawing.Point(343, 35)
        Me.DT_CADASTRAMENTO.Name = "DT_CADASTRAMENTO"
        Me.DT_CADASTRAMENTO.Size = New System.Drawing.Size(97, 20)
        Me.DT_CADASTRAMENTO.TabIndex = 49
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(340, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(94, 15)
        Me.Label11.TabIndex = 50
        Me.Label11.Text = "Cadastrado em:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(114, 15)
        Me.Label2.TabIndex = 46
        Me.Label2.Text = "Código Funcionário"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(164, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 15)
        Me.Label3.TabIndex = 46
        Me.Label3.Text = "Classe da Senha"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 15)
        Me.Label1.TabIndex = 46
        Me.Label1.Text = "Código Senha"
        '
        'TXT_COD_FUNCIONARIO
        '
        Me.TXT_COD_FUNCIONARIO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_COD_FUNCIONARIO.Enabled = False
        Me.TXT_COD_FUNCIONARIO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_COD_FUNCIONARIO.Location = New System.Drawing.Point(2, 92)
        Me.TXT_COD_FUNCIONARIO.Name = "TXT_COD_FUNCIONARIO"
        Me.TXT_COD_FUNCIONARIO.Size = New System.Drawing.Size(118, 22)
        Me.TXT_COD_FUNCIONARIO.TabIndex = 45
        '
        'TXT_COD_SENHA
        '
        Me.TXT_COD_SENHA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_COD_SENHA.Enabled = False
        Me.TXT_COD_SENHA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_COD_SENHA.Location = New System.Drawing.Point(2, 33)
        Me.TXT_COD_SENHA.Name = "TXT_COD_SENHA"
        Me.TXT_COD_SENHA.Size = New System.Drawing.Size(118, 22)
        Me.TXT_COD_SENHA.TabIndex = 45
        '
        'BUT_SALVAR
        '
        Me.BUT_SALVAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_SALVAR.Image = Global.oficina_mecanica.My.Resources.Resources.Hopstarter_Sleek_Xp_Basic_Ok
        Me.BUT_SALVAR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_SALVAR.Location = New System.Drawing.Point(270, 268)
        Me.BUT_SALVAR.Name = "BUT_SALVAR"
        Me.BUT_SALVAR.Size = New System.Drawing.Size(88, 46)
        Me.BUT_SALVAR.TabIndex = 17
        Me.BUT_SALVAR.Text = "SALVAR"
        Me.BUT_SALVAR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_SALVAR.UseVisualStyleBackColor = True
        '
        'BUT_EXCLUIR
        '
        Me.BUT_EXCLUIR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_EXCLUIR.Image = CType(resources.GetObject("BUT_EXCLUIR.Image"), System.Drawing.Image)
        Me.BUT_EXCLUIR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_EXCLUIR.Location = New System.Drawing.Point(364, 268)
        Me.BUT_EXCLUIR.Name = "BUT_EXCLUIR"
        Me.BUT_EXCLUIR.Size = New System.Drawing.Size(91, 46)
        Me.BUT_EXCLUIR.TabIndex = 18
        Me.BUT_EXCLUIR.Text = "EXCLUIR"
        Me.BUT_EXCLUIR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_EXCLUIR.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.RDO_SERVICO)
        Me.GroupBox3.Controls.Add(Me.RDO_PRODUTO)
        Me.GroupBox3.Location = New System.Drawing.Point(343, 19)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(117, 43)
        Me.GroupBox3.TabIndex = 19
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Ativo"
        '
        'RDO_SERVICO
        '
        Me.RDO_SERVICO.AutoSize = True
        Me.RDO_SERVICO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RDO_SERVICO.Location = New System.Drawing.Point(61, 19)
        Me.RDO_SERVICO.Name = "RDO_SERVICO"
        Me.RDO_SERVICO.Size = New System.Drawing.Size(48, 19)
        Me.RDO_SERVICO.TabIndex = 3
        Me.RDO_SERVICO.Text = "Não"
        Me.RDO_SERVICO.UseVisualStyleBackColor = True
        '
        'RDO_PRODUTO
        '
        Me.RDO_PRODUTO.AutoSize = True
        Me.RDO_PRODUTO.Checked = True
        Me.RDO_PRODUTO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RDO_PRODUTO.Location = New System.Drawing.Point(6, 18)
        Me.RDO_PRODUTO.Name = "RDO_PRODUTO"
        Me.RDO_PRODUTO.Size = New System.Drawing.Size(47, 19)
        Me.RDO_PRODUTO.TabIndex = 3
        Me.RDO_PRODUTO.TabStop = True
        Me.RDO_PRODUTO.Text = "Sim"
        Me.RDO_PRODUTO.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TXT_SENHA2)
        Me.GroupBox2.Controls.Add(Me.TXT_SENHA)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 145)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(700, 117)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        '
        'TXT_SENHA2
        '
        Me.TXT_SENHA2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.TXT_SENHA2.Location = New System.Drawing.Point(4, 79)
        Me.TXT_SENHA2.Name = "TXT_SENHA2"
        Me.TXT_SENHA2.Size = New System.Drawing.Size(116, 22)
        Me.TXT_SENHA2.TabIndex = 47
        '
        'TXT_SENHA
        '
        Me.TXT_SENHA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.TXT_SENHA.Location = New System.Drawing.Point(3, 36)
        Me.TXT_SENHA.Name = "TXT_SENHA"
        Me.TXT_SENHA.Size = New System.Drawing.Size(117, 22)
        Me.TXT_SENHA.TabIndex = 47
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 60)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 15)
        Me.Label5.TabIndex = 46
        Me.Label5.Text = "Confirmar Senha"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(10, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 15)
        Me.Label4.TabIndex = 46
        Me.Label4.Text = "Digitar Senha"
        '
        'FCadastroSenha
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(724, 326)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.BUT_SALVAR)
        Me.Controls.Add(Me.BUT_EXCLUIR)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FCadastroSenha"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FCadastroSenha"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents TXT_COD_FUNCIONARIO As TextBox
    Friend WithEvents TXT_COD_SENHA As TextBox
    Friend WithEvents DT_CADASTRAMENTO As DateTimePicker
    Friend WithEvents Label11 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents CBO_CLASSE_SENHA As ComboBox
    Friend WithEvents BUT_SALVAR As Button
    Friend WithEvents BUT_EXCLUIR As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents RDO_SERVICO As RadioButton
    Friend WithEvents RDO_PRODUTO As RadioButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents TXT_SENHA2 As TextBox
    Friend WithEvents TXT_SENHA As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
End Class
