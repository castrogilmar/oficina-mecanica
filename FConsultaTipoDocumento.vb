﻿Public Class FConsultaTipoDocumento

    Private Sub FConsultaTipoDocumento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CONSULTA DE TIPOS DE DOCUMENTO"

        PreencheCBO("select txt_tipo from (select txt_tipo from oficina.tb_tipo_cliente " + " union " + _
                    "select 'TODOS'::varchar txt_tipo) a  order by txt_tipo", _
                    "Tipo", "tb_tipo_cliente", "txt_tipo", CBO_TIPO_CLIENTE, Me)

    End Sub

    Private Sub BUT_CONSULTA_Click(sender As Object, e As EventArgs) Handles BUT_CONSULTA.Click
        PreencheGrid("tb_tipo_documento", "select a.cod_documento, a.txt_documento, b.txt_tipo tpo_cliente from oficina.tb_tipo_documento a, oficina.tb_tipo_cliente b " + _
                     " where a.tpo_cliente = b.cod_tipo" + IIf(TXT_DESCRICAO.Text <> Nothing, " and txt_documento like '" + TXT_DESCRICAO.Text.Trim + "%' ", "") + _
                     IIf(CBO_TIPO_CLIENTE.Text <> "TODOS", " and tpo_cliente in (select cod_tipo from oficina.tb_tipo_cliente where txt_tipo = '" + CBO_TIPO_CLIENTE.Text.Trim + "') ", ""), Me, DTG_TIPOS_DOCUMENTOS)

    End Sub

    Private Sub BUT_NOVO_Click(sender As Object, e As EventArgs) Handles BUT_NOVO.Click
        FCadastroTipoDocumento.ShowDialog()
        FCadastroTipoDocumento.Dispose()
    End Sub

    Private Sub DTG_TIPOS_DOCUMENTOS_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DTG_TIPOS_DOCUMENTOS.CellClick
        If e.ColumnIndex = DTG_TIPOS_DOCUMENTOS.Columns("cod_documento").Index And Not IsDBNull(DTG_TIPOS_DOCUMENTOS.CurrentRow.Cells("cod_documento").Value) Then
            'MessageBox.Show(DTG_GRUPOS.CurrentRow.Cells("cod_grupo").Value)

            FCadastroTipoDocumento.TXT_COD_DOCUMENTO.Text = DTG_TIPOS_DOCUMENTOS.CurrentRow.Cells("cod_documento").Value.ToString
            FCadastroTipoDocumento.TXT_DESCRICAO.Text = DTG_TIPOS_DOCUMENTOS.CurrentRow.Cells("txt_documento").Value

            PreencheCBO("select txt_tipo from oficina.tb_tipo_cliente " + " order by txt_tipo ", _
                    "Tipo", "tb_tipo_cliente", "txt_tipo", FCadastroTipoDocumento.CBO_TIPO_CLIENTE, FCadastroTipoDocumento)

            FCadastroTipoDocumento.CBO_TIPO_CLIENTE.SelectedIndex = _
                FCadastroTipoDocumento.CBO_TIPO_CLIENTE.FindString(DTG_TIPOS_DOCUMENTOS.CurrentRow.Cells("tpo_cliente").Value)

            FCadastroTipoDocumento.CBO_TIPO_CLIENTE.Enabled = False

            FCadastroTipoDocumento.ShowDialog()
            FCadastroTipoDocumento.Dispose()
        End If
    End Sub
    
End Class