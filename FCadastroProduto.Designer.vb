﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FCadastroProduto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FCadastroProduto))
        Me.BUT_SALVAR = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TXT_APRAZO = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TXT_AVISTA = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TXT_CUSTO = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TXT_ESTOQUE_ATUAL = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TXT_ESTOQUE_MIN = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TXT_DESCRICAO = New System.Windows.Forms.TextBox()
        Me.RDO_SERVICO = New System.Windows.Forms.RadioButton()
        Me.RDO_PRODUTO = New System.Windows.Forms.RadioButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CBO_GRUPO = New System.Windows.Forms.ComboBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TXT_COD_PRODUTO = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BUT_EXCLUIR = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.CBO_FORNECEDOR = New System.Windows.Forms.ComboBox()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'BUT_SALVAR
        '
        Me.BUT_SALVAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_SALVAR.Image = Global.oficina_mecanica.My.Resources.Resources.Hopstarter_Sleek_Xp_Basic_Ok
        Me.BUT_SALVAR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_SALVAR.Location = New System.Drawing.Point(225, 338)
        Me.BUT_SALVAR.Name = "BUT_SALVAR"
        Me.BUT_SALVAR.Size = New System.Drawing.Size(88, 46)
        Me.BUT_SALVAR.TabIndex = 9
        Me.BUT_SALVAR.Text = "SALVAR"
        Me.BUT_SALVAR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_SALVAR.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(226, 17)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(49, 15)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Á Prazo"
        '
        'TXT_APRAZO
        '
        Me.TXT_APRAZO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_APRAZO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_APRAZO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_APRAZO.Location = New System.Drawing.Point(226, 35)
        Me.TXT_APRAZO.Name = "TXT_APRAZO"
        Me.TXT_APRAZO.Size = New System.Drawing.Size(100, 22)
        Me.TXT_APRAZO.TabIndex = 8
        Me.TXT_APRAZO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(120, 17)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 15)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Á Vista"
        '
        'TXT_AVISTA
        '
        Me.TXT_AVISTA.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_AVISTA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_AVISTA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_AVISTA.Location = New System.Drawing.Point(120, 35)
        Me.TXT_AVISTA.Name = "TXT_AVISTA"
        Me.TXT_AVISTA.Size = New System.Drawing.Size(100, 22)
        Me.TXT_AVISTA.TabIndex = 7
        Me.TXT_AVISTA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(14, 17)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 15)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Custo"
        '
        'TXT_CUSTO
        '
        Me.TXT_CUSTO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_CUSTO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_CUSTO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_CUSTO.Location = New System.Drawing.Point(14, 35)
        Me.TXT_CUSTO.Name = "TXT_CUSTO"
        Me.TXT_CUSTO.Size = New System.Drawing.Size(100, 22)
        Me.TXT_CUSTO.TabIndex = 6
        Me.TXT_CUSTO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label8)
        Me.GroupBox4.Controls.Add(Me.TXT_APRAZO)
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.TXT_AVISTA)
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.TXT_CUSTO)
        Me.GroupBox4.Location = New System.Drawing.Point(132, 260)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(498, 66)
        Me.GroupBox4.TabIndex = 6
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Valores"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.TXT_ESTOQUE_ATUAL)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.TXT_ESTOQUE_MIN)
        Me.GroupBox3.Location = New System.Drawing.Point(4, 260)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(122, 112)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Estoque"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(13, 60)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(34, 15)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Atual"
        '
        'TXT_ESTOQUE_ATUAL
        '
        Me.TXT_ESTOQUE_ATUAL.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_ESTOQUE_ATUAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_ESTOQUE_ATUAL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_ESTOQUE_ATUAL.Location = New System.Drawing.Point(10, 78)
        Me.TXT_ESTOQUE_ATUAL.Name = "TXT_ESTOQUE_ATUAL"
        Me.TXT_ESTOQUE_ATUAL.Size = New System.Drawing.Size(100, 22)
        Me.TXT_ESTOQUE_ATUAL.TabIndex = 5
        Me.TXT_ESTOQUE_ATUAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(13, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 15)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Mínimo"
        '
        'TXT_ESTOQUE_MIN
        '
        Me.TXT_ESTOQUE_MIN.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_ESTOQUE_MIN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_ESTOQUE_MIN.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_ESTOQUE_MIN.Location = New System.Drawing.Point(10, 35)
        Me.TXT_ESTOQUE_MIN.Name = "TXT_ESTOQUE_MIN"
        Me.TXT_ESTOQUE_MIN.Size = New System.Drawing.Size(100, 22)
        Me.TXT_ESTOQUE_MIN.TabIndex = 4
        Me.TXT_ESTOQUE_MIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(7, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Código"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(4, 164)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(174, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Descrição do Produto / Serviço"
        '
        'TXT_DESCRICAO
        '
        Me.TXT_DESCRICAO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_DESCRICAO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_DESCRICAO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_DESCRICAO.Location = New System.Drawing.Point(7, 182)
        Me.TXT_DESCRICAO.Name = "TXT_DESCRICAO"
        Me.TXT_DESCRICAO.Size = New System.Drawing.Size(611, 22)
        Me.TXT_DESCRICAO.TabIndex = 3
        '
        'RDO_SERVICO
        '
        Me.RDO_SERVICO.AutoSize = True
        Me.RDO_SERVICO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RDO_SERVICO.Location = New System.Drawing.Point(80, 18)
        Me.RDO_SERVICO.Name = "RDO_SERVICO"
        Me.RDO_SERVICO.Size = New System.Drawing.Size(65, 19)
        Me.RDO_SERVICO.TabIndex = 1
        Me.RDO_SERVICO.Text = "Serviço"
        Me.RDO_SERVICO.UseVisualStyleBackColor = True
        '
        'RDO_PRODUTO
        '
        Me.RDO_PRODUTO.AutoSize = True
        Me.RDO_PRODUTO.Checked = True
        Me.RDO_PRODUTO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RDO_PRODUTO.Location = New System.Drawing.Point(6, 18)
        Me.RDO_PRODUTO.Name = "RDO_PRODUTO"
        Me.RDO_PRODUTO.Size = New System.Drawing.Size(68, 19)
        Me.RDO_PRODUTO.TabIndex = 1
        Me.RDO_PRODUTO.TabStop = True
        Me.RDO_PRODUTO.Text = "Produto"
        Me.RDO_PRODUTO.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(172, 65)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(116, 15)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Descrição do Grupo"
        '
        'CBO_GRUPO
        '
        Me.CBO_GRUPO.BackColor = System.Drawing.SystemColors.Info
        Me.CBO_GRUPO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBO_GRUPO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBO_GRUPO.FormattingEnabled = True
        Me.CBO_GRUPO.Location = New System.Drawing.Point(175, 83)
        Me.CBO_GRUPO.Name = "CBO_GRUPO"
        Me.CBO_GRUPO.Size = New System.Drawing.Size(442, 21)
        Me.CBO_GRUPO.TabIndex = 2
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RDO_SERVICO)
        Me.GroupBox2.Controls.Add(Me.RDO_PRODUTO)
        Me.GroupBox2.Location = New System.Drawing.Point(10, 65)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(150, 43)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Tipo Cadastro"
        '
        'TXT_COD_PRODUTO
        '
        Me.TXT_COD_PRODUTO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_COD_PRODUTO.Enabled = False
        Me.TXT_COD_PRODUTO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_COD_PRODUTO.Location = New System.Drawing.Point(10, 37)
        Me.TXT_COD_PRODUTO.Name = "TXT_COD_PRODUTO"
        Me.TXT_COD_PRODUTO.Size = New System.Drawing.Size(100, 22)
        Me.TXT_COD_PRODUTO.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.CBO_FORNECEDOR)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.CBO_GRUPO)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TXT_DESCRICAO)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TXT_COD_PRODUTO)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 11)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(626, 216)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'BUT_EXCLUIR
        '
        Me.BUT_EXCLUIR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_EXCLUIR.Enabled = False
        Me.BUT_EXCLUIR.Image = CType(resources.GetObject("BUT_EXCLUIR.Image"), System.Drawing.Image)
        Me.BUT_EXCLUIR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_EXCLUIR.Location = New System.Drawing.Point(319, 338)
        Me.BUT_EXCLUIR.Name = "BUT_EXCLUIR"
        Me.BUT_EXCLUIR.Size = New System.Drawing.Size(91, 46)
        Me.BUT_EXCLUIR.TabIndex = 10
        Me.BUT_EXCLUIR.Text = "EXCLUIR"
        Me.BUT_EXCLUIR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_EXCLUIR.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(173, 118)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(70, 15)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Fornecedor"
        '
        'CBO_FORNECEDOR
        '
        Me.CBO_FORNECEDOR.BackColor = System.Drawing.SystemColors.Info
        Me.CBO_FORNECEDOR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBO_FORNECEDOR.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBO_FORNECEDOR.FormattingEnabled = True
        Me.CBO_FORNECEDOR.Location = New System.Drawing.Point(176, 136)
        Me.CBO_FORNECEDOR.Name = "CBO_FORNECEDOR"
        Me.CBO_FORNECEDOR.Size = New System.Drawing.Size(442, 21)
        Me.CBO_FORNECEDOR.TabIndex = 3
        '
        'FCadastroProduto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(638, 396)
        Me.Controls.Add(Me.BUT_SALVAR)
        Me.Controls.Add(Me.BUT_EXCLUIR)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FCadastroProduto"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FCadastroProduto"
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BUT_SALVAR As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TXT_APRAZO As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TXT_AVISTA As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TXT_CUSTO As System.Windows.Forms.TextBox
    Friend WithEvents BUT_EXCLUIR As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TXT_ESTOQUE_ATUAL As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TXT_ESTOQUE_MIN As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TXT_DESCRICAO As System.Windows.Forms.TextBox
    Friend WithEvents RDO_SERVICO As System.Windows.Forms.RadioButton
    Friend WithEvents RDO_PRODUTO As System.Windows.Forms.RadioButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CBO_GRUPO As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TXT_COD_PRODUTO As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents CBO_FORNECEDOR As System.Windows.Forms.ComboBox
End Class
