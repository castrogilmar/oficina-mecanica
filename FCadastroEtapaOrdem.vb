﻿Imports Npgsql
Imports System.Data

Public Class FCadastroEtapaOrdem
    Private Sub ManutencaoTpoCliente()
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction
        Dim vL_DataRead As NpgsqlDataReader

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            With vL_Cmd
                .Connection = vL_Conn
                .CommandType = CommandType.Text
                .CommandText = "select cod_Etapa from oficina.tb_etapa_ordem where txt_etapa = '" + TXT_DESCRICAO.Text.Trim + "'"
                vL_DataRead = .ExecuteReader
            End With

            If vL_DataRead.HasRows Then
                MessageBox.Show("A Etapa informado já foi cadastrado anteriormente!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                vL_Conn.Close()
                Exit Sub
            End If

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = IIf(TXT_COD_ETAPA.Text <> Nothing, "update oficina.tb_etapa_ordem set txt_etapa ='" + TXT_DESCRICAO.Text.Trim +
                                       "' where cod_etapa=" + TXT_COD_ETAPA.Text.Trim + " RETURNING cod_etapa",
                                       "insert into oficina.tb_etapa_ordem (txt_etapa) values('" + TXT_DESCRICAO.Text.Trim + "') " +
                                       " RETURNING cod_etapa")
                    vL_DataRead = .ExecuteReader()
                End With

                vL_Trans.Commit()
                vL_DataRead.Read()

                MessageBox.Show("Manutenção realizada com sucesso. Código da etapa: " + vL_DataRead.Item("cod_etapa").ToString.Trim, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Catch ex As Exception
                vL_Trans.Rollback()
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Sub

    Private Function ExclusaoEtapaOrdem() As Boolean
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = "delete from oficina.tb_etapa_ordem where cod_etapa=" + TXT_COD_ETAPA.Text.Trim + "::integer"
                    .ExecuteNonQuery()
                End With

                vL_Trans.Commit()

                MessageBox.Show("Etapa excluído com sucesso!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                ExclusaoEtapaOrdem = True
            Catch ex As Exception
                vL_Trans.Rollback()
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                ExclusaoEtapaOrdem = False
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            ExclusaoEtapaOrdem = False
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Function
    Private Sub FCadastroEtapaOrdem_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CADASTRO DA ETAPA DA ORDEM DE SERVIÇO "

        If TXT_COD_ETAPA.Text = Nothing Then
            BUT_EXCLUIR.Enabled = False
        End If

    End Sub

    Private Sub BUT_SALVAR_Click(sender As Object, e As EventArgs) Handles BUT_SALVAR.Click
        If TXT_DESCRICAO.Text = Nothing Then
            MessageBox.Show("Descrição da Etapa de Ordem de Serviço não informada!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        ManutencaoTpoCliente()
    End Sub

    Private Sub BUT_EXCLUIR_Click(sender As Object, e As EventArgs) Handles BUT_EXCLUIR.Click
        If MessageBox.Show("Deseja realmente excluir a Etapa de Ordem de Serviço?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        If ExclusaoEtapaOrdem() Then
            Me.Close()
        End If
    End Sub
End Class
