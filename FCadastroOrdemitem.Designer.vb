﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FCadastroOrdemitem
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FCadastroOrdemitem))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TXT_COD_ORDEMSERVICO = New System.Windows.Forms.TextBox()
        Me.TXT_DT_ORDEMSERVICO = New System.Windows.Forms.TextBox()
        Me.TXT_FL_SITUACAO = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.CBO_TXT_SERVICO = New System.Windows.Forms.ComboBox()
        Me.BUT_SALVAR = New System.Windows.Forms.Button()
        Me.BUT_EXCLUIR = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label1.Location = New System.Drawing.Point(9, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 18)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Ordem Serviço"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label2.Location = New System.Drawing.Point(152, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 18)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Data "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label3.Location = New System.Drawing.Point(255, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 18)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Situação"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label4.Location = New System.Drawing.Point(17, 60)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 18)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Serviço"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label5.Location = New System.Drawing.Point(468, 60)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 18)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Quant."
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label6.Location = New System.Drawing.Point(535, 60)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(76, 18)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Valor Unit."
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label7.Location = New System.Drawing.Point(671, 60)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 18)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Total"
        '
        'TXT_COD_ORDEMSERVICO
        '
        Me.TXT_COD_ORDEMSERVICO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_COD_ORDEMSERVICO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_COD_ORDEMSERVICO.Enabled = False
        Me.TXT_COD_ORDEMSERVICO.Location = New System.Drawing.Point(12, 34)
        Me.TXT_COD_ORDEMSERVICO.Name = "TXT_COD_ORDEMSERVICO"
        Me.TXT_COD_ORDEMSERVICO.Size = New System.Drawing.Size(100, 22)
        Me.TXT_COD_ORDEMSERVICO.TabIndex = 7
        '
        'TXT_DT_ORDEMSERVICO
        '
        Me.TXT_DT_ORDEMSERVICO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_DT_ORDEMSERVICO.Enabled = False
        Me.TXT_DT_ORDEMSERVICO.Location = New System.Drawing.Point(127, 34)
        Me.TXT_DT_ORDEMSERVICO.Name = "TXT_DT_ORDEMSERVICO"
        Me.TXT_DT_ORDEMSERVICO.Size = New System.Drawing.Size(100, 22)
        Me.TXT_DT_ORDEMSERVICO.TabIndex = 8
        '
        'TXT_FL_SITUACAO
        '
        Me.TXT_FL_SITUACAO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_FL_SITUACAO.Enabled = False
        Me.TXT_FL_SITUACAO.Location = New System.Drawing.Point(244, 34)
        Me.TXT_FL_SITUACAO.Name = "TXT_FL_SITUACAO"
        Me.TXT_FL_SITUACAO.Size = New System.Drawing.Size(100, 22)
        Me.TXT_FL_SITUACAO.TabIndex = 9
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(463, 91)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(66, 22)
        Me.TextBox5.TabIndex = 11
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(538, 91)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(100, 22)
        Me.TextBox6.TabIndex = 12
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(648, 91)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(100, 22)
        Me.TextBox7.TabIndex = 13
        '
        'CBO_TXT_SERVICO
        '
        Me.CBO_TXT_SERVICO.FormattingEnabled = True
        Me.CBO_TXT_SERVICO.Location = New System.Drawing.Point(12, 91)
        Me.CBO_TXT_SERVICO.Name = "CBO_TXT_SERVICO"
        Me.CBO_TXT_SERVICO.Size = New System.Drawing.Size(445, 24)
        Me.CBO_TXT_SERVICO.TabIndex = 14
        '
        'BUT_SALVAR
        '
        Me.BUT_SALVAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_SALVAR.Image = Global.oficina_mecanica.My.Resources.Resources.Hopstarter_Sleek_Xp_Basic_Ok
        Me.BUT_SALVAR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_SALVAR.Location = New System.Drawing.Point(266, 135)
        Me.BUT_SALVAR.Margin = New System.Windows.Forms.Padding(4)
        Me.BUT_SALVAR.Name = "BUT_SALVAR"
        Me.BUT_SALVAR.Size = New System.Drawing.Size(117, 57)
        Me.BUT_SALVAR.TabIndex = 17
        Me.BUT_SALVAR.Text = "SALVAR"
        Me.BUT_SALVAR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_SALVAR.UseVisualStyleBackColor = True
        '
        'BUT_EXCLUIR
        '
        Me.BUT_EXCLUIR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_EXCLUIR.Image = CType(resources.GetObject("BUT_EXCLUIR.Image"), System.Drawing.Image)
        Me.BUT_EXCLUIR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_EXCLUIR.Location = New System.Drawing.Point(391, 135)
        Me.BUT_EXCLUIR.Margin = New System.Windows.Forms.Padding(4)
        Me.BUT_EXCLUIR.Name = "BUT_EXCLUIR"
        Me.BUT_EXCLUIR.Size = New System.Drawing.Size(121, 57)
        Me.BUT_EXCLUIR.TabIndex = 18
        Me.BUT_EXCLUIR.Text = "EXCLUIR"
        Me.BUT_EXCLUIR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_EXCLUIR.UseVisualStyleBackColor = True
        '
        'FCadastroOrdemitem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(759, 208)
        Me.Controls.Add(Me.BUT_SALVAR)
        Me.Controls.Add(Me.BUT_EXCLUIR)
        Me.Controls.Add(Me.CBO_TXT_SERVICO)
        Me.Controls.Add(Me.TextBox7)
        Me.Controls.Add(Me.TextBox6)
        Me.Controls.Add(Me.TextBox5)
        Me.Controls.Add(Me.TXT_FL_SITUACAO)
        Me.Controls.Add(Me.TXT_DT_ORDEMSERVICO)
        Me.Controls.Add(Me.TXT_COD_ORDEMSERVICO)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FCadastroOrdemitem"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "FCadastroOrdemitem"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents TXT_COD_ORDEMSERVICO As TextBox
    Friend WithEvents TXT_FL_SITUACAO As TextBox
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents CBO_TXT_SERVICO As ComboBox
    Friend WithEvents BUT_SALVAR As Button
    Friend WithEvents BUT_EXCLUIR As Button
    Friend WithEvents TXT_DT_ORDEMSERVICO As TextBox
End Class
