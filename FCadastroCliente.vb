﻿Imports Npgsql
Imports System.Data

Public Class FCadastroCliente

    Private Sub FCadastroCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CADASTRO DE CLIENTES"


        If TXT_COD_CLIENTE.Text = Nothing Then
            PreencheCBO("select txt_tipo from oficina.tb_tipo_cliente " + " order by txt_tipo ", _
                    "Tipo", "tb_tipo_cliente", "txt_tipo", CBO_TIPO_CLIENTE, Me)

            PreencheCBO_UF(CBO_UF, Me)
        End If
        
    End Sub

    Private Function ManutencaoCliente() As Boolean
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction
        Dim vL_DataRead As NpgsqlDataReader

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = "select * from oficina.fct_manutencao_cliente(" + _
                                                 IIf(TXT_COD_CLIENTE.Text = Nothing, "0", TXT_COD_CLIENTE.Text) + ", " + _
                                                 "'" + CBO_TIPO_CLIENTE.Text + "', " + _
                                                 "'" + FormatDateTime(DT_NASCIMENTO.Value, DateFormat.ShortDate) + "', " + _
                                                 "'" + IIf(RDO_MASCULINO.Checked, "M", "F") + "', " + _
                                                 "'" + TXT_NOME.Text.Trim + "', " + _
                                                 "'" + TXT_NR_CEP.Text.Replace("_", "").Replace(".", "").Replace("-", "").Replace(",", "") + "', " + _
                                                 "'" + CBO_UF.Text + "', " + _
                                                 "'" + CBO_CIDADE.Text + "', " + _
                                                 "'" + txt_endereco.Text + "', " + _
                                                 "'" + txt_bairro.Text + "', " + _
                                                 "'" + IIf(txt_complemento.Text = Nothing, " ", txt_complemento.Text) + "', " + _
                                                 "'" + IIf(txt_referencia.Text = Nothing, " ", txt_referencia.Text) + "')"

                                                 
                    vL_DataRead = .ExecuteReader()
                End With

                vL_Trans.Commit()
                vL_DataRead.Read()

                MessageBox.Show(vL_DataRead.Item(0).ToString.Trim, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                ManutencaoCliente = True
            Catch ex As Exception
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                vL_Trans.Rollback()
                ManutencaoCliente = False
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            ManutencaoCliente = False
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Function

    Private Function RemoveCliente() As Boolean
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction
        'Dim vL_DataRead As NpgsqlDataReader

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = "delete from oficina.tb_cliente where cod_cliente =" + TXT_COD_CLIENTE.Text
                    'vL_DataRead = .ExecuteReader()
                    .ExecuteNonQuery()
                End With

                vL_Trans.Commit()
                'vL_DataRead.Read()

                MessageBox.Show("Exclusão realizada com sucesso!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                RemoveCliente = True
            Catch ex As Exception
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                vL_Trans.Rollback()
                RemoveCliente = False
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            RemoveCliente = False
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Function

    Private Sub TXT_NR_CEP_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TXT_NR_CEP.KeyPress
        e.Handled = Not Char.IsDigit(e.KeyChar) And Not Char.IsControl(e.KeyChar)
    End Sub

    Private Sub CBO_UF_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBO_UF.SelectedIndexChanged
        PreencheCBO("SELECT TXT_CIDADE FROM OFICINA.V_CIDADES WHERE SG_UF = '" + CBO_UF.Text + "' order by TXT_CIDADE ", _
                    "Cidades", "v_cidades", "TXT_CIDADE", CBO_CIDADE, Me)
    End Sub

    Private Sub BUT_SALVAR_Click(sender As Object, e As EventArgs) Handles BUT_SALVAR.Click
        If CBO_TIPO_CLIENTE.Text = Nothing Then
            MessageBox.Show("Informo o tipo de cliente!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            CBO_TIPO_CLIENTE.Focus()
            Exit Sub
        End If

        If TXT_NOME.Text = Nothing Then
            MessageBox.Show("Informe o nome do cliente!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TXT_NOME.Focus()
            Exit Sub
        End If

        If TXT_NR_CEP.Text.Replace("-", "").Replace(".", "").Replace("_", "").Replace(" ", "") = "".Replace(",", "") Then
            MessageBox.Show("Informe o CEP do endereço do cliente!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TXT_NR_CEP.Focus()
            Exit Sub
        End If

        If CBO_UF.Text = Nothing Then
            MessageBox.Show("Informe a UF do endereço do cliente!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            CBO_UF.Focus()
            Exit Sub
        End If

        If CBO_CIDADE.Text = Nothing Then
            MessageBox.Show("Informe a cidade do endereço do cliente!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            CBO_CIDADE.Focus()
            Exit Sub
        End If

        If txt_endereco.Text = Nothing Then
            MessageBox.Show("Informe o endereço do cliente!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txt_endereco.Focus()
            Exit Sub
        End If

        If txt_bairro.Text = Nothing Then
            MessageBox.Show("Informe o bairro do endereço do cliente!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txt_bairro.Focus()
            Exit Sub
        End If

        If ManutencaoCliente() = True Then
            Me.Close()
            Me.Dispose()
        End If
    End Sub

    Private Sub BUT_EXCLUIR_Click(sender As Object, e As EventArgs) Handles BUT_EXCLUIR.Click
        If RemoveCliente() = True Then
            Me.Close()
        End If
    End Sub
End Class