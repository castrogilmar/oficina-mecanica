﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FCadastroOrdemServico
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FCadastroOrdemServico))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dt_ordemservico = New System.Windows.Forms.TextBox()
        Me.hr_ordemservico = New System.Windows.Forms.TextBox()
        Me.cod_ordemservico = New System.Windows.Forms.TextBox()
        Me.Cd_tipo = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_cliente = New System.Windows.Forms.TextBox()
        Me.Cd_fone = New System.Windows.Forms.TextBox()
        Me.cd_placa = New System.Windows.Forms.TextBox()
        Me.Cd_Ano = New System.Windows.Forms.TextBox()
        Me.id_vendedor = New System.Windows.Forms.ComboBox()
        Me.dt_entrega = New System.Windows.Forms.DateTimePicker()
        Me.r = New System.Windows.Forms.DataGridView()
        Me.Tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Referência = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descrição = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Valor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Qtde = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BUT_SALVAR = New System.Windows.Forms.Button()
        Me.BUT_EXCLUIR = New System.Windows.Forms.Button()
        Me.BUT_NOVO_ITEM = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        CType(Me.r, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label1.Location = New System.Drawing.Point(291, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 18)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Data"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label2.Location = New System.Drawing.Point(445, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 18)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Hora"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label3.Location = New System.Drawing.Point(564, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 18)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Tipo"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label4.Location = New System.Drawing.Point(38, 10)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 18)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "O.S."
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label5.Location = New System.Drawing.Point(164, 10)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 18)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Situação"
        '
        'dt_ordemservico
        '
        Me.dt_ordemservico.BackColor = System.Drawing.SystemColors.Info
        Me.dt_ordemservico.Enabled = False
        Me.dt_ordemservico.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.dt_ordemservico.Location = New System.Drawing.Point(278, 30)
        Me.dt_ordemservico.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dt_ordemservico.Name = "dt_ordemservico"
        Me.dt_ordemservico.Size = New System.Drawing.Size(112, 26)
        Me.dt_ordemservico.TabIndex = 3
        '
        'hr_ordemservico
        '
        Me.hr_ordemservico.BackColor = System.Drawing.SystemColors.Info
        Me.hr_ordemservico.Enabled = False
        Me.hr_ordemservico.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.hr_ordemservico.Location = New System.Drawing.Point(418, 31)
        Me.hr_ordemservico.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.hr_ordemservico.Name = "hr_ordemservico"
        Me.hr_ordemservico.Size = New System.Drawing.Size(97, 26)
        Me.hr_ordemservico.TabIndex = 4
        '
        'cod_ordemservico
        '
        Me.cod_ordemservico.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cod_ordemservico.Enabled = False
        Me.cod_ordemservico.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.cod_ordemservico.Location = New System.Drawing.Point(11, 30)
        Me.cod_ordemservico.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cod_ordemservico.Name = "cod_ordemservico"
        Me.cod_ordemservico.Size = New System.Drawing.Size(112, 26)
        Me.cod_ordemservico.TabIndex = 1
        '
        'Cd_tipo
        '
        Me.Cd_tipo.BackColor = System.Drawing.SystemColors.Info
        Me.Cd_tipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Cd_tipo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Cd_tipo.FormattingEnabled = True
        Me.Cd_tipo.Location = New System.Drawing.Point(533, 30)
        Me.Cd_tipo.Name = "Cd_tipo"
        Me.Cd_tipo.Size = New System.Drawing.Size(136, 26)
        Me.Cd_tipo.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label6.Location = New System.Drawing.Point(8, 57)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 18)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Cliente"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label7.Location = New System.Drawing.Point(682, 57)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(65, 18)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Telefone"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label8.Location = New System.Drawing.Point(8, 109)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 18)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Vendedor"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label9.Location = New System.Drawing.Point(9, 161)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(108, 18)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Placa do Carro"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label10.Location = New System.Drawing.Point(446, 163)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(88, 18)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Ano/Modelo"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label11.Location = New System.Drawing.Point(9, 214)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(141, 18)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Previsão de Entrega"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label12.Location = New System.Drawing.Point(437, 218)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(46, 18)
        Me.Label12.TabIndex = 17
        Me.Label12.Text = "Etapa"
        '
        'txt_cliente
        '
        Me.txt_cliente.BackColor = System.Drawing.SystemColors.Info
        Me.txt_cliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.txt_cliente.Location = New System.Drawing.Point(14, 80)
        Me.txt_cliente.Name = "txt_cliente"
        Me.txt_cliente.Size = New System.Drawing.Size(643, 26)
        Me.txt_cliente.TabIndex = 6
        '
        'Cd_fone
        '
        Me.Cd_fone.BackColor = System.Drawing.SystemColors.Info
        Me.Cd_fone.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.Cd_fone.Location = New System.Drawing.Point(685, 80)
        Me.Cd_fone.Name = "Cd_fone"
        Me.Cd_fone.Size = New System.Drawing.Size(221, 26)
        Me.Cd_fone.TabIndex = 7
        '
        'cd_placa
        '
        Me.cd_placa.BackColor = System.Drawing.SystemColors.Info
        Me.cd_placa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.cd_placa.Location = New System.Drawing.Point(15, 185)
        Me.cd_placa.Name = "cd_placa"
        Me.cd_placa.Size = New System.Drawing.Size(297, 26)
        Me.cd_placa.TabIndex = 9
        '
        'Cd_Ano
        '
        Me.Cd_Ano.BackColor = System.Drawing.SystemColors.Info
        Me.Cd_Ano.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.Cd_Ano.Location = New System.Drawing.Point(434, 185)
        Me.Cd_Ano.Name = "Cd_Ano"
        Me.Cd_Ano.Size = New System.Drawing.Size(220, 26)
        Me.Cd_Ano.TabIndex = 10
        '
        'id_vendedor
        '
        Me.id_vendedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.id_vendedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.id_vendedor.FormattingEnabled = True
        Me.id_vendedor.Location = New System.Drawing.Point(12, 130)
        Me.id_vendedor.Name = "id_vendedor"
        Me.id_vendedor.Size = New System.Drawing.Size(378, 26)
        Me.id_vendedor.TabIndex = 8
        '
        'dt_entrega
        '
        Me.dt_entrega.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.dt_entrega.CalendarMonthBackground = System.Drawing.SystemColors.Info
        Me.dt_entrega.Location = New System.Drawing.Point(11, 240)
        Me.dt_entrega.Name = "dt_entrega"
        Me.dt_entrega.Size = New System.Drawing.Size(344, 24)
        Me.dt_entrega.TabIndex = 11
        '
        'r
        '
        Me.r.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.r.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Tipo, Me.Referência, Me.Descrição, Me.Valor, Me.Qtde, Me.Total})
        Me.r.Location = New System.Drawing.Point(15, 286)
        Me.r.Name = "r"
        Me.r.RowTemplate.Height = 24
        Me.r.Size = New System.Drawing.Size(1039, 200)
        Me.r.TabIndex = 18
        '
        'Tipo
        '
        Me.Tipo.HeaderText = "Tipo"
        Me.Tipo.Name = "Tipo"
        Me.Tipo.Width = 80
        '
        'Referência
        '
        Me.Referência.HeaderText = "Referência"
        Me.Referência.Name = "Referência"
        Me.Referência.Width = 80
        '
        'Descrição
        '
        Me.Descrição.HeaderText = "Descrição"
        Me.Descrição.Name = "Descrição"
        Me.Descrição.Width = 515
        '
        'Valor
        '
        Me.Valor.HeaderText = "Valor R$"
        Me.Valor.Name = "Valor"
        '
        'Qtde
        '
        Me.Qtde.HeaderText = "Qtde"
        Me.Qtde.Name = "Qtde"
        '
        'Total
        '
        Me.Total.HeaderText = "Total R$"
        Me.Total.Name = "Total"
        Me.Total.Width = 120
        '
        'BUT_SALVAR
        '
        Me.BUT_SALVAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_SALVAR.Image = Global.oficina_mecanica.My.Resources.Resources.Hopstarter_Sleek_Xp_Basic_Ok
        Me.BUT_SALVAR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_SALVAR.Location = New System.Drawing.Point(808, 538)
        Me.BUT_SALVAR.Margin = New System.Windows.Forms.Padding(4)
        Me.BUT_SALVAR.Name = "BUT_SALVAR"
        Me.BUT_SALVAR.Size = New System.Drawing.Size(117, 57)
        Me.BUT_SALVAR.TabIndex = 19
        Me.BUT_SALVAR.Text = "SALVAR"
        Me.BUT_SALVAR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_SALVAR.UseVisualStyleBackColor = True
        '
        'BUT_EXCLUIR
        '
        Me.BUT_EXCLUIR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_EXCLUIR.Image = CType(resources.GetObject("BUT_EXCLUIR.Image"), System.Drawing.Image)
        Me.BUT_EXCLUIR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_EXCLUIR.Location = New System.Drawing.Point(933, 538)
        Me.BUT_EXCLUIR.Margin = New System.Windows.Forms.Padding(4)
        Me.BUT_EXCLUIR.Name = "BUT_EXCLUIR"
        Me.BUT_EXCLUIR.Size = New System.Drawing.Size(121, 57)
        Me.BUT_EXCLUIR.TabIndex = 20
        Me.BUT_EXCLUIR.Text = "EXCLUIR"
        Me.BUT_EXCLUIR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_EXCLUIR.UseVisualStyleBackColor = True
        '
        'BUT_NOVO_ITEM
        '
        Me.BUT_NOVO_ITEM.Location = New System.Drawing.Point(939, 246)
        Me.BUT_NOVO_ITEM.Name = "BUT_NOVO_ITEM"
        Me.BUT_NOVO_ITEM.Size = New System.Drawing.Size(115, 34)
        Me.BUT_NOVO_ITEM.TabIndex = 21
        Me.BUT_NOVO_ITEM.Text = "Item"
        Me.BUT_NOVO_ITEM.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(804, 10)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(71, 18)
        Me.Label13.TabIndex = 22
        Me.Label13.Text = "Vendedor"
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Info
        Me.TextBox1.Enabled = False
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.TextBox1.Location = New System.Drawing.Point(753, 33)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(172, 26)
        Me.TextBox1.TabIndex = 23
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(703, 161)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(90, 18)
        Me.Label14.TabIndex = 24
        Me.Label14.Text = "Km. Anterior"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(836, 161)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(75, 18)
        Me.Label15.TabIndex = 25
        Me.Label15.Text = "Km.  Atual"
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.SystemColors.Info
        Me.TextBox2.Location = New System.Drawing.Point(695, 187)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 24)
        Me.TextBox2.TabIndex = 26
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(825, 187)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(100, 24)
        Me.TextBox3.TabIndex = 27
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(937, 200)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(117, 36)
        Me.Button1.TabIndex = 28
        Me.Button1.Text = "Cliente"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.BackColor = System.Drawing.SystemColors.Info
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(403, 238)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(198, 26)
        Me.ComboBox1.TabIndex = 29
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(138, 30)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(134, 26)
        Me.ComboBox2.TabIndex = 30
        '
        'FCadastroOrdemServico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1061, 603)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.BUT_NOVO_ITEM)
        Me.Controls.Add(Me.BUT_SALVAR)
        Me.Controls.Add(Me.BUT_EXCLUIR)
        Me.Controls.Add(Me.r)
        Me.Controls.Add(Me.dt_entrega)
        Me.Controls.Add(Me.id_vendedor)
        Me.Controls.Add(Me.Cd_Ano)
        Me.Controls.Add(Me.cd_placa)
        Me.Controls.Add(Me.Cd_fone)
        Me.Controls.Add(Me.txt_cliente)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Cd_tipo)
        Me.Controls.Add(Me.cod_ordemservico)
        Me.Controls.Add(Me.hr_ordemservico)
        Me.Controls.Add(Me.dt_ordemservico)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FCadastroOrdemServico"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FCadastroOrdemServico"
        CType(Me.r, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents dt_ordemservico As TextBox
    Friend WithEvents hr_ordemservico As TextBox
    Friend WithEvents cod_ordemservico As TextBox
    Friend WithEvents Cd_tipo As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txt_cliente As TextBox
    Friend WithEvents Cd_fone As TextBox
    Friend WithEvents cd_placa As TextBox
    Friend WithEvents Cd_Ano As TextBox
    Friend WithEvents id_vendedor As ComboBox
    Friend WithEvents dt_entrega As DateTimePicker
    Friend WithEvents r As DataGridView
    Friend WithEvents BUT_SALVAR As Button
    Friend WithEvents BUT_EXCLUIR As Button
    Friend WithEvents Tipo As DataGridViewTextBoxColumn
    Friend WithEvents Referência As DataGridViewTextBoxColumn
    Friend WithEvents Descrição As DataGridViewTextBoxColumn
    Friend WithEvents Valor As DataGridViewTextBoxColumn
    Friend WithEvents Qtde As DataGridViewTextBoxColumn
    Friend WithEvents Total As DataGridViewTextBoxColumn
    Friend WithEvents BUT_NOVO_ITEM As Button
    Friend WithEvents Label13 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents ComboBox2 As ComboBox
End Class
