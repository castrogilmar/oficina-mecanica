﻿Public Class FConsultaTipoCliente
    Private Sub FConsultaAuxiliar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CONSULTA DE TIPOS DE CLIENTE"
    End Sub

    Private Sub BUT_NOVO_Click(sender As Object, e As EventArgs) Handles BUT_NOVO.Click
        FCadastroTipoCliente.ShowDialog()
        FCadastroTipoCliente.Dispose()
    End Sub

    Private Sub BUT_CONSULTA_Click(sender As Object, e As EventArgs) Handles BUT_CONSULTA.Click
        PreencheGrid("tb_tipo_cliente", "select * from oficina.tb_tipo_cliente " + _
                     IIf(TXT_DESCRICAO.Text <> Nothing, " where txt_tipo like '" + TXT_DESCRICAO.Text.Trim + "%' ", ""), Me, DTG_TIPOS_CLIENTE)
    End Sub

    Private Sub DTG_TIPOS_CLIENTE_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DTG_TIPOS_CLIENTE.CellClick
        If e.ColumnIndex = DTG_TIPOS_CLIENTE.Columns("cod_tipo").Index And Not IsDBNull(DTG_TIPOS_CLIENTE.CurrentRow.Cells("cod_tipo").Value) Then
            'MessageBox.Show(DTG_GRUPOS.CurrentRow.Cells("cod_grupo").Value)

            FCadastroTipoCliente.TXT_COD_TIPO.Text = DTG_TIPOS_CLIENTE.CurrentRow.Cells("cod_tipo").Value.ToString
            FCadastroTipoCliente.TXT_DESCRICAO.Text = DTG_TIPOS_CLIENTE.CurrentRow.Cells("txt_tipo").Value

            FCadastroTipoCliente.ShowDialog()
            FCadastroTipoCliente.Dispose()
        End If
    End Sub
End Class