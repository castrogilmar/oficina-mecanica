﻿Imports Npgsql
Imports System.Data

Public Class FCadastroTipoDocumento

    Private Sub FCadastroTipoDocumento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CADASTRO DE TIPOS DE DOCUMENTO"

        If TXT_COD_DOCUMENTO.Text = Nothing Then
            BUT_EXCLUIR.Enabled = False
        End If

        If TXT_COD_DOCUMENTO.Text = Nothing Then
            PreencheCBO("select txt_tipo from oficina.tb_tipo_cliente " + " order by txt_tipo ", _
                    "Tipo", "tb_tipo_cliente", "txt_tipo", CBO_TIPO_CLIENTE, Me)
        End If
        
    End Sub

    Private Function ExclusaoTpoDocumento() As Boolean
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = "delete from oficina.tb_tipo_documento where cod_documento=" + TXT_COD_DOCUMENTO.Text.Trim + "::integer"
                    .ExecuteNonQuery()
                End With

                vL_Trans.Commit()

                MessageBox.Show("Tipo excluído com sucesso!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                ExclusaoTpoDocumento = True
            Catch ex As Exception
                vL_Trans.Rollback()
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                ExclusaoTpoDocumento = False
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            ExclusaoTpoDocumento = False
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Function

    Private Function ManutencaoTpoDocumento() As Boolean
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction
        Dim vL_DataRead As NpgsqlDataReader

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            With vL_Cmd
                .Connection = vL_Conn
                .CommandType = CommandType.Text
                .CommandText = "select cod_documento from oficina.tb_tipo_documento where txt_documento = '" + TXT_DESCRICAO.Text.Trim + "'"
                vL_DataRead = .ExecuteReader
            End With

            If vL_DataRead.HasRows Then
                MessageBox.Show("O documento informado já foi cadastrado anteriormente!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                vL_Conn.Close()
                Return False
            End If

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = "select * from oficina.fct_manutencao_tpo_documento(" + _
                                                 IIf(TXT_COD_DOCUMENTO.Text = Nothing, "0", TXT_COD_DOCUMENTO.Text) + ", " + _
                                                 "'" + CBO_TIPO_CLIENTE.Text + "', " + _
                                                 "'" + TXT_DESCRICAO.Text + "')"
                    vL_DataRead = .ExecuteReader()
                End With

                vL_Trans.Commit()
                vL_DataRead.Read()

                MessageBox.Show(vL_DataRead.Item(0).ToString.Trim, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                ManutencaoTpoDocumento = True
            Catch ex As Exception
                vL_Trans.Rollback()
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                ManutencaoTpoDocumento = False
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            ManutencaoTpoDocumento = False
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Function

    Private Sub BUT_SALVAR_Click(sender As Object, e As EventArgs) Handles BUT_SALVAR.Click
        If TXT_DESCRICAO.Text = Nothing Then
            MessageBox.Show("Descrição do tipo de documento não informada!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TXT_DESCRICAO.Focus()
            Exit Sub
        End If

        If CBO_TIPO_CLIENTE.Text = Nothing Then
            MessageBox.Show("Informe o tipo de cliente para o qual o tipo de documento será criado!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            CBO_TIPO_CLIENTE.Focus()
            Exit Sub
        End If

        If ManutencaoTpoDocumento() = True Then
            Me.Close()
            Me.Dispose()
        End If
    End Sub

    Private Sub BUT_EXCLUIR_Click(sender As Object, e As EventArgs) Handles BUT_EXCLUIR.Click
        If MessageBox.Show("Deseja realmente excluir o tipo de documento?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        If ExclusaoTpoDocumento() Then
            Me.Close()
        End If
    End Sub
End Class