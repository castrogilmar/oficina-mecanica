﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FCadastroFuncionario
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FCadastroFuncionario))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.RDO_SERVICO = New System.Windows.Forms.RadioButton()
        Me.RDO_PRODUTO = New System.Windows.Forms.RadioButton()
        Me.DT_ADMICAO = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TXT_NONE_GERRA = New System.Windows.Forms.TextBox()
        Me.TXT_NO_CARGO = New System.Windows.Forms.ComboBox()
        Me.TXT_NOME_FUNCIONARIO = New System.Windows.Forms.TextBox()
        Me.TXT_COD_FUNCIONARIO = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TXT_NR_CEP = New System.Windows.Forms.MaskedTextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TXT_NO_REFERENCIA = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TXT_NO_COMPLEMENTO = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TXT_NO_UF = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TXT_NO_BAIRRO = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TXT_NO_ENDERECO = New System.Windows.Forms.TextBox()
        Me.BUT_SALVAR = New System.Windows.Forms.Button()
        Me.BUT_EXCLUIR = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.DT_ADMICAO)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.TXT_NONE_GERRA)
        Me.GroupBox1.Controls.Add(Me.TXT_NO_CARGO)
        Me.GroupBox1.Controls.Add(Me.TXT_NOME_FUNCIONARIO)
        Me.GroupBox1.Controls.Add(Me.TXT_COD_FUNCIONARIO)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(674, 160)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.RDO_SERVICO)
        Me.GroupBox3.Controls.Add(Me.RDO_PRODUTO)
        Me.GroupBox3.Location = New System.Drawing.Point(346, 57)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(174, 43)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Sexo"
        '
        'RDO_SERVICO
        '
        Me.RDO_SERVICO.AutoSize = True
        Me.RDO_SERVICO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RDO_SERVICO.Location = New System.Drawing.Point(94, 19)
        Me.RDO_SERVICO.Name = "RDO_SERVICO"
        Me.RDO_SERVICO.Size = New System.Drawing.Size(77, 19)
        Me.RDO_SERVICO.TabIndex = 3
        Me.RDO_SERVICO.Text = "Feminino"
        Me.RDO_SERVICO.UseVisualStyleBackColor = True
        '
        'RDO_PRODUTO
        '
        Me.RDO_PRODUTO.AutoSize = True
        Me.RDO_PRODUTO.Checked = True
        Me.RDO_PRODUTO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RDO_PRODUTO.Location = New System.Drawing.Point(6, 18)
        Me.RDO_PRODUTO.Name = "RDO_PRODUTO"
        Me.RDO_PRODUTO.Size = New System.Drawing.Size(82, 19)
        Me.RDO_PRODUTO.TabIndex = 3
        Me.RDO_PRODUTO.TabStop = True
        Me.RDO_PRODUTO.Text = "Masculino"
        Me.RDO_PRODUTO.UseVisualStyleBackColor = True
        '
        'DT_ADMICAO
        '
        Me.DT_ADMICAO.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DT_ADMICAO.CalendarMonthBackground = System.Drawing.SystemColors.Info
        Me.DT_ADMICAO.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DT_ADMICAO.Location = New System.Drawing.Point(346, 31)
        Me.DT_ADMICAO.Name = "DT_ADMICAO"
        Me.DT_ADMICAO.Size = New System.Drawing.Size(97, 20)
        Me.DT_ADMICAO.TabIndex = 49
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(343, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 13)
        Me.Label4.TabIndex = 48
        Me.Label4.Text = "Data Admição"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(99, 15)
        Me.Label3.TabIndex = 47
        Me.Label3.Text = "Nome de Guerra"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 15)
        Me.Label2.TabIndex = 47
        Me.Label2.Text = "Nome"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(343, 112)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(40, 15)
        Me.Label8.TabIndex = 47
        Me.Label8.Text = "Cargo"
        '
        'TXT_NONE_GERRA
        '
        Me.TXT_NONE_GERRA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_NONE_GERRA.Enabled = False
        Me.TXT_NONE_GERRA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_NONE_GERRA.Location = New System.Drawing.Point(6, 130)
        Me.TXT_NONE_GERRA.Name = "TXT_NONE_GERRA"
        Me.TXT_NONE_GERRA.Size = New System.Drawing.Size(324, 21)
        Me.TXT_NONE_GERRA.TabIndex = 46
        '
        'TXT_NO_CARGO
        '
        Me.TXT_NO_CARGO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_NO_CARGO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.TXT_NO_CARGO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.TXT_NO_CARGO.FormattingEnabled = True
        Me.TXT_NO_CARGO.Location = New System.Drawing.Point(346, 130)
        Me.TXT_NO_CARGO.Name = "TXT_NO_CARGO"
        Me.TXT_NO_CARGO.Size = New System.Drawing.Size(328, 21)
        Me.TXT_NO_CARGO.TabIndex = 10
        '
        'TXT_NOME_FUNCIONARIO
        '
        Me.TXT_NOME_FUNCIONARIO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_NOME_FUNCIONARIO.Enabled = False
        Me.TXT_NOME_FUNCIONARIO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_NOME_FUNCIONARIO.Location = New System.Drawing.Point(6, 81)
        Me.TXT_NOME_FUNCIONARIO.Name = "TXT_NOME_FUNCIONARIO"
        Me.TXT_NOME_FUNCIONARIO.Size = New System.Drawing.Size(324, 21)
        Me.TXT_NOME_FUNCIONARIO.TabIndex = 46
        '
        'TXT_COD_FUNCIONARIO
        '
        Me.TXT_COD_FUNCIONARIO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_COD_FUNCIONARIO.Enabled = False
        Me.TXT_COD_FUNCIONARIO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_COD_FUNCIONARIO.Location = New System.Drawing.Point(6, 34)
        Me.TXT_COD_FUNCIONARIO.Name = "TXT_COD_FUNCIONARIO"
        Me.TXT_COD_FUNCIONARIO.Size = New System.Drawing.Size(100, 21)
        Me.TXT_COD_FUNCIONARIO.TabIndex = 46
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 15)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "Código"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TXT_NR_CEP)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.TXT_NO_REFERENCIA)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.TXT_NO_COMPLEMENTO)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.TXT_NO_UF)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.TXT_NO_BAIRRO)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.TXT_NO_ENDERECO)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 178)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(674, 210)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        '
        'TXT_NR_CEP
        '
        Me.TXT_NR_CEP.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_NR_CEP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_NR_CEP.Location = New System.Drawing.Point(9, 43)
        Me.TXT_NR_CEP.Mask = "00.000-000"
        Me.TXT_NR_CEP.Name = "TXT_NR_CEP"
        Me.TXT_NR_CEP.Size = New System.Drawing.Size(88, 22)
        Me.TXT_NR_CEP.TabIndex = 8
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(6, 161)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(67, 15)
        Me.Label10.TabIndex = 50
        Me.Label10.Text = "Referência"
        '
        'TXT_NO_REFERENCIA
        '
        Me.TXT_NO_REFERENCIA.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_NO_REFERENCIA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_NO_REFERENCIA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_NO_REFERENCIA.Location = New System.Drawing.Point(6, 179)
        Me.TXT_NO_REFERENCIA.Name = "TXT_NO_REFERENCIA"
        Me.TXT_NO_REFERENCIA.Size = New System.Drawing.Size(353, 22)
        Me.TXT_NO_REFERENCIA.TabIndex = 14
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(329, 114)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(85, 15)
        Me.Label9.TabIndex = 48
        Me.Label9.Text = "Complemento"
        '
        'TXT_NO_COMPLEMENTO
        '
        Me.TXT_NO_COMPLEMENTO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_NO_COMPLEMENTO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_NO_COMPLEMENTO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_NO_COMPLEMENTO.Location = New System.Drawing.Point(329, 132)
        Me.TXT_NO_COMPLEMENTO.Name = "TXT_NO_COMPLEMENTO"
        Me.TXT_NO_COMPLEMENTO.Size = New System.Drawing.Size(325, 22)
        Me.TXT_NO_COMPLEMENTO.TabIndex = 13
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(122, 27)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(23, 15)
        Me.Label7.TabIndex = 45
        Me.Label7.Text = "UF"
        '
        'TXT_NO_UF
        '
        Me.TXT_NO_UF.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_NO_UF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.TXT_NO_UF.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.TXT_NO_UF.FormattingEnabled = True
        Me.TXT_NO_UF.Location = New System.Drawing.Point(125, 43)
        Me.TXT_NO_UF.Name = "TXT_NO_UF"
        Me.TXT_NO_UF.Size = New System.Drawing.Size(186, 21)
        Me.TXT_NO_UF.TabIndex = 9
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 114)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 15)
        Me.Label6.TabIndex = 42
        Me.Label6.Text = "Bairro"
        '
        'TXT_NO_BAIRRO
        '
        Me.TXT_NO_BAIRRO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_NO_BAIRRO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_NO_BAIRRO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_NO_BAIRRO.Location = New System.Drawing.Point(6, 132)
        Me.TXT_NO_BAIRRO.Name = "TXT_NO_BAIRRO"
        Me.TXT_NO_BAIRRO.Size = New System.Drawing.Size(274, 22)
        Me.TXT_NO_BAIRRO.TabIndex = 12
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(9, 25)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 15)
        Me.Label5.TabIndex = 40
        Me.Label5.Text = "CEP"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(6, 67)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(60, 15)
        Me.Label11.TabIndex = 38
        Me.Label11.Text = "Endereço"
        '
        'TXT_NO_ENDERECO
        '
        Me.TXT_NO_ENDERECO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_NO_ENDERECO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_NO_ENDERECO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_NO_ENDERECO.Location = New System.Drawing.Point(6, 85)
        Me.TXT_NO_ENDERECO.Name = "TXT_NO_ENDERECO"
        Me.TXT_NO_ENDERECO.Size = New System.Drawing.Size(648, 22)
        Me.TXT_NO_ENDERECO.TabIndex = 11
        '
        'BUT_SALVAR
        '
        Me.BUT_SALVAR.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BUT_SALVAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_SALVAR.Image = Global.oficina_mecanica.My.Resources.Resources.Hopstarter_Sleek_Xp_Basic_Ok
        Me.BUT_SALVAR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_SALVAR.Location = New System.Drawing.Point(244, 408)
        Me.BUT_SALVAR.Name = "BUT_SALVAR"
        Me.BUT_SALVAR.Size = New System.Drawing.Size(88, 46)
        Me.BUT_SALVAR.TabIndex = 16
        Me.BUT_SALVAR.Text = "SALVAR"
        Me.BUT_SALVAR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_SALVAR.UseVisualStyleBackColor = True
        '
        'BUT_EXCLUIR
        '
        Me.BUT_EXCLUIR.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BUT_EXCLUIR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_EXCLUIR.Image = CType(resources.GetObject("BUT_EXCLUIR.Image"), System.Drawing.Image)
        Me.BUT_EXCLUIR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_EXCLUIR.Location = New System.Drawing.Point(361, 408)
        Me.BUT_EXCLUIR.Name = "BUT_EXCLUIR"
        Me.BUT_EXCLUIR.Size = New System.Drawing.Size(91, 46)
        Me.BUT_EXCLUIR.TabIndex = 17
        Me.BUT_EXCLUIR.Text = "EXCLUIR"
        Me.BUT_EXCLUIR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_EXCLUIR.UseVisualStyleBackColor = True
        '
        'FCadastroFuncionario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(697, 466)
        Me.Controls.Add(Me.BUT_EXCLUIR)
        Me.Controls.Add(Me.BUT_SALVAR)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FCadastroFuncionario"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FCadastroFuncionario"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents TXT_COD_FUNCIONARIO As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents TXT_NONE_GERRA As TextBox
    Friend WithEvents TXT_NOME_FUNCIONARIO As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents DT_ADMICAO As DateTimePicker
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents RDO_SERVICO As RadioButton
    Friend WithEvents RDO_PRODUTO As RadioButton
    Friend WithEvents Label8 As Label
    Friend WithEvents TXT_NO_CARGO As ComboBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents TXT_NR_CEP As MaskedTextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents TXT_NO_REFERENCIA As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents TXT_NO_COMPLEMENTO As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents TXT_NO_UF As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents TXT_NO_BAIRRO As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents TXT_NO_ENDERECO As TextBox
    Friend WithEvents BUT_SALVAR As Button
    Friend WithEvents BUT_EXCLUIR As Button
End Class
