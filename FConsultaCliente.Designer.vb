﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FConsultaCliente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DTG_CLIENTES = New System.Windows.Forms.DataGridView()
        Me.TXT_NOME = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CBO_TIPO_CLIENTE = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BUT_CONSULTA = New System.Windows.Forms.Button()
        Me.BUT_NOVO = New System.Windows.Forms.Button()
        Me.cod_cliente = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.txt_cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpo_cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dt_nascimento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fl_sexo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nr_cep = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sg_uf = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txt_cidade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txt_endereco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txt_bairro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txt_complemento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txt_referencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DTG_CLIENTES, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DTG_CLIENTES
        '
        Me.DTG_CLIENTES.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DTG_CLIENTES.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cod_cliente, Me.txt_cliente, Me.tpo_cliente, Me.dt_nascimento, Me.fl_sexo, Me.nr_cep, Me.sg_uf, Me.txt_cidade, Me.txt_endereco, Me.txt_bairro, Me.txt_complemento, Me.txt_referencia})
        Me.DTG_CLIENTES.Location = New System.Drawing.Point(12, 116)
        Me.DTG_CLIENTES.Name = "DTG_CLIENTES"
        Me.DTG_CLIENTES.Size = New System.Drawing.Size(1230, 488)
        Me.DTG_CLIENTES.TabIndex = 15
        '
        'TXT_NOME
        '
        Me.TXT_NOME.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_NOME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_NOME.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_NOME.Location = New System.Drawing.Point(9, 71)
        Me.TXT_NOME.Name = "TXT_NOME"
        Me.TXT_NOME.Size = New System.Drawing.Size(607, 22)
        Me.TXT_NOME.TabIndex = 4
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.CBO_TIPO_CLIENTE)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TXT_NOME)
        Me.GroupBox1.Controls.Add(Me.BUT_CONSULTA)
        Me.GroupBox1.Controls.Add(Me.BUT_NOVO)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1230, 110)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(89, 15)
        Me.Label4.TabIndex = 48
        Me.Label4.Text = "Tipo de Cliente"
        '
        'CBO_TIPO_CLIENTE
        '
        Me.CBO_TIPO_CLIENTE.BackColor = System.Drawing.SystemColors.Info
        Me.CBO_TIPO_CLIENTE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBO_TIPO_CLIENTE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBO_TIPO_CLIENTE.FormattingEnabled = True
        Me.CBO_TIPO_CLIENTE.Location = New System.Drawing.Point(9, 29)
        Me.CBO_TIPO_CLIENTE.Name = "CBO_TIPO_CLIENTE"
        Me.CBO_TIPO_CLIENTE.Size = New System.Drawing.Size(186, 21)
        Me.CBO_TIPO_CLIENTE.TabIndex = 47
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(97, 15)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Nome do cliente"
        '
        'BUT_CONSULTA
        '
        Me.BUT_CONSULTA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_CONSULTA.Image = Global.oficina_mecanica.My.Resources.Resources.Visualpharm_Must_Have_Search
        Me.BUT_CONSULTA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_CONSULTA.Location = New System.Drawing.Point(1080, 38)
        Me.BUT_CONSULTA.Name = "BUT_CONSULTA"
        Me.BUT_CONSULTA.Size = New System.Drawing.Size(106, 46)
        Me.BUT_CONSULTA.TabIndex = 6
        Me.BUT_CONSULTA.Text = "CONSULTAR"
        Me.BUT_CONSULTA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_CONSULTA.UseVisualStyleBackColor = True
        '
        'BUT_NOVO
        '
        Me.BUT_NOVO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_NOVO.Image = Global.oficina_mecanica.My.Resources.Resources.Visualpharm_Must_Have_Add
        Me.BUT_NOVO.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_NOVO.Location = New System.Drawing.Point(994, 38)
        Me.BUT_NOVO.Name = "BUT_NOVO"
        Me.BUT_NOVO.Size = New System.Drawing.Size(80, 46)
        Me.BUT_NOVO.TabIndex = 5
        Me.BUT_NOVO.Text = "NOVO"
        Me.BUT_NOVO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_NOVO.UseVisualStyleBackColor = True
        '
        'cod_cliente
        '
        Me.cod_cliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.cod_cliente.DataPropertyName = "cod_cliente"
        Me.cod_cliente.HeaderText = "CÓDIGO"
        Me.cod_cliente.Name = "cod_cliente"
        Me.cod_cliente.ReadOnly = True
        Me.cod_cliente.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cod_cliente.Width = 55
        '
        'txt_cliente
        '
        Me.txt_cliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_cliente.DataPropertyName = "txt_cliente"
        Me.txt_cliente.HeaderText = "NOME DO CLIENTE"
        Me.txt_cliente.Name = "txt_cliente"
        Me.txt_cliente.ReadOnly = True
        Me.txt_cliente.Width = 120
        '
        'tpo_cliente
        '
        Me.tpo_cliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.tpo_cliente.DataPropertyName = "tpo_cliente"
        Me.tpo_cliente.HeaderText = "TPO CLIENTE"
        Me.tpo_cliente.Name = "tpo_cliente"
        Me.tpo_cliente.ReadOnly = True
        Me.tpo_cliente.Width = 94
        '
        'dt_nascimento
        '
        Me.dt_nascimento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.dt_nascimento.DataPropertyName = "dt_nascimento"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.dt_nascimento.DefaultCellStyle = DataGridViewCellStyle5
        Me.dt_nascimento.HeaderText = "DT NASCIMENTO"
        Me.dt_nascimento.Name = "dt_nascimento"
        Me.dt_nascimento.ReadOnly = True
        Me.dt_nascimento.Width = 111
        '
        'fl_sexo
        '
        Me.fl_sexo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.fl_sexo.DataPropertyName = "fl_sexo"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.fl_sexo.DefaultCellStyle = DataGridViewCellStyle6
        Me.fl_sexo.HeaderText = "SEXO"
        Me.fl_sexo.Name = "fl_sexo"
        Me.fl_sexo.ReadOnly = True
        Me.fl_sexo.Width = 61
        '
        'nr_cep
        '
        Me.nr_cep.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.nr_cep.DataPropertyName = "nr_cep"
        Me.nr_cep.HeaderText = "CEP"
        Me.nr_cep.Name = "nr_cep"
        Me.nr_cep.ReadOnly = True
        Me.nr_cep.Width = 53
        '
        'sg_uf
        '
        Me.sg_uf.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.sg_uf.DataPropertyName = "sg_uf"
        Me.sg_uf.HeaderText = "UF"
        Me.sg_uf.Name = "sg_uf"
        Me.sg_uf.ReadOnly = True
        Me.sg_uf.Width = 46
        '
        'txt_cidade
        '
        Me.txt_cidade.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_cidade.DataPropertyName = "txt_cidade"
        Me.txt_cidade.HeaderText = "CIDADE"
        Me.txt_cidade.Name = "txt_cidade"
        Me.txt_cidade.ReadOnly = True
        Me.txt_cidade.Width = 72
        '
        'txt_endereco
        '
        Me.txt_endereco.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_endereco.DataPropertyName = "txt_endereco"
        Me.txt_endereco.HeaderText = "ENDEREÇO"
        Me.txt_endereco.Name = "txt_endereco"
        Me.txt_endereco.ReadOnly = True
        Me.txt_endereco.Width = 92
        '
        'txt_bairro
        '
        Me.txt_bairro.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_bairro.DataPropertyName = "txt_bairro"
        Me.txt_bairro.HeaderText = "BAIRRO"
        Me.txt_bairro.Name = "txt_bairro"
        Me.txt_bairro.ReadOnly = True
        Me.txt_bairro.Width = 73
        '
        'txt_complemento
        '
        Me.txt_complemento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_complemento.DataPropertyName = "txt_complemento"
        Me.txt_complemento.HeaderText = "COMPLEMENTO"
        Me.txt_complemento.Name = "txt_complemento"
        Me.txt_complemento.ReadOnly = True
        Me.txt_complemento.Width = 115
        '
        'txt_referencia
        '
        Me.txt_referencia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_referencia.DataPropertyName = "txt_referencia"
        Me.txt_referencia.HeaderText = "PONTO DE REFERÊNCIA"
        Me.txt_referencia.Name = "txt_referencia"
        Me.txt_referencia.ReadOnly = True
        Me.txt_referencia.Width = 145
        '
        'FConsultaCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1254, 616)
        Me.Controls.Add(Me.DTG_CLIENTES)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FConsultaCliente"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FConsultaCliente"
        CType(Me.DTG_CLIENTES, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DTG_CLIENTES As System.Windows.Forms.DataGridView
    Friend WithEvents TXT_NOME As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents CBO_TIPO_CLIENTE As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents BUT_CONSULTA As System.Windows.Forms.Button
    Friend WithEvents BUT_NOVO As System.Windows.Forms.Button
    Friend WithEvents cod_cliente As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents txt_cliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpo_cliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dt_nascimento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fl_sexo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nr_cep As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sg_uf As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txt_cidade As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txt_endereco As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txt_bairro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txt_complemento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txt_referencia As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
