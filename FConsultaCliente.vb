﻿Public Class FConsultaCliente

    Private Sub FConsultaCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CONSULTA DE CLIENTES"

        PreencheCBO("select txt_tipo from (select txt_tipo from oficina.tb_tipo_cliente " + " union " + _
                    "select 'TODOS'::varchar txt_tipo) a  order by txt_tipo", _
                    "Tipo", "tb_tipo_cliente", "txt_tipo", CBO_TIPO_CLIENTE, Me)
    End Sub

    Private Sub BUT_NOVO_Click(sender As Object, e As EventArgs) Handles BUT_NOVO.Click
        FCadastroCliente.ShowDialog()
        FCadastroCliente.Dispose()
    End Sub

    Private Sub BUT_CONSULTA_Click(sender As Object, e As EventArgs) Handles BUT_CONSULTA.Click
        PreencheGrid("tb_cliente", "select a.cod_cliente, a.txt_cliente, b.txt_tipo tpo_cliente, to_char(dt_nascimento,'DD/MM/YYYY') dt_nascimento, " + _
                     "case when fl_sexo = true then 'F' else 'M' end fl_sexo, " + _
                     "a.nr_cep, c.sg_uf, c.txt_cidade, a.txt_endereco, a.txt_bairro, a.txt_complemento, a.txt_referencia " + _
                     "from oficina.tb_cliente a, oficina.tb_tipo_cliente b, oficina.v_cidades c " + _
                     " where a.tpo_cliente = b.cod_tipo and a.cod_cidade = c.cod_cidade " + IIf(TXT_NOME.Text <> Nothing, " and txt_cliente like '" + TXT_NOME.Text.Trim + "%' ", "") + _
                     IIf(CBO_TIPO_CLIENTE.Text <> "TODOS", " and tpo_cliente in (select cod_tipo from oficina.tb_tipo_cliente where txt_tipo = '" + CBO_TIPO_CLIENTE.Text.Trim + "') ", ""), Me, DTG_CLIENTES)
    End Sub

    Private Sub DTG_CLIENTES_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DTG_CLIENTES.CellClick
        If e.ColumnIndex = DTG_CLIENTES.Columns("cod_cliente").Index And Not IsDBNull(DTG_CLIENTES.CurrentRow.Cells("cod_cliente").Value) Then
            'MessageBox.Show(DTG_GRUPOS.CurrentRow.Cells("cod_grupo").Value)

            FCadastroCliente.TXT_COD_CLIENTE.Text = DTG_CLIENTES.CurrentRow.Cells("cod_cliente").Value.ToString
            FCadastroCliente.TXT_NOME.Text = DTG_CLIENTES.CurrentRow.Cells("txt_cliente").Value
            FCadastroCliente.DT_NASCIMENTO.Text = DTG_CLIENTES.CurrentRow.Cells("dt_nascimento").Value
            FCadastroCliente.RDO_MASCULINO.Checked = IIf(DTG_CLIENTES.CurrentRow.Cells("fl_sexo").Value = "M", True, False)
            FCadastroCliente.TXT_NR_CEP.Text = DTG_CLIENTES.CurrentRow.Cells("nr_cep").Value
            FCadastroCliente.txt_endereco.Text = DTG_CLIENTES.CurrentRow.Cells("txt_endereco").Value
            FCadastroCliente.txt_bairro.Text = DTG_CLIENTES.CurrentRow.Cells("txt_bairro").Value
            FCadastroCliente.txt_complemento.Text = DTG_CLIENTES.CurrentRow.Cells("txt_complemento").Value
            FCadastroCliente.txt_referencia.Text = DTG_CLIENTES.CurrentRow.Cells("txt_referencia").Value

            PreencheCBO("select txt_tipo from oficina.tb_tipo_cliente " + " order by txt_tipo ", _
                    "Tipo", "tb_tipo_cliente", "txt_tipo", FCadastroCliente.CBO_TIPO_CLIENTE, FCadastroCliente)

            PreencheCBO_UF(FCadastroCliente.CBO_UF, FCadastroCliente)

            FCadastroCliente.CBO_UF.SelectedIndex = _
               FCadastroCliente.CBO_UF.FindString(DTG_CLIENTES.CurrentRow.Cells("sg_uf").Value)

            PreencheCBO("SELECT TXT_CIDADE FROM OFICINA.V_CIDADES WHERE SG_UF = '" + FCadastroCliente.CBO_UF.Text + "' order by TXT_CIDADE ", _
                    "Cidades", "v_cidades", "TXT_CIDADE", FCadastroCliente.CBO_CIDADE, FCadastroCliente)

            FCadastroCliente.CBO_CIDADE.SelectedIndex = _
               FCadastroCliente.CBO_CIDADE.FindString(DTG_CLIENTES.CurrentRow.Cells("txt_cidade").Value)

            FCadastroCliente.CBO_TIPO_CLIENTE.SelectedIndex = _
                FCadastroCliente.CBO_TIPO_CLIENTE.FindString(DTG_CLIENTES.CurrentRow.Cells("tpo_cliente").Value)

            FCadastroCliente.CBO_TIPO_CLIENTE.Enabled = False

            FCadastroCliente.ShowDialog()
            FCadastroCliente.Dispose()
        End If
    End Sub

    Private Sub DTG_CLIENTES_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DTG_CLIENTES.CellContentClick

    End Sub
End Class