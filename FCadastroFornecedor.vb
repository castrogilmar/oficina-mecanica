﻿Imports Npgsql
Imports System.Data

Public Class FCadastroFornecedor

    Private Sub FCadastroFornecedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CADASTRO DE FORNECEDORES"


        If TXT_COD_FORNECEDOR.Text = Nothing Then
            PreencheCBO_UF(CBO_UF, Me)
        End If
    End Sub

    Private Function ManutencaoFornecedor() As Boolean
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction
        Dim vL_DataRead As NpgsqlDataReader

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = "select * from oficina.fct_manutencao_fornecedor(" + _
                                                 IIf(TXT_COD_FORNECEDOR.Text = Nothing, "0", TXT_COD_FORNECEDOR.Text) + ", " + _
                                                 "'" + TXT_FANTASIA.Text.Trim + "', " + _
                                                 "'" + TXT_RAZAO_SOCIAL.Text.Trim + "', " + _
                                                 "'" + TXT_NR_CEP.Text.Replace("_", "").Replace(".", "").Replace("-", "").Replace(",", "") + "', " + _
                                                 "'" + CBO_UF.Text + "', " + _
                                                 "'" + CBO_CIDADE.Text + "', " + _
                                                 "'" + txt_endereco.Text + "', " + _
                                                 "'" + txt_bairro.Text + "', " + _
                                                 "'" + IIf(txt_complemento.Text = Nothing, " ", txt_complemento.Text) + "', " + _
                                                 "'" + IIf(txt_referencia.Text = Nothing, " ", txt_referencia.Text) + "')"


                    vL_DataRead = .ExecuteReader()
                End With

                vL_Trans.Commit()
                vL_DataRead.Read()

                MessageBox.Show(vL_DataRead.Item(0).ToString.Trim, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                ManutencaoFornecedor = True
            Catch ex As Exception
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                vL_Trans.Rollback()
                ManutencaoFornecedor = False
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            ManutencaoFornecedor = False
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Function

    Private Function RemoveFornecedor() As Boolean
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction
        'Dim vL_DataRead As NpgsqlDataReader

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = "delete from oficina.tb_fornecedor where cod_fornecedor =" + TXT_COD_FORNECEDOR.Text
                    'vL_DataRead = .ExecuteReader()
                    .ExecuteNonQuery()
                End With

                vL_Trans.Commit()
                'vL_DataRead.Read()

                MessageBox.Show("Exclusão realizada com sucesso!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                RemoveFornecedor = True
            Catch ex As Exception
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                vL_Trans.Rollback()
                RemoveFornecedor = False
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            RemoveFornecedor = False
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Function

    Private Sub TXT_NR_CEP_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TXT_NR_CEP.KeyPress
        e.Handled = Not Char.IsDigit(e.KeyChar) And Not Char.IsControl(e.KeyChar)
    End Sub

    Private Sub CBO_UF_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBO_UF.SelectedIndexChanged
        PreencheCBO("SELECT TXT_CIDADE FROM OFICINA.V_CIDADES WHERE SG_UF = '" + CBO_UF.Text + "' order by TXT_CIDADE ", _
                    "Cidades", "v_cidades", "TXT_CIDADE", CBO_CIDADE, Me)
    End Sub

    Private Sub BUT_SALVAR_Click(sender As Object, e As EventArgs) Handles BUT_SALVAR.Click
        If TXT_RAZAO_SOCIAL.Text = Nothing Then
            MessageBox.Show("Informe a razão social da empresa!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TXT_RAZAO_SOCIAL.Focus()
            Exit Sub
        End If

        If TXT_FANTASIA.Text = Nothing Then
            MessageBox.Show("Informe o nome fantasia da empresa!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TXT_FANTASIA.Focus()
            Exit Sub
        End If

        If TXT_NR_CEP.Text.Replace("-", "").Replace(".", "").Replace("_", "").Replace(" ", "") = "".Replace(",", "") Then
            MessageBox.Show("Informe o CEP do endereço do fornecedor!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TXT_NR_CEP.Focus()
            Exit Sub
        End If

        If CBO_UF.Text = Nothing Then
            MessageBox.Show("Informe a UF do endereço do fornecedor!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            CBO_UF.Focus()
            Exit Sub
        End If

        If CBO_CIDADE.Text = Nothing Then
            MessageBox.Show("Informe a cidade do endereço do fornecedor!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            CBO_CIDADE.Focus()
            Exit Sub
        End If

        If txt_endereco.Text = Nothing Then
            MessageBox.Show("Informe o endereço do fornecedor!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txt_endereco.Focus()
            Exit Sub
        End If

        If txt_bairro.Text = Nothing Then
            MessageBox.Show("Informe o bairro do endereço do fornecedor!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txt_bairro.Focus()
            Exit Sub
        End If

        If ManutencaoFornecedor() = True Then
            Me.Close()
            Me.Dispose()
        End If
    End Sub

    Private Sub BUT_EXCLUIR_Click(sender As Object, e As EventArgs) Handles BUT_EXCLUIR.Click
        If RemoveFornecedor() = True Then
            Me.Close()
        End If
    End Sub
End Class