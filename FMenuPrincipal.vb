﻿Public Class FMenuPrincipal

    Private Sub FMenuPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo
    End Sub

    Private Sub GRUPOSToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GRUPOSToolStripMenuItem.Click
        FCadastroGrupos.ShowDialog()
        FCadastroGrupos.Dispose()
    End Sub

    Private Sub PRODUTOSSERVIÇOSToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PRODUTOSSERVIÇOSToolStripMenuItem.Click
        FCadastroProduto.ShowDialog()
        FCadastroProduto.Dispose()
    End Sub

    Private Sub GRUPOSToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles GRUPOSToolStripMenuItem1.Click
        FConsultaGrupos.ShowDialog()
        FConsultaGrupos.Dispose()
    End Sub

    Private Sub PRODUTOSSERVIÇOSToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles PRODUTOSSERVIÇOSToolStripMenuItem1.Click
        FConsultaProdutos.ShowDialog()
        FConsultaProdutos.Dispose()
    End Sub

    Private Sub ORDEMDESERVIÇOSToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ORDEMDESERVIÇOSToolStripMenuItem.Click
        FCadastroOrdemServico.ShowDialog()
        FCadastroOrdemServico.Dispose()

    End Sub

    Private Sub PEDIDOToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PEDIDOToolStripMenuItem.Click
        FCadastroPedido.ShowDialog()
        FCadastroPedido.Dispose()

    End Sub

    
    Private Sub CLIENTEToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CLIENTEToolStripMenuItem.Click
        FCadastroTipoCliente.ShowDialog()
        FCadastroTipoCliente.Dispose()
    End Sub

    Private Sub CLIENTEToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles CLIENTEToolStripMenuItem1.Click
        FConsultaCliente.ShowDialog()
        FConsultaCliente.Dispose()
    End Sub

    Private Sub CLIENTESToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CLIENTESToolStripMenuItem.Click
        FConsultaTipoCliente.ShowDialog()
        FConsultaTipoCliente.Dispose()
    End Sub

    Private Sub TIPOSDEDOCUMENTOToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TIPOSDEDOCUMENTOToolStripMenuItem.Click
        FConsultaTipoDocumento.ShowDialog()
        FConsultaTipoDocumento.Dispose()
    End Sub

    Private Sub TIPOSDEDOCUMENTOToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles TIPOSDEDOCUMENTOToolStripMenuItem1.Click
        FCadastroTipoDocumento.ShowDialog()
        FCadastroTipoDocumento.Dispose()
    End Sub

    Private Sub CLIENTESToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles CLIENTESToolStripMenuItem1.Click
        FCadastroCliente.ShowDialog()
        FCadastroCliente.Dispose()
    End Sub

    Private Sub FUNCIONARIOToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FUNCIONARIOToolStripMenuItem.Click
        FCadastroFuncionario.ShowDialog()
        FCadastroFuncionario.Dispose()
    End Sub

    Private Sub CARGOToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CARGOToolStripMenuItem.Click
        FCadastroCargo.ShowDialog()
        FCadastroCargo.Dispose()
    End Sub

    Private Sub SENHAToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SENHAToolStripMenuItem.Click
        FCadastroSenha.ShowDialog()
        FCadastroSenha.Dispose()
    End Sub

    Private Sub FORNECEDORToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FORNECEDORToolStripMenuItem.Click
        FCadastroFornecedor.ShowDialog()
        FCadastroFornecedor.Dispose()
    End Sub

    Private Sub FORNECEDORToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles FORNECEDORToolStripMenuItem1.Click
        FConsultaFornecedor.ShowDialog()
        FConsultaFornecedor.Dispose()
    End Sub
End Class