﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FCadrastroPedidoCliente
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FCadrastroPedidoCliente))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.BUT_SALVAR = New System.Windows.Forms.Button()
        Me.BUT_EXCLUIR = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label1.Location = New System.Drawing.Point(8, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 18)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "C.P.F."
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.TextBox1.Location = New System.Drawing.Point(68, 16)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(350, 26)
        Me.TextBox1.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(127, 18)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Selecionar Cliente"
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(11, 66)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(626, 28)
        Me.ComboBox1.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Label3.Location = New System.Drawing.Point(8, 113)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(75, 18)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Vendedor:"
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(11, 144)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(626, 28)
        Me.ComboBox2.TabIndex = 5
        '
        'BUT_SALVAR
        '
        Me.BUT_SALVAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_SALVAR.Image = Global.oficina_mecanica.My.Resources.Resources.Hopstarter_Sleek_Xp_Basic_Ok
        Me.BUT_SALVAR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_SALVAR.Location = New System.Drawing.Point(203, 196)
        Me.BUT_SALVAR.Margin = New System.Windows.Forms.Padding(4)
        Me.BUT_SALVAR.Name = "BUT_SALVAR"
        Me.BUT_SALVAR.Size = New System.Drawing.Size(117, 57)
        Me.BUT_SALVAR.TabIndex = 21
        Me.BUT_SALVAR.Text = "NOVO"
        Me.BUT_SALVAR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_SALVAR.UseVisualStyleBackColor = True
        '
        'BUT_EXCLUIR
        '
        Me.BUT_EXCLUIR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_EXCLUIR.Image = CType(resources.GetObject("BUT_EXCLUIR.Image"), System.Drawing.Image)
        Me.BUT_EXCLUIR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_EXCLUIR.Location = New System.Drawing.Point(328, 196)
        Me.BUT_EXCLUIR.Margin = New System.Windows.Forms.Padding(4)
        Me.BUT_EXCLUIR.Name = "BUT_EXCLUIR"
        Me.BUT_EXCLUIR.Size = New System.Drawing.Size(121, 57)
        Me.BUT_EXCLUIR.TabIndex = 22
        Me.BUT_EXCLUIR.Text = "CANCELAR"
        Me.BUT_EXCLUIR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_EXCLUIR.UseVisualStyleBackColor = True
        '
        'FCadrastroPedidoCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(650, 272)
        Me.Controls.Add(Me.BUT_SALVAR)
        Me.Controls.Add(Me.BUT_EXCLUIR)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FCadrastroPedidoCliente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CADASTRO DE PEDIDO"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents ComboBox2 As ComboBox
    Friend WithEvents BUT_SALVAR As Button
    Friend WithEvents BUT_EXCLUIR As Button
End Class
