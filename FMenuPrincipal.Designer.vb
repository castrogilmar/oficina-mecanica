﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FMenuPrincipal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FMenuPrincipal))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.CADASTROSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GRUPOSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PRODUTOSSERVIÇOSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ORDEMDESERVIÇOSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PEDIDOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CLIENTEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TIPOSDEDOCUMENTOToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CLIENTESToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FUNCIONARIOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CARGOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SENHAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FORNECEDORToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CONSULTASToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GRUPOSToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PRODUTOSSERVIÇOSToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CLIENTESToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CLIENTEToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TIPOSDEDOCUMENTOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FORNECEDORToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CADASTROSToolStripMenuItem, Me.CONSULTASToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(715, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'CADASTROSToolStripMenuItem
        '
        Me.CADASTROSToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GRUPOSToolStripMenuItem, Me.PRODUTOSSERVIÇOSToolStripMenuItem, Me.ORDEMDESERVIÇOSToolStripMenuItem, Me.PEDIDOToolStripMenuItem, Me.CLIENTEToolStripMenuItem, Me.TIPOSDEDOCUMENTOToolStripMenuItem1, Me.CLIENTESToolStripMenuItem1, Me.FUNCIONARIOToolStripMenuItem, Me.CARGOToolStripMenuItem, Me.SENHAToolStripMenuItem, Me.FORNECEDORToolStripMenuItem})
        Me.CADASTROSToolStripMenuItem.Name = "CADASTROSToolStripMenuItem"
        Me.CADASTROSToolStripMenuItem.Size = New System.Drawing.Size(86, 20)
        Me.CADASTROSToolStripMenuItem.Text = "CADASTROS"
        '
        'GRUPOSToolStripMenuItem
        '
        Me.GRUPOSToolStripMenuItem.Name = "GRUPOSToolStripMenuItem"
        Me.GRUPOSToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.GRUPOSToolStripMenuItem.Text = "GRUPOS"
        '
        'PRODUTOSSERVIÇOSToolStripMenuItem
        '
        Me.PRODUTOSSERVIÇOSToolStripMenuItem.Name = "PRODUTOSSERVIÇOSToolStripMenuItem"
        Me.PRODUTOSSERVIÇOSToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.PRODUTOSSERVIÇOSToolStripMenuItem.Text = "PRODUTOS/SERVIÇOS"
        '
        'ORDEMDESERVIÇOSToolStripMenuItem
        '
        Me.ORDEMDESERVIÇOSToolStripMenuItem.Name = "ORDEMDESERVIÇOSToolStripMenuItem"
        Me.ORDEMDESERVIÇOSToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.ORDEMDESERVIÇOSToolStripMenuItem.Text = "ORDEM DE SERVIÇOS"
        '
        'PEDIDOToolStripMenuItem
        '
        Me.PEDIDOToolStripMenuItem.Name = "PEDIDOToolStripMenuItem"
        Me.PEDIDOToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.PEDIDOToolStripMenuItem.Text = "PEDIDO"
        '
        'CLIENTEToolStripMenuItem
        '
        Me.CLIENTEToolStripMenuItem.Name = "CLIENTEToolStripMenuItem"
        Me.CLIENTEToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.CLIENTEToolStripMenuItem.Text = "TIPOS DE CLIENTES"
        '
        'TIPOSDEDOCUMENTOToolStripMenuItem1
        '
        Me.TIPOSDEDOCUMENTOToolStripMenuItem1.Name = "TIPOSDEDOCUMENTOToolStripMenuItem1"
        Me.TIPOSDEDOCUMENTOToolStripMenuItem1.Size = New System.Drawing.Size(201, 22)
        Me.TIPOSDEDOCUMENTOToolStripMenuItem1.Text = "TIPOS DE DOCUMENTO"
        '
        'CLIENTESToolStripMenuItem1
        '
        Me.CLIENTESToolStripMenuItem1.Name = "CLIENTESToolStripMenuItem1"
        Me.CLIENTESToolStripMenuItem1.Size = New System.Drawing.Size(201, 22)
        Me.CLIENTESToolStripMenuItem1.Text = "CLIENTES"
        '
        'FUNCIONARIOToolStripMenuItem
        '
        Me.FUNCIONARIOToolStripMenuItem.Name = "FUNCIONARIOToolStripMenuItem"
        Me.FUNCIONARIOToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.FUNCIONARIOToolStripMenuItem.Text = "FUNCIONARIO"
        '
        'CARGOToolStripMenuItem
        '
        Me.CARGOToolStripMenuItem.Name = "CARGOToolStripMenuItem"
        Me.CARGOToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.CARGOToolStripMenuItem.Text = "CARGO"
        '
        'SENHAToolStripMenuItem
        '
        Me.SENHAToolStripMenuItem.Name = "SENHAToolStripMenuItem"
        Me.SENHAToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.SENHAToolStripMenuItem.Text = "SENHA"
        '
        'FORNECEDORToolStripMenuItem
        '
        Me.FORNECEDORToolStripMenuItem.Name = "FORNECEDORToolStripMenuItem"
        Me.FORNECEDORToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.FORNECEDORToolStripMenuItem.Text = "FORNECEDOR"
        '
        'CONSULTASToolStripMenuItem
        '
        Me.CONSULTASToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GRUPOSToolStripMenuItem1, Me.PRODUTOSSERVIÇOSToolStripMenuItem1, Me.CLIENTESToolStripMenuItem, Me.CLIENTEToolStripMenuItem1, Me.TIPOSDEDOCUMENTOToolStripMenuItem, Me.FORNECEDORToolStripMenuItem1})
        Me.CONSULTASToolStripMenuItem.Name = "CONSULTASToolStripMenuItem"
        Me.CONSULTASToolStripMenuItem.Size = New System.Drawing.Size(86, 20)
        Me.CONSULTASToolStripMenuItem.Text = "CONSULTAS"
        '
        'GRUPOSToolStripMenuItem1
        '
        Me.GRUPOSToolStripMenuItem1.Name = "GRUPOSToolStripMenuItem1"
        Me.GRUPOSToolStripMenuItem1.Size = New System.Drawing.Size(201, 22)
        Me.GRUPOSToolStripMenuItem1.Text = "GRUPOS"
        '
        'PRODUTOSSERVIÇOSToolStripMenuItem1
        '
        Me.PRODUTOSSERVIÇOSToolStripMenuItem1.Name = "PRODUTOSSERVIÇOSToolStripMenuItem1"
        Me.PRODUTOSSERVIÇOSToolStripMenuItem1.Size = New System.Drawing.Size(201, 22)
        Me.PRODUTOSSERVIÇOSToolStripMenuItem1.Text = "PRODUTOS/SERVIÇOS"
        '
        'CLIENTESToolStripMenuItem
        '
        Me.CLIENTESToolStripMenuItem.Name = "CLIENTESToolStripMenuItem"
        Me.CLIENTESToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.CLIENTESToolStripMenuItem.Text = "TIPOS DE CLIENTE"
        '
        'CLIENTEToolStripMenuItem1
        '
        Me.CLIENTEToolStripMenuItem1.Name = "CLIENTEToolStripMenuItem1"
        Me.CLIENTEToolStripMenuItem1.Size = New System.Drawing.Size(201, 22)
        Me.CLIENTEToolStripMenuItem1.Text = "CLIENTE"
        '
        'TIPOSDEDOCUMENTOToolStripMenuItem
        '
        Me.TIPOSDEDOCUMENTOToolStripMenuItem.Name = "TIPOSDEDOCUMENTOToolStripMenuItem"
        Me.TIPOSDEDOCUMENTOToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.TIPOSDEDOCUMENTOToolStripMenuItem.Text = "TIPOS DE DOCUMENTO"
        '
        'FORNECEDORToolStripMenuItem1
        '
        Me.FORNECEDORToolStripMenuItem1.Name = "FORNECEDORToolStripMenuItem1"
        Me.FORNECEDORToolStripMenuItem1.Size = New System.Drawing.Size(201, 22)
        Me.FORNECEDORToolStripMenuItem1.Text = "FORNECEDOR"
        '
        'FMenuPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(715, 447)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FMenuPrincipal"
        Me.Text = "FMenuPrincipal"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents CADASTROSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GRUPOSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PRODUTOSSERVIÇOSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CONSULTASToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GRUPOSToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PRODUTOSSERVIÇOSToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ORDEMDESERVIÇOSToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PEDIDOToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CLIENTESToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CLIENTEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CLIENTEToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TIPOSDEDOCUMENTOToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TIPOSDEDOCUMENTOToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CLIENTESToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FUNCIONARIOToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CARGOToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SENHAToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FORNECEDORToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FORNECEDORToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
End Class
