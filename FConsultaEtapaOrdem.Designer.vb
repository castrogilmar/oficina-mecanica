﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FConsultaEtapaOrdem
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TXT_DESCRICAO = New System.Windows.Forms.TextBox()
        Me.DTG_TIPOS_CLIENTE = New System.Windows.Forms.DataGridView()
        Me.BUT_CONSULTA = New System.Windows.Forms.Button()
        Me.BUT_NOVO = New System.Windows.Forms.Button()
        Me.cod_etapa = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.txt_tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DTG_TIPOS_CLIENTE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TXT_DESCRICAO)
        Me.GroupBox1.Controls.Add(Me.BUT_CONSULTA)
        Me.GroupBox1.Controls.Add(Me.BUT_NOVO)
        Me.GroupBox1.Location = New System.Drawing.Point(2, 2)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1176, 100)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 20)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(138, 18)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Descrição da Etapa"
        '
        'TXT_DESCRICAO
        '
        Me.TXT_DESCRICAO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_DESCRICAO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_DESCRICAO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_DESCRICAO.Location = New System.Drawing.Point(12, 42)
        Me.TXT_DESCRICAO.Margin = New System.Windows.Forms.Padding(4)
        Me.TXT_DESCRICAO.Name = "TXT_DESCRICAO"
        Me.TXT_DESCRICAO.Size = New System.Drawing.Size(808, 26)
        Me.TXT_DESCRICAO.TabIndex = 4
        '
        'DTG_TIPOS_CLIENTE
        '
        Me.DTG_TIPOS_CLIENTE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DTG_TIPOS_CLIENTE.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cod_etapa, Me.txt_tipo})
        Me.DTG_TIPOS_CLIENTE.Location = New System.Drawing.Point(2, 109)
        Me.DTG_TIPOS_CLIENTE.Margin = New System.Windows.Forms.Padding(4)
        Me.DTG_TIPOS_CLIENTE.Name = "DTG_TIPOS_CLIENTE"
        Me.DTG_TIPOS_CLIENTE.Size = New System.Drawing.Size(1172, 550)
        Me.DTG_TIPOS_CLIENTE.TabIndex = 13
        '
        'BUT_CONSULTA
        '
        Me.BUT_CONSULTA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_CONSULTA.Image = Global.oficina_mecanica.My.Resources.Resources.Visualpharm_Must_Have_Search
        Me.BUT_CONSULTA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_CONSULTA.Location = New System.Drawing.Point(1017, 27)
        Me.BUT_CONSULTA.Margin = New System.Windows.Forms.Padding(4)
        Me.BUT_CONSULTA.Name = "BUT_CONSULTA"
        Me.BUT_CONSULTA.Size = New System.Drawing.Size(141, 57)
        Me.BUT_CONSULTA.TabIndex = 6
        Me.BUT_CONSULTA.Text = "CONSULTAR"
        Me.BUT_CONSULTA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_CONSULTA.UseVisualStyleBackColor = True
        '
        'BUT_NOVO
        '
        Me.BUT_NOVO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_NOVO.Image = Global.oficina_mecanica.My.Resources.Resources.Visualpharm_Must_Have_Add
        Me.BUT_NOVO.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_NOVO.Location = New System.Drawing.Point(849, 27)
        Me.BUT_NOVO.Margin = New System.Windows.Forms.Padding(4)
        Me.BUT_NOVO.Name = "BUT_NOVO"
        Me.BUT_NOVO.Size = New System.Drawing.Size(160, 57)
        Me.BUT_NOVO.TabIndex = 5
        Me.BUT_NOVO.Text = "NOVO ETAPA"
        Me.BUT_NOVO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_NOVO.UseVisualStyleBackColor = True
        '
        'cod_etapa
        '
        Me.cod_etapa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.cod_etapa.DataPropertyName = "cod_etapa"
        Me.cod_etapa.HeaderText = "CÓDIGO"
        Me.cod_etapa.Name = "cod_etapa"
        Me.cod_etapa.ReadOnly = True
        Me.cod_etapa.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cod_etapa.Width = 69
        '
        'txt_tipo
        '
        Me.txt_tipo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_tipo.DataPropertyName = "txt_tipo"
        Me.txt_tipo.HeaderText = "DESCRIÇÃO"
        Me.txt_tipo.Name = "txt_tipo"
        Me.txt_tipo.ReadOnly = True
        Me.txt_tipo.Width = 116
        '
        'FConsultaEtapaOrdem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1183, 666)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.DTG_TIPOS_CLIENTE)
        Me.Name = "FConsultaEtapaOrdem"
        Me.Text = "FConsultaEtapaOrdem"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DTG_TIPOS_CLIENTE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TXT_DESCRICAO As TextBox
    Friend WithEvents BUT_CONSULTA As Button
    Friend WithEvents BUT_NOVO As Button
    Friend WithEvents DTG_TIPOS_CLIENTE As DataGridView
    Friend WithEvents cod_etapa As DataGridViewButtonColumn
    Friend WithEvents txt_tipo As DataGridViewTextBoxColumn
End Class
