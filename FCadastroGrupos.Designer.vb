﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FCadastroGrupos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FCadastroGrupos))
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TXT_DESCRICAO = New System.Windows.Forms.TextBox()
        Me.BUT_SALVAR = New System.Windows.Forms.Button()
        Me.BUT_EXCLUIR = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RDO_SERVICO = New System.Windows.Forms.RadioButton()
        Me.RDO_PRODUTO = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TXT_COD_GRUPO = New System.Windows.Forms.TextBox()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(16, 60)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(143, 18)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Descrição do Grupo"
        '
        'TXT_DESCRICAO
        '
        Me.TXT_DESCRICAO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_DESCRICAO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_DESCRICAO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_DESCRICAO.Location = New System.Drawing.Point(16, 82)
        Me.TXT_DESCRICAO.Margin = New System.Windows.Forms.Padding(4)
        Me.TXT_DESCRICAO.Name = "TXT_DESCRICAO"
        Me.TXT_DESCRICAO.Size = New System.Drawing.Size(808, 26)
        Me.TXT_DESCRICAO.TabIndex = 18
        '
        'BUT_SALVAR
        '
        Me.BUT_SALVAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_SALVAR.Image = Global.oficina_mecanica.My.Resources.Resources.Hopstarter_Sleek_Xp_Basic_Ok
        Me.BUT_SALVAR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_SALVAR.Location = New System.Drawing.Point(299, 129)
        Me.BUT_SALVAR.Margin = New System.Windows.Forms.Padding(4)
        Me.BUT_SALVAR.Name = "BUT_SALVAR"
        Me.BUT_SALVAR.Size = New System.Drawing.Size(117, 57)
        Me.BUT_SALVAR.TabIndex = 19
        Me.BUT_SALVAR.Text = "SALVAR"
        Me.BUT_SALVAR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_SALVAR.UseVisualStyleBackColor = True
        '
        'BUT_EXCLUIR
        '
        Me.BUT_EXCLUIR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_EXCLUIR.Image = CType(resources.GetObject("BUT_EXCLUIR.Image"), System.Drawing.Image)
        Me.BUT_EXCLUIR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_EXCLUIR.Location = New System.Drawing.Point(424, 129)
        Me.BUT_EXCLUIR.Margin = New System.Windows.Forms.Padding(4)
        Me.BUT_EXCLUIR.Name = "BUT_EXCLUIR"
        Me.BUT_EXCLUIR.Size = New System.Drawing.Size(121, 57)
        Me.BUT_EXCLUIR.TabIndex = 20
        Me.BUT_EXCLUIR.Text = "EXCLUIR"
        Me.BUT_EXCLUIR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_EXCLUIR.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RDO_SERVICO)
        Me.GroupBox2.Controls.Add(Me.RDO_PRODUTO)
        Me.GroupBox2.Location = New System.Drawing.Point(195, 15)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(200, 53)
        Me.GroupBox2.TabIndex = 21
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Tipo de Grupo"
        '
        'RDO_SERVICO
        '
        Me.RDO_SERVICO.AutoSize = True
        Me.RDO_SERVICO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RDO_SERVICO.Location = New System.Drawing.Point(107, 22)
        Me.RDO_SERVICO.Margin = New System.Windows.Forms.Padding(4)
        Me.RDO_SERVICO.Name = "RDO_SERVICO"
        Me.RDO_SERVICO.Size = New System.Drawing.Size(79, 22)
        Me.RDO_SERVICO.TabIndex = 1
        Me.RDO_SERVICO.Text = "Serviço"
        Me.RDO_SERVICO.UseVisualStyleBackColor = True
        '
        'RDO_PRODUTO
        '
        Me.RDO_PRODUTO.AutoSize = True
        Me.RDO_PRODUTO.Checked = True
        Me.RDO_PRODUTO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RDO_PRODUTO.Location = New System.Drawing.Point(8, 22)
        Me.RDO_PRODUTO.Margin = New System.Windows.Forms.Padding(4)
        Me.RDO_PRODUTO.Name = "RDO_PRODUTO"
        Me.RDO_PRODUTO.Size = New System.Drawing.Size(82, 22)
        Me.RDO_PRODUTO.TabIndex = 1
        Me.RDO_PRODUTO.TabStop = True
        Me.RDO_PRODUTO.Text = "Produto"
        Me.RDO_PRODUTO.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 11)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 18)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Código"
        '
        'TXT_COD_GRUPO
        '
        Me.TXT_COD_GRUPO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_COD_GRUPO.Enabled = False
        Me.TXT_COD_GRUPO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_COD_GRUPO.Location = New System.Drawing.Point(20, 33)
        Me.TXT_COD_GRUPO.Margin = New System.Windows.Forms.Padding(4)
        Me.TXT_COD_GRUPO.Name = "TXT_COD_GRUPO"
        Me.TXT_COD_GRUPO.Size = New System.Drawing.Size(132, 26)
        Me.TXT_COD_GRUPO.TabIndex = 22
        '
        'FCadastroGrupos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(844, 201)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TXT_COD_GRUPO)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TXT_DESCRICAO)
        Me.Controls.Add(Me.BUT_SALVAR)
        Me.Controls.Add(Me.BUT_EXCLUIR)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FCadastroGrupos"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FCdastroGrupos"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TXT_DESCRICAO As System.Windows.Forms.TextBox
    Friend WithEvents BUT_SALVAR As System.Windows.Forms.Button
    Friend WithEvents BUT_EXCLUIR As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents RDO_SERVICO As System.Windows.Forms.RadioButton
    Friend WithEvents RDO_PRODUTO As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TXT_COD_GRUPO As System.Windows.Forms.TextBox
End Class
