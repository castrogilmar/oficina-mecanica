﻿Public Class FConsultaFornecedor

    Private Sub FConsultaFornecedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CONSULTA DE FORNECEDORES"
    End Sub

    Private Sub BUT_NOVO_Click(sender As Object, e As EventArgs) Handles BUT_NOVO.Click
        FCadastroFornecedor.ShowDialog()
        FCadastroFornecedor.Dispose()
    End Sub

    Private Sub BUT_CONSULTA_Click(sender As Object, e As EventArgs) Handles BUT_CONSULTA.Click
        PreencheGrid("tb_fornecedor", "select a.cod_fornecedor, a.txt_fantasia, a.txt_razao_social, " + _
                     "a.nr_cep, c.sg_uf, c.txt_cidade, a.txt_endereco, a.txt_bairro, a.txt_complemento, a.txt_referencia " + _
                     "from oficina.tb_fornecedor a, oficina.v_cidades c " + _
                     " where a.cod_cidade = c.cod_cidade " + IIf(TXT_NOME.Text <> Nothing, " and txt_fantasia like '" + TXT_NOME.Text.Trim + "%' ", ""), Me, DTG_FORNECEDORES)
    End Sub

    Private Sub DTG_FORNECEDORES_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DTG_FORNECEDORES.CellClick
        If e.ColumnIndex = DTG_FORNECEDORES.Columns("cod_fornecedor").Index And Not IsDBNull(DTG_FORNECEDORES.CurrentRow.Cells("cod_fornecedor").Value) Then
            'MessageBox.Show(DTG_GRUPOS.CurrentRow.Cells("cod_grupo").Value)

            FCadastroFornecedor.TXT_COD_FORNECEDOR.Text = DTG_FORNECEDORES.CurrentRow.Cells("cod_fornecedor").Value.ToString
            FCadastroFornecedor.TXT_FANTASIA.Text = DTG_FORNECEDORES.CurrentRow.Cells("txt_fantasia").Value
            FCadastroFornecedor.TXT_RAZAO_SOCIAL.Text = DTG_FORNECEDORES.CurrentRow.Cells("txt_razao_social").Value
            FCadastroFornecedor.TXT_NR_CEP.Text = DTG_FORNECEDORES.CurrentRow.Cells("nr_cep").Value
            FCadastroFornecedor.txt_endereco.Text = DTG_FORNECEDORES.CurrentRow.Cells("txt_endereco").Value
            FCadastroFornecedor.txt_bairro.Text = DTG_FORNECEDORES.CurrentRow.Cells("txt_bairro").Value
            FCadastroFornecedor.txt_complemento.Text = DTG_FORNECEDORES.CurrentRow.Cells("txt_complemento").Value
            FCadastroFornecedor.txt_referencia.Text = DTG_FORNECEDORES.CurrentRow.Cells("txt_referencia").Value

            PreencheCBO_UF(FCadastroFornecedor.CBO_UF, FCadastroFornecedor)

            FCadastroFornecedor.CBO_UF.SelectedIndex = _
               FCadastroFornecedor.CBO_UF.FindString(DTG_FORNECEDORES.CurrentRow.Cells("sg_uf").Value)

            PreencheCBO("SELECT TXT_CIDADE FROM OFICINA.V_CIDADES WHERE SG_UF = '" + FCadastroFornecedor.CBO_UF.Text + "' order by TXT_CIDADE ", _
                    "Cidades", "v_cidades", "TXT_CIDADE", FCadastroFornecedor.CBO_CIDADE, FCadastroFornecedor)

            FCadastroFornecedor.CBO_CIDADE.SelectedIndex = _
               FCadastroFornecedor.CBO_CIDADE.FindString(DTG_FORNECEDORES.CurrentRow.Cells("txt_cidade").Value)

            FCadastroFornecedor.ShowDialog()
            FCadastroFornecedor.Dispose()
        End If
    End Sub
End Class