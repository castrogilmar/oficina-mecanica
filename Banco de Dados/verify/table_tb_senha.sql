-- Verify oficina:table_tb_senha to pg

BEGIN;

SELECT EXISTS (
   SELECT 1
   FROM   information_schema.tables 
   WHERE  table_schema = 'oficina'
   AND    table_name = 'tb_senha'
   );

ROLLBACK;