-- Deploy oficina:table_tb_senha to pg

BEGIN;

SELECT 1
   FROM   information_schema.tables 
   WHERE  table_schema = 'oficina'
   AND    table_name = 'tb_localidade_dne'
   ;

ROLLBACK;