-- Verify oficina:tabela_tb_cargo to pg

BEGIN;

SELECT 1
   FROM   information_schema.VIEWS 
   WHERE  table_schema = 'oficina'
   AND    table_name = 'v_cidades';
  
ROLLBACK;