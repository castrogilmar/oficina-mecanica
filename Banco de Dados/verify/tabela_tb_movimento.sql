-- Verify [% project % ]:tabela_tb_movimento to pg

BEGIN;

SELECT 1
   FROM   information_schema.tables 
   WHERE  table_schema = 'oficina'
   AND    table_name = 'tb_movimento';

ROLLBACK;