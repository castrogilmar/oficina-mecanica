-- Verify oficina:campo_tb_funcionario_cod_cargo to pg

BEGIN;

select column_name from information_schema.columns
      where table_schema = 'oficina' and table_name = 'tb_funcionario' and column_name = 'cod_cargo';

ROLLBACK;