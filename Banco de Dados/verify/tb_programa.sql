-- Verify oficina:tb_programa to pg

BEGIN;

SELECT 1
   FROM   information_schema.tables 
   WHERE  table_schema = 'oficina'
   AND    table_name = 'tb_programa';

ROLLBACK;