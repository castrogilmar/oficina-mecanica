-- Verify oficina:oficina_tb_senha_cod_senha_seq to pg

BEGIN;

SELECT EXISTS (
   SELECT 1
   FROM   information_schema.sequences 
   WHERE  sequence_schema = 'oficina'
   AND    sequence_name = 'tb_senha_cod_senha_seq'
   );

ROLLBACK;