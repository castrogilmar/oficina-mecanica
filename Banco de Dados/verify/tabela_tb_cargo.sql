-- Verify oficina:tabela_tb_cargo to pg

BEGIN;

SELECT 1
   FROM   information_schema.tables 
   WHERE  table_schema = 'oficina'
   AND    table_name = 'tb_cargo';

ROLLBACK;