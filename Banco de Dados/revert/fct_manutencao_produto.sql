-- Revert oficina:table_tb_senha to pg

BEGIN;

  DROP FUNCTION oficina.fct_manutencao_produto(integer, character varying, character varying, character varying, numeric, numeric, numeric);

COMMIT;