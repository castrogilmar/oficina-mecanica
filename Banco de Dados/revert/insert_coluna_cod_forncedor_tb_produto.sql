-- Revert oficina:insert_coluna_cod_forncedor_tb_produto to pg

BEGIN;

alter table oficina.tb_produto drop column cod_fornecedor;

DROP INDEX oficina.idx_tb_produto_cod_fornecedor;

COMMIT;