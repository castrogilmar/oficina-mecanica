-- Deploy oficina:alter_fct_manutencao_produto to pg

BEGIN;

DROP FUNCTION oficina.fct_manutencao_produto(integer, character varying, character varying, character varying, numeric, numeric, numeric);

CREATE OR REPLACE FUNCTION oficina.fct_manutencao_produto(
    v_cod_produto integer,
    v_tpo_produto character varying,
    v_txt_grupo character varying,
    v_txt_descricao character varying,
    v_vlr_aprazo numeric,
    v_vlr_avista numeric,
    v_vlr_custo numeric,
    v_txt_fantasia character varying)
  RETURNS text AS
$BODY$
declare
   v_retorno text;
   v_cod_grupo integer;
   v_cod_fornecedor integer;
begin
   v_cod_grupo=(select cod_grupo from oficina.tb_grupo a where a.txt_grupo = v_TXT_grupo);
   v_cod_fornecedor=(select cod_fornecedor from oficina.tb_fornecedor a where a.txt_fantasia = v_txt_fantasia);

   if coalesce(v_cod_produto,0) = 0 then
      begin
         insert into oficina.tb_Produto (cod_grupo, dt_cadastro, tpo_produto, txt_produto, vlr_aprazo, vlr_avista, vlr_custo, cod_fornecedor)
             select v_cod_grupo, current_date, left(v_tpo_produto,1), v_txt_descricao, v_vlr_aprazo, v_vlr_avista, v_vlr_custo, v_cod_fornecedor
             returning 'PRODUTO CRIADO COM SUCESSO CODIGO [ ' || cod_produto::text || ']' into v_retorno;
      exception
         when others then
            raise exception '%', SQLERRM ;
            
      end;      
   else
      begin
         update oficina.tb_Produto a set dt_cadastro=txt_produto=v_txt_descricao, a.vlr_aprazo=v_vlr_aprazo, 
                                         a.vlr_avista=v_vlr_avista, a.vlr_custo=v_vlr_custo, a.cod_fornecedor=v_cod_fornecedor
             where a.cod_produto = v_cod_produto 
             returning 'MANUTENCAO NO PRODUTO CODIGO [ ' || cod_produto::text || '] REALIZADA COM SUCESSO' into v_retorno;
      exception
         when others then
            raise exception '%', SQLERRM ;
      end;
   end if; 
   return v_retorno;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION oficina.fct_manutencao_produto(integer, character varying, character varying, character varying, numeric, numeric, numeric, character varying)
  OWNER TO oficina;


COMMIT;