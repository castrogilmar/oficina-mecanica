-- Deploy oficina:table_tb_senha to pg

BEGIN;

  CREATE TABLE oficina.tb_localidade_dne
(
  loc_nu character varying(8),
  ufe_sg character varying(2),
  loc_no character varying(400),
  cep character varying(8),
  loc_in_sit character varying(1),
  loc_in_tipo_loc character varying(1),
  loc_nu_sub character varying(8),
  loc_no_abrev character varying(200),
  mun_nu character varying(8)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE oficina.tb_localidade_dne
  OWNER TO oficina;

COMMIT;