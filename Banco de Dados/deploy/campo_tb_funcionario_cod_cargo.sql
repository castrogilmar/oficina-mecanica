-- Deploy oficina:campo_tb_funcionario_cod_cargo to pg

BEGIN;

do $$
begin
   perform column_name from information_schema.columns
      where table_schema = 'oficina' and table_name = 'tb_funcionario' and column_name = 'cod_cargo';
   if not found then
      alter table oficina.tb_funcionario add column cod_cargo integer;
   end if;
end $$;

COMMIT;