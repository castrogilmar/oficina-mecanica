-- Deploy oficina:table_tb_senha to pg

BEGIN;

  CREATE OR REPLACE FUNCTION oficina.trg_fct_cliente()
  RETURNS trigger AS
$BODY$begin
   perform cod_cliente from oficina.tb_cliente
      where txt_cliente = new.txt_cliente and tpo_cliente = new.tpo_cliente and dt_nascimento = new.dt_nascimento;
   if found and (tg_op = 'INSERT' or (old.cod_cliente <> new.cod_cliente and tg_op = 'UPDATE')) then
      raise exception '%', 'CLIENTE JA CADASTRADO ANTERIORMENTE';
   end if; 
   return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION oficina.trg_fct_cliente()
  OWNER TO oficina;

COMMIT;