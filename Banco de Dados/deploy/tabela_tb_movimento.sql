-- Deploy [% project % ]:tabela_tb_movimento to pg

BEGIN;

-- Table: oficina.tb_movimento

-- DROP TABLE oficina.tb_movimento;

CREATE TABLE oficina.tb_movimento
(
  cod_produto bigserial NOT NULL,
  dt_movimento date,
  tp_movimento varchar(1),
  qt_movimento numeric(15,2),
  CONSTRAINT tb_movimento_pkey PRIMARY KEY (cod_produto, dt_movimento, tp_movimento)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE oficina.tb_movimento
  OWNER TO oficina;

-- Index: oficina.idx_tb_movimento_dt_movimento

-- DROP INDEX oficina.idx_tb_movimento_dt_movimento;

CREATE INDEX idx_tb_movimento_dt_movimento
  ON oficina.tb_movimento
  USING btree
  (dt_movimento);

-- Index: oficina.idx_tb_movimento_cod_produto

-- DROP INDEX oficina.idx_tb_movimento_cod_produto;

CREATE INDEX idx_tb_movimento_cod_produto
  ON oficina.tb_movimento
  USING btree
  (cod_produto);  

-- Function: oficina.trg_fct_cliente()

-- DROP FUNCTION oficina.trg_fct_cliente();

CREATE OR REPLACE FUNCTION oficina.trg_fct_movimento()
  RETURNS trigger AS
$BODY$begin
   begin
      insert into oficina.tb_estoque select new.cod_produto, current_date, new.qt_movimento * (case when new.tp_movimento = 'S' then -1 else 1 end);
   exception
      when foreign_key_violation then
         update oficina.tb_estoque set dt_atualizacao=current_date, qt_estoque = qt_estoque + (new.qt_movimento * (case when new.tp_movimento = 'S' then -1 else 1 end))
            where cod_produto = new.cod_Produto;
   end;   
   return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION oficina.trg_fct_movimento()
  OWNER TO oficina;

CREATE TRIGGER trg_movimento
  BEFORE INSERT OR UPDATE
  ON oficina.tb_movimento
  FOR EACH ROW
  EXECUTE PROCEDURE oficina.trg_fct_movimento();
  


COMMIT;