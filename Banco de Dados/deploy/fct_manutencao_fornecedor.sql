-- Deploy oficina:fct_manutencao_fornecedor to pg

BEGIN;


CREATE OR REPLACE FUNCTION oficina.fct_manutencao_fornecedor(
    v_cod_fornecedor integer,
    v_txt_fantasia character varying,
    v_txt_razao_social character varying,
    v_nr_cep character varying,
    v_sg_uf character varying,
    v_txt_cidade character varying,
    v_txt_endereco character varying,
    v_txt_bairro character varying,
    v_txt_complemento character varying,
    v_txt_referencia character varying)
  RETURNS text AS
$BODY$
declare
   v_retorno text;
   v_tpo_cliente integer;
   v_cod_cidade integer;
begin   
   v_cod_cidade=(select cod_cidade from oficina.v_cidades a where a.txt_cidade = v_txt_cidade);

   if coalesce(v_cod_fornecedor,0) = 0 then
      begin
         insert into oficina.tb_fornecedor (txt_fantasia, txt_razao_social, nr_cep, txt_endereco, txt_bairro, cod_cidade, txt_complemento,
                                         txt_referencia, dt_cadastro, sg_uf)                                         
             select v_txt_fantasia, v_txt_Razao_social, v_nr_cep, v_txt_endereco, v_txt_bairro, v_cod_cidade, coalesce(v_txt_complemento,''),
                    coalesce(v_txt_referencia,''), current_date, v_sg_uf
             returning 'FORNECEDOR CRIADO COM SUCESSO CODIGO [ ' || cod_fornecedor::text || ']!' into v_retorno;
      exception
         when others then
            raise exception '%', SQLERRM ;
            
      end;      
   else
      begin
         update oficina.tb_fornecedor set txt_fantasia=v_txt_fantasia, 
                                          txt_razao_social=v_txt_razao_social,
                                          nr_cep=v_nr_cep, 
                                          sg_uf=v_sg_uf,
                                          txt_endereco=v_txt_endereco,
                                          txt_bairro=v_txt_bairro, 
                                          cod_cidade=v_cod_cidade, 
                                          txt_complemento=v_txt_complemento,
                                          txt_referencia=v_txt_referencia                                     
             where cod_fornecedor = v_cod_fornecedor 
             returning 'MANUTENCAO NO FORNECEDOR CODIGO [ ' || cod_fornecedor::text || '] REALIZADA COM SUCESSO!' into v_retorno;
      exception
         when others then
            raise exception '%', SQLERRM ;
      end;
   end if; 
   return v_retorno;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION oficina.fct_manutencao_fornecedor(integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO oficina;


COMMIT;