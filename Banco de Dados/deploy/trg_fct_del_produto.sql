-- Revert oficina:table_tb_senha to pg

BEGIN;

  CREATE OR REPLACE FUNCTION oficina.trg_fct_del_produto()
  RETURNS trigger AS
$BODY$begin
   return old;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION oficina.trg_fct_del_produto()
  OWNER TO oficina;

COMMIT;