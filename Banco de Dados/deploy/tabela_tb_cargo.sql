-- Deploy oficina:tabela_tb_cargo to pg

BEGIN;

-- Table: oficina.tb_cargo

-- DROP TABLE oficina.tb_cargo;

CREATE TABLE oficina.tb_cargo
(
    cod_cargo bigint NOT NULL,
    txt_cargo character varying(500) COLLATE pg_catalog."default",
    CONSTRAINT tb_cargo_pkey PRIMARY KEY (cod_cargo)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE oficina.tb_cargo
    OWNER to oficina;

COMMIT;