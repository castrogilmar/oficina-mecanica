-- Verify oficina:table_tb_senha to pg

BEGIN;

  CREATE OR REPLACE FUNCTION oficina.fct_manutencao_tpo_documento(
    v_cod_documento integer,
    v_tpo_cliente character varying,
    v_txt_documento character varying)
  RETURNS text AS
$BODY$
declare
   v_retorno text;
   v_cod_tpo_cliente integer;
begin
   v_cod_tpo_cliente=(select cod_tipo from oficina.tb_tipo_cliente a where a.txt_tipo = v_tpo_cliente);

   if coalesce(v_cod_documento,0) = 0 then
      begin
         insert into oficina.tb_tipo_documento (tpo_cliente, txt_documento)
             select v_cod_tpo_cliente, v_txt_documento
             returning 'TIPO DE DOCUMENTO CRIADO COM SUCESSO CODIGO [ ' || cod_documento::text || ']!' into v_retorno;
      exception
         when others then
            raise exception '%', SQLERRM ;
            
      end;      
   else
      begin
         update oficina.tb_tipo_documento set txt_documento=v_txt_documento
             where cod_documento = v_cod_documento 
             returning 'MANUTENCAO NO TIPO DE DOCUMENTO CODIGO [ ' || cod_documento::text || '] REALIZADA COM SUCESSO!' into v_retorno;
      exception
         when others then
            raise exception '%', SQLERRM;
      end;
   end if; 
   return v_retorno;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION oficina.fct_manutencao_tpo_documento(integer, character varying, character varying)
  OWNER TO oficina;

COMMIT;