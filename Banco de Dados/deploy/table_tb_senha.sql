-- Deploy oficina:table_tb_senha to pg

BEGIN;

-- Table: oficina.tb_senha

-- DROP TABLE oficina.tb_senha;

CREATE TABLE oficina.tb_senha
(
    cod_senha bigint NOT NULL,
    cod_funcionario bigint NOT NULL,
    cod_classe character varying(200) COLLATE pg_catalog."default" NOT NULL,
    cod_programa bigint NOT NULL,
    dt_cadastro date,
    fl_ativo boolean NOT NULL,
    CONSTRAINT tb_senha_pkey PRIMARY KEY (cod_senha)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE oficina.tb_senha
    OWNER to oficina;
	
COMMIT;