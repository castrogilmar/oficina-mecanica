-- Deploy oficina:tb_programa to pg

BEGIN;

-- Table: oficina.tb_programa

-- DROP TABLE oficina.tb_programa;

CREATE TABLE oficina.tb_programa
(
    cod_programa bigint NOT NULL,
    txt_programa character varying(200) COLLATE pg_catalog."default",
    fl_ativo boolean NOT NULL,
    CONSTRAINT tb_programa_pkey PRIMARY KEY (cod_programa)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE oficina.tb_programa
    OWNER to oficina;

COMMIT;