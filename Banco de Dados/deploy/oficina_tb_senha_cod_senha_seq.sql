-- Deploy oficina:oficina_tb_senha_cod_senha_seq to pg

BEGIN;

CREATE SEQUENCE oficina.tb_senha_cod_senha_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE oficina.tb_senha_cod_senha_seq
    OWNER TO oficina;

COMMIT;