-- Deploy [% project % ]:tabela_tb_estoque to pg

BEGIN;

-- Table: oficina.tb_estoque

CREATE TABLE oficina.tb_estoque
(
  cod_produto bigserial NOT NULL,
  dt_atualizacao date,
  qt_estoque numeric(15,2),
  CONSTRAINT tb_estoque_pkey PRIMARY KEY (cod_produto, dt_atualizacao)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE oficina.tb_estoque
  OWNER TO oficina;

-- Index: oficina.idx_tb_estoque_dt_atualizacao

-- DROP INDEX oficina.idx_tb_estoque_dt_atualizacao;

CREATE INDEX idx_dt_atualizacao
  ON oficina.tb_estoque
  USING btree
  (dt_atualizacao);

-- Index: oficina.idx_tb_estoque_cd_produto

-- DROP INDEX oficina.idx_tb_estoque_cd_produto;

CREATE INDEX idx_tb_estoque_cd_produto
  ON oficina.tb_estoque
  USING btree
  (cod_produto);

COMMIT;