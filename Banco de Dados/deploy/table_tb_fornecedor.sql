-- Deploy oficina:table_tb_fornecedor to pg

BEGIN;

-- Table: oficina.tb_fornecedor

-- DROP TABLE oficina.tb_fornecedor;

CREATE TABLE oficina.tb_fornecedor
(
  cod_fornecedor bigserial NOT NULL,
  txt_razao_social character varying(200),
  txt_fantasia character varying(200),
  nr_cep character varying(8),
  txt_endereco character varying(200),
  txt_bairro character varying(100),
  cod_cidade integer NOT NULL,
  txt_complemento character varying(100),
  txt_referencia character varying(100),
  dt_cadastro date,
  txt_email text,
  sg_uf character varying(2),
  CONSTRAINT tb_fornecedor_pkey PRIMARY KEY (cod_fornecedor)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE oficina.tb_fornecedor
  OWNER TO oficina;

-- Index: oficina.idx_tb_fornecedor_cod_cidade

-- DROP INDEX oficina.idx_tb_fornecedor_cod_cidade;

CREATE INDEX idx_tb_fornecedor_cod_cidade
  ON oficina.tb_fornecedor
  USING btree
  (cod_cidade);

-- Index: oficina.idx_tb_fornecedor_txt_fantasia

-- DROP INDEX oficina.idx_tb_fornecedor_txt_fantasia;

CREATE INDEX idx_tb_fornecedor_txt_fantasia
  ON oficina.tb_fornecedor
  USING btree
  (txt_fantasia COLLATE pg_catalog."default");


-- Index: oficina.idx_tb_fornecedor_txt_razao_social

-- DROP INDEX oficina.idx_tb_fornecedor_txt_razao_social;

CREATE INDEX idx_tb_fornecedor_txt_razao_social
  ON oficina.tb_fornecedor
  USING btree
  (txt_razao_social COLLATE pg_catalog."default");  

-- Function: oficina.trg_fct_del_fornecedor()

-- DROP FUNCTION oficina.trg_fct_del_fornecedor();

CREATE OR REPLACE FUNCTION oficina.trg_fct_del_fornecedor()
  RETURNS trigger AS
$BODY$begin
   perform cod_produto from oficina.tb_produto
      where cod_fornecedor = old.cod_fornecedor;
   if found then
      raise exception '%', 'FORNECEDOR NAO PODE SER EXCLUIDO POIS EXISTEM PRODUTOS CADASTRADOS PARA O MESMO';
   end if; 
   return old;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION oficina.trg_fct_del_fornecedor()
  OWNER TO oficina;

-- Trigger: trg_del_fornecedor on oficina.tb_fornecedor

-- DROP TRIGGER trg_del_fornecedor ON oficina.tb_fornecedor;

CREATE TRIGGER trg_del_fornecedor
  BEFORE DELETE
  ON oficina.tb_fornecedor
  FOR EACH ROW
  EXECUTE PROCEDURE oficina.trg_fct_del_fornecedor();

COMMIT;