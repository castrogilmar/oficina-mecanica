-- Verify oficina:tabela_tb_cargo to pg

BEGIN;

CREATE OR REPLACE VIEW oficina.v_cidades AS 
 SELECT tb_localidade_dne.mun_nu::integer AS cod_cidade,
    tb_localidade_dne.ufe_sg AS sg_uf,
    upper(tb_localidade_dne.loc_no::text) AS txt_cidade,
    tb_localidade_dne.cep AS nr_cep
   FROM oficina.tb_localidade_dne
  WHERE tb_localidade_dne.loc_in_tipo_loc::text = 'M'::text;

ALTER TABLE oficina.v_cidades
  OWNER TO oficina;
  
COMMIT;