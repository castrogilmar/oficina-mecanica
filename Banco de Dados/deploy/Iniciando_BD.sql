-- Deploy oficina:Iniciando_BD to 

BEGIN;

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 9.6.5

-- Started on 2017-10-18 00:31:16

SET statement_timeout = 0;
SET lock_timeout = 0;
--SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
--SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 24578)
-- Name: oficina; Type: SCHEMA; Schema: -; Owner: oficina
--

CREATE SCHEMA oficina;


ALTER SCHEMA oficina OWNER TO oficina;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2219 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = oficina, pg_catalog;

--
-- TOC entry 202 (class 1255 OID 24633)
-- Name: trg_fct_cliente(); Type: FUNCTION; Schema: oficina; Owner: oficina
--

CREATE FUNCTION trg_fct_cliente() RETURNS trigger
    LANGUAGE plpgsql
    AS $$begin
   perform cod_cliente from oficina.tb_cliente
      where txt_cliente = new.txt_cliente and tpo_cliente = new.tpo_cliente and dt_nascimento = new.dt_nascimento;
   if found then
      raise exception '%', 'Cliente já cadastrado';
   end if; 
   return new;
end;
$$;


ALTER FUNCTION oficina.trg_fct_cliente() OWNER TO oficina;

--
-- TOC entry 203 (class 1255 OID 24635)
-- Name: trg_fct_cliente_doc(); Type: FUNCTION; Schema: oficina; Owner: oficina
--

CREATE FUNCTION trg_fct_cliente_doc() RETURNS trigger
    LANGUAGE plpgsql
    AS $$begin
   perform cod_documento from oficina.tb_cliente_doc
      where txt_documento = new.txt_documento and tpo_documento = new.tpo_documento;
   if found then
      raise exception '%','Documento já cadastrado pra o cliente';
   end if;
   return new;
end;
      
$$;


ALTER FUNCTION oficina.trg_fct_cliente_doc() OWNER TO oficina;

--
-- TOC entry 217 (class 1255 OID 24640)
-- Name: trg_fct_cliente_fone(); Type: FUNCTION; Schema: oficina; Owner: oficina
--

CREATE FUNCTION trg_fct_cliente_fone() RETURNS trigger
    LANGUAGE plpgsql
    AS $$begin
   perform cod_fone from oficina.tb_cliente_fone
      where txt_fone = new.txt_fone and tpo_fone = new.tpo_fone;
   if found then
      raise exception '%','Telefone já cadastrado para o cliente';
   end if;
   return new;
end;   
$$;


ALTER FUNCTION oficina.trg_fct_cliente_fone() OWNER TO oficina;

--
-- TOC entry 216 (class 1255 OID 24652)
-- Name: trg_fct_tb_tipo_cliente(); Type: FUNCTION; Schema: oficina; Owner: oficina
--

CREATE FUNCTION trg_fct_tb_tipo_cliente() RETURNS trigger
    LANGUAGE plpgsql
    AS $$begin  

   perform cod_tipo from oficina.tb_tipo_cliente
         where txt_tipo = new.txt_tipo;
   if found then
      if (tg_op = 'UPDATE' and old.txt_tipo <> new.txt_tipo) or tg_op = 'INSERT' then
         raise exception '%','Tipo de cliente já cadastrado anteriormente';
         return null;
      end if;   
   end if;
   return new;
end;
$$;


ALTER FUNCTION oficina.trg_fct_tb_tipo_cliente() OWNER TO oficina;

--
-- TOC entry 219 (class 1255 OID 24654)
-- Name: trg_fct_tb_tipo_cliente_delete(); Type: FUNCTION; Schema: oficina; Owner: oficina
--

CREATE FUNCTION trg_fct_tb_tipo_cliente_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$begin
   perform cod_cliente from oficina.tb_cliente 
      where tpo_cliente = old.cod_tipo;
   if found then
      raise exception '%','Tipo de cliente nao pode ser excluido pois esta em uso!';
   end if;
   return old;
end;   
$$;


ALTER FUNCTION oficina.trg_fct_tb_tipo_cliente_delete() OWNER TO oficina;

--
-- TOC entry 220 (class 1255 OID 24665)
-- Name: trg_fct_tipo_documento(); Type: FUNCTION; Schema: oficina; Owner: oficina
--

CREATE FUNCTION trg_fct_tipo_documento() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
perform cod_documento from oficina.tb_tipo_documento
         where txt_documento = new.txt_documento;
   if found then
      if (tg_op = 'UPDATE' and old.txt_documento <> new.txt_documento) or tg_op = 'INSERT' then
         raise exception '%','Tipo de documento já cadastrado anteriormente';
         return null;
      end if;   
   end if;
   return new;
end;   
$$;


ALTER FUNCTION oficina.trg_fct_tipo_documento() OWNER TO oficina;

--
-- TOC entry 218 (class 1255 OID 24664)
-- Name: trg_fct_tipo_documento_delete(); Type: FUNCTION; Schema: oficina; Owner: postgres
--

CREATE FUNCTION trg_fct_tipo_documento_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  perform cod_cliente from oficina.tb_cliente_doc
     where tpo_documento = old.cod_documento;
  if found then
     raise exception '%','Documento nao pode ser excluido, pois esta em uso!';
  end if;
  return old;
end;  
$$;


ALTER FUNCTION oficina.trg_fct_tipo_documento_delete() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 191 (class 1259 OID 24601)
-- Name: tb_cliente; Type: TABLE; Schema: oficina; Owner: oficina
--

CREATE TABLE tb_cliente (
    cod_cliente bigint NOT NULL,
    txt_cliente character varying(200),
    tpo_cliente integer NOT NULL,
    nr_cep character varying(8) NOT NULL,
    txt_endereco character varying(200),
    txt_bairro character varying(100),
    cod_cidade integer NOT NULL,
    txt_complemento character varying(100),
    txt_referencia character varying(100),
    dt_nascimento date,
    dt_cadastro date,
    txt_email text,
    vlr_credito numeric(15,2),
    fl_sexo boolean
);


ALTER TABLE tb_cliente OWNER TO oficina;

--
-- TOC entry 190 (class 1259 OID 24599)
-- Name: tb_cliente_cod_cliente_seq; Type: SEQUENCE; Schema: oficina; Owner: oficina
--

CREATE SEQUENCE tb_cliente_cod_cliente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_cliente_cod_cliente_seq OWNER TO oficina;

--
-- TOC entry 2220 (class 0 OID 0)
-- Dependencies: 190
-- Name: tb_cliente_cod_cliente_seq; Type: SEQUENCE OWNED BY; Schema: oficina; Owner: oficina
--

ALTER SEQUENCE tb_cliente_cod_cliente_seq OWNED BY tb_cliente.cod_cliente;


--
-- TOC entry 193 (class 1259 OID 24612)
-- Name: tb_cliente_doc; Type: TABLE; Schema: oficina; Owner: oficina
--

CREATE TABLE tb_cliente_doc (
    cod_cliente bigint NOT NULL,
    tpo_documento integer,
    txt_documento character varying(50),
    cod_documento integer NOT NULL
);


ALTER TABLE tb_cliente_doc OWNER TO oficina;

--
-- TOC entry 192 (class 1259 OID 24610)
-- Name: tb_cliente_doc_cod_documento_seq; Type: SEQUENCE; Schema: oficina; Owner: oficina
--

CREATE SEQUENCE tb_cliente_doc_cod_documento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_cliente_doc_cod_documento_seq OWNER TO oficina;

--
-- TOC entry 2221 (class 0 OID 0)
-- Dependencies: 192
-- Name: tb_cliente_doc_cod_documento_seq; Type: SEQUENCE OWNED BY; Schema: oficina; Owner: oficina
--

ALTER SEQUENCE tb_cliente_doc_cod_documento_seq OWNED BY tb_cliente_doc.cod_documento;


--
-- TOC entry 195 (class 1259 OID 24620)
-- Name: tb_cliente_fone; Type: TABLE; Schema: oficina; Owner: oficina
--

CREATE TABLE tb_cliente_fone (
    cod_cliente bigint NOT NULL,
    tpo_fone integer,
    txt_fone character varying(30),
    cod_fone integer NOT NULL
);


ALTER TABLE tb_cliente_fone OWNER TO oficina;

--
-- TOC entry 194 (class 1259 OID 24618)
-- Name: tb_cliente_fone_cod_fone_seq; Type: SEQUENCE; Schema: oficina; Owner: oficina
--

CREATE SEQUENCE tb_cliente_fone_cod_fone_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_cliente_fone_cod_fone_seq OWNER TO oficina;

--
-- TOC entry 2222 (class 0 OID 0)
-- Dependencies: 194
-- Name: tb_cliente_fone_cod_fone_seq; Type: SEQUENCE OWNED BY; Schema: oficina; Owner: oficina
--

ALTER SEQUENCE tb_cliente_fone_cod_fone_seq OWNED BY tb_cliente_fone.cod_fone;


--
-- TOC entry 201 (class 1259 OID 24680)
-- Name: tb_funcionario; Type: TABLE; Schema: oficina; Owner: oficina
--

CREATE TABLE tb_funcionario (
    cod_matricula integer NOT NULL,
    txt_nome character varying(200),
    txt_nguerra character varying(50),
    nr_cep character varying(8) NOT NULL,
    txt_endereco character varying(200),
    txt_bairro character varying(100),
    cod_cidade integer NOT NULL,
    txt_complemento character varying(100),
    txt_referencia character varying(100),
    dt_nascimento date,
    dt_cadastro date,
    txt_email text,
    fl_sexo boolean
);


ALTER TABLE tb_funcionario OWNER TO oficina;

--
-- TOC entry 200 (class 1259 OID 24678)
-- Name: tb_funcionario_cod_matricula_seq; Type: SEQUENCE; Schema: oficina; Owner: oficina
--

CREATE SEQUENCE tb_funcionario_cod_matricula_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_funcionario_cod_matricula_seq OWNER TO oficina;

--
-- TOC entry 2223 (class 0 OID 0)
-- Dependencies: 200
-- Name: tb_funcionario_cod_matricula_seq; Type: SEQUENCE OWNED BY; Schema: oficina; Owner: oficina
--

ALTER SEQUENCE tb_funcionario_cod_matricula_seq OWNED BY tb_funcionario.cod_matricula;


--
-- TOC entry 189 (class 1259 OID 24592)
-- Name: tb_grupo; Type: TABLE; Schema: oficina; Owner: oficina
--

CREATE TABLE tb_grupo (
    cod_grupo integer NOT NULL,
    txt_grupo character varying(200),
    tpo_produto character(1)
);


ALTER TABLE tb_grupo OWNER TO oficina;

--
-- TOC entry 188 (class 1259 OID 24590)
-- Name: tb_grupo_cod_grupo_seq; Type: SEQUENCE; Schema: oficina; Owner: oficina
--

CREATE SEQUENCE tb_grupo_cod_grupo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_grupo_cod_grupo_seq OWNER TO oficina;

--
-- TOC entry 2224 (class 0 OID 0)
-- Dependencies: 188
-- Name: tb_grupo_cod_grupo_seq; Type: SEQUENCE OWNED BY; Schema: oficina; Owner: oficina
--

ALTER SEQUENCE tb_grupo_cod_grupo_seq OWNED BY tb_grupo.cod_grupo;


--
-- TOC entry 187 (class 1259 OID 24581)
-- Name: tb_produto; Type: TABLE; Schema: oficina; Owner: oficina
--

CREATE TABLE tb_produto (
    cod_grupo integer,
    cod_produto bigint NOT NULL,
    dt_cadastro date,
    tpo_produto character(1),
    txt_produto character varying(500),
    vlr_aprazo numeric(15,2),
    vlr_avista numeric(15,2),
    vlr_custo numeric(15,2),
    fl_situacao boolean
);


ALTER TABLE tb_produto OWNER TO oficina;

--
-- TOC entry 186 (class 1259 OID 24579)
-- Name: tb_produto_cod_produto_seq; Type: SEQUENCE; Schema: oficina; Owner: oficina
--

CREATE SEQUENCE tb_produto_cod_produto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_produto_cod_produto_seq OWNER TO oficina;

--
-- TOC entry 2225 (class 0 OID 0)
-- Dependencies: 186
-- Name: tb_produto_cod_produto_seq; Type: SEQUENCE OWNED BY; Schema: oficina; Owner: oficina
--

ALTER SEQUENCE tb_produto_cod_produto_seq OWNED BY tb_produto.cod_produto;


--
-- TOC entry 197 (class 1259 OID 24644)
-- Name: tb_tipo_cliente; Type: TABLE; Schema: oficina; Owner: oficina
--

CREATE TABLE tb_tipo_cliente (
    cod_tipo integer NOT NULL,
    txt_tipo character varying(50)
);


ALTER TABLE tb_tipo_cliente OWNER TO oficina;

--
-- TOC entry 196 (class 1259 OID 24642)
-- Name: tb_tipo_cliente_cod_tipo_seq; Type: SEQUENCE; Schema: oficina; Owner: oficina
--

CREATE SEQUENCE tb_tipo_cliente_cod_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_tipo_cliente_cod_tipo_seq OWNER TO oficina;

--
-- TOC entry 2226 (class 0 OID 0)
-- Dependencies: 196
-- Name: tb_tipo_cliente_cod_tipo_seq; Type: SEQUENCE OWNED BY; Schema: oficina; Owner: oficina
--

ALTER SEQUENCE tb_tipo_cliente_cod_tipo_seq OWNED BY tb_tipo_cliente.cod_tipo;


--
-- TOC entry 199 (class 1259 OID 24658)
-- Name: tb_tipo_documento; Type: TABLE; Schema: oficina; Owner: oficina
--

CREATE TABLE tb_tipo_documento (
    cod_documento integer NOT NULL,
    txt_documento character varying(30),
    txt_funcao_validacao character varying(50)
);


ALTER TABLE tb_tipo_documento OWNER TO oficina;

--
-- TOC entry 198 (class 1259 OID 24656)
-- Name: tb_tipo_documento_cod_documento_seq; Type: SEQUENCE; Schema: oficina; Owner: oficina
--

CREATE SEQUENCE tb_tipo_documento_cod_documento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tb_tipo_documento_cod_documento_seq OWNER TO oficina;

--
-- TOC entry 2227 (class 0 OID 0)
-- Dependencies: 198
-- Name: tb_tipo_documento_cod_documento_seq; Type: SEQUENCE OWNED BY; Schema: oficina; Owner: oficina
--

ALTER SEQUENCE tb_tipo_documento_cod_documento_seq OWNED BY tb_tipo_documento.cod_documento;


--
-- TOC entry 2056 (class 2604 OID 24604)
-- Name: tb_cliente cod_cliente; Type: DEFAULT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_cliente ALTER COLUMN cod_cliente SET DEFAULT nextval('tb_cliente_cod_cliente_seq'::regclass);


--
-- TOC entry 2057 (class 2604 OID 24615)
-- Name: tb_cliente_doc cod_documento; Type: DEFAULT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_cliente_doc ALTER COLUMN cod_documento SET DEFAULT nextval('tb_cliente_doc_cod_documento_seq'::regclass);


--
-- TOC entry 2058 (class 2604 OID 24623)
-- Name: tb_cliente_fone cod_fone; Type: DEFAULT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_cliente_fone ALTER COLUMN cod_fone SET DEFAULT nextval('tb_cliente_fone_cod_fone_seq'::regclass);


--
-- TOC entry 2061 (class 2604 OID 24683)
-- Name: tb_funcionario cod_matricula; Type: DEFAULT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_funcionario ALTER COLUMN cod_matricula SET DEFAULT nextval('tb_funcionario_cod_matricula_seq'::regclass);


--
-- TOC entry 2055 (class 2604 OID 24595)
-- Name: tb_grupo cod_grupo; Type: DEFAULT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_grupo ALTER COLUMN cod_grupo SET DEFAULT nextval('tb_grupo_cod_grupo_seq'::regclass);


--
-- TOC entry 2054 (class 2604 OID 24584)
-- Name: tb_produto cod_produto; Type: DEFAULT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_produto ALTER COLUMN cod_produto SET DEFAULT nextval('tb_produto_cod_produto_seq'::regclass);


--
-- TOC entry 2059 (class 2604 OID 24647)
-- Name: tb_tipo_cliente cod_tipo; Type: DEFAULT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_tipo_cliente ALTER COLUMN cod_tipo SET DEFAULT nextval('tb_tipo_cliente_cod_tipo_seq'::regclass);


--
-- TOC entry 2060 (class 2604 OID 24661)
-- Name: tb_tipo_documento cod_documento; Type: DEFAULT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_tipo_documento ALTER COLUMN cod_documento SET DEFAULT nextval('tb_tipo_documento_cod_documento_seq'::regclass);


--
-- TOC entry 2077 (class 2606 OID 24617)
-- Name: tb_cliente_doc tb_cliente_doc_pkey; Type: CONSTRAINT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_cliente_doc
    ADD CONSTRAINT tb_cliente_doc_pkey PRIMARY KEY (cod_documento);


--
-- TOC entry 2082 (class 2606 OID 24625)
-- Name: tb_cliente_fone tb_cliente_fone_pkey; Type: CONSTRAINT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_cliente_fone
    ADD CONSTRAINT tb_cliente_fone_pkey PRIMARY KEY (cod_fone);


--
-- TOC entry 2072 (class 2606 OID 24609)
-- Name: tb_cliente tb_cliente_pkey; Type: CONSTRAINT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_cliente
    ADD CONSTRAINT tb_cliente_pkey PRIMARY KEY (cod_cliente);


--
-- TOC entry 2088 (class 2606 OID 24688)
-- Name: tb_funcionario tb_funcionario_pkey; Type: CONSTRAINT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_funcionario
    ADD CONSTRAINT tb_funcionario_pkey PRIMARY KEY (cod_matricula);


--
-- TOC entry 2066 (class 2606 OID 24597)
-- Name: tb_grupo tb_grupo_pkey; Type: CONSTRAINT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_grupo
    ADD CONSTRAINT tb_grupo_pkey PRIMARY KEY (cod_grupo);


--
-- TOC entry 2064 (class 2606 OID 24589)
-- Name: tb_produto tb_produto_pkey; Type: CONSTRAINT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_produto
    ADD CONSTRAINT tb_produto_pkey PRIMARY KEY (cod_produto);


--
-- TOC entry 2084 (class 2606 OID 24649)
-- Name: tb_tipo_cliente tb_tipo_cliente_pkey; Type: CONSTRAINT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_tipo_cliente
    ADD CONSTRAINT tb_tipo_cliente_pkey PRIMARY KEY (cod_tipo);


--
-- TOC entry 2086 (class 2606 OID 24663)
-- Name: tb_tipo_documento tb_tipo_documento_pkey; Type: CONSTRAINT; Schema: oficina; Owner: oficina
--

ALTER TABLE ONLY tb_tipo_documento
    ADD CONSTRAINT tb_tipo_documento_pkey PRIMARY KEY (cod_documento);


--
-- TOC entry 2067 (class 1259 OID 24632)
-- Name: idx_cliente_cod_cidade; Type: INDEX; Schema: oficina; Owner: oficina
--

CREATE INDEX idx_cliente_cod_cidade ON tb_cliente USING btree (cod_cidade);


--
-- TOC entry 2073 (class 1259 OID 24626)
-- Name: idx_cliente_doc_cod_cliente; Type: INDEX; Schema: oficina; Owner: oficina
--

CREATE INDEX idx_cliente_doc_cod_cliente ON tb_cliente_doc USING btree (cod_cliente);


--
-- TOC entry 2074 (class 1259 OID 24628)
-- Name: idx_cliente_doc_tpo_documento; Type: INDEX; Schema: oficina; Owner: oficina
--

CREATE INDEX idx_cliente_doc_tpo_documento ON tb_cliente_doc USING btree (tpo_documento);


--
-- TOC entry 2075 (class 1259 OID 24638)
-- Name: idx_cliente_doc_txt_documento; Type: INDEX; Schema: oficina; Owner: oficina
--

CREATE INDEX idx_cliente_doc_txt_documento ON tb_cliente_doc USING btree (txt_documento);


--
-- TOC entry 2078 (class 1259 OID 24629)
-- Name: idx_cliente_fone_tpo_fone; Type: INDEX; Schema: oficina; Owner: oficina
--

CREATE INDEX idx_cliente_fone_tpo_fone ON tb_cliente_fone USING btree (tpo_fone);


--
-- TOC entry 2079 (class 1259 OID 24639)
-- Name: idx_cliente_fone_txt_fone; Type: INDEX; Schema: oficina; Owner: oficina
--

CREATE INDEX idx_cliente_fone_txt_fone ON tb_cliente_fone USING btree (txt_fone);


--
-- TOC entry 2068 (class 1259 OID 24630)
-- Name: idx_cliente_nr_cep; Type: INDEX; Schema: oficina; Owner: oficina
--

CREATE INDEX idx_cliente_nr_cep ON tb_cliente USING btree (nr_cep);


--
-- TOC entry 2069 (class 1259 OID 24631)
-- Name: idx_cliente_tpo_cliente; Type: INDEX; Schema: oficina; Owner: oficina
--

CREATE INDEX idx_cliente_tpo_cliente ON tb_cliente USING btree (tpo_cliente);


--
-- TOC entry 2070 (class 1259 OID 24637)
-- Name: idx_cliente_txt_cliente; Type: INDEX; Schema: oficina; Owner: oficina
--

CREATE INDEX idx_cliente_txt_cliente ON tb_cliente USING btree (txt_cliente);


--
-- TOC entry 2062 (class 1259 OID 24598)
-- Name: idx_grupo; Type: INDEX; Schema: oficina; Owner: oficina
--

CREATE INDEX idx_grupo ON tb_produto USING btree (cod_grupo);


--
-- TOC entry 2080 (class 1259 OID 24627)
-- Name: idx_tb_cliente_fone_cod_cliente; Type: INDEX; Schema: oficina; Owner: oficina
--

CREATE INDEX idx_tb_cliente_fone_cod_cliente ON tb_cliente_fone USING btree (cod_cliente);


--
-- TOC entry 2090 (class 2620 OID 24636)
-- Name: tb_cliente_doc trg_client_doc; Type: TRIGGER; Schema: oficina; Owner: oficina
--

CREATE TRIGGER trg_client_doc BEFORE INSERT OR UPDATE ON tb_cliente_doc FOR EACH ROW EXECUTE PROCEDURE trg_fct_cliente_doc();


--
-- TOC entry 2089 (class 2620 OID 24634)
-- Name: tb_cliente trg_cliente; Type: TRIGGER; Schema: oficina; Owner: oficina
--

CREATE TRIGGER trg_cliente BEFORE INSERT OR UPDATE ON tb_cliente FOR EACH ROW EXECUTE PROCEDURE trg_fct_cliente();


--
-- TOC entry 2091 (class 2620 OID 24641)
-- Name: tb_cliente_fone trg_cliente_fone; Type: TRIGGER; Schema: oficina; Owner: oficina
--

CREATE TRIGGER trg_cliente_fone BEFORE INSERT OR UPDATE ON tb_cliente_fone FOR EACH ROW EXECUTE PROCEDURE trg_fct_cliente_fone();


--
-- TOC entry 2092 (class 2620 OID 24653)
-- Name: tb_tipo_cliente trg_tipo_cliente; Type: TRIGGER; Schema: oficina; Owner: oficina
--

CREATE TRIGGER trg_tipo_cliente BEFORE INSERT OR UPDATE ON tb_tipo_cliente FOR EACH ROW EXECUTE PROCEDURE trg_fct_tb_tipo_cliente();


--
-- TOC entry 2093 (class 2620 OID 24655)
-- Name: tb_tipo_cliente trg_tipo_cliente_delete; Type: TRIGGER; Schema: oficina; Owner: oficina
--

CREATE TRIGGER trg_tipo_cliente_delete BEFORE DELETE ON tb_tipo_cliente FOR EACH ROW EXECUTE PROCEDURE trg_fct_tb_tipo_cliente_delete();


--
-- TOC entry 2094 (class 2620 OID 24666)
-- Name: tb_tipo_documento trg_tipo_documento; Type: TRIGGER; Schema: oficina; Owner: oficina
--

CREATE TRIGGER trg_tipo_documento BEFORE INSERT OR UPDATE ON tb_tipo_documento FOR EACH ROW EXECUTE PROCEDURE trg_fct_tipo_documento();


--
-- TOC entry 2095 (class 2620 OID 24667)
-- Name: tb_tipo_documento trg_tipo_documento_delete; Type: TRIGGER; Schema: oficina; Owner: oficina
--

CREATE TRIGGER trg_tipo_documento_delete BEFORE DELETE ON tb_tipo_documento FOR EACH ROW EXECUTE PROCEDURE trg_fct_tipo_documento_delete();


-- Completed on 2017-10-18 00:31:18

--
-- PostgreSQL database dump complete
--



COMMIT;

