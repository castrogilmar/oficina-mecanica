-- Deploy oficina:trigger_trg_fct_fornecedor to pg

BEGIN;


CREATE OR REPLACE FUNCTION oficina.trg_fct_fornecedor()
  RETURNS trigger AS
$BODY$begin
   perform cod_fornecedor from oficina.tb_fornecedor
      where txt_fantasia = new.txt_fantasia and txt_razao_social = new.txt_razao_social;
   if found and (tg_op = 'INSERT' or (old.cod_fornecedor <> new.cod_fornecedor and tg_op = 'UPDATE')) then
      raise exception '%', 'FORNECEDOR JA CADASTRADO ANTERIORMENTE';
   end if; 
   return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION oficina.trg_fct_fornecedor()
  OWNER TO oficina;


CREATE TRIGGER trg_fornecedor
  BEFORE INSERT OR UPDATE
  ON oficina.tb_fornecedor
  FOR EACH ROW
  EXECUTE PROCEDURE oficina.trg_fct_fornecedor();



COMMIT;