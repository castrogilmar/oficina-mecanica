-- Deploy oficina:insert_coluna_cod_forncedor_tb_produto to pg

BEGIN;

alter table oficina.tb_produto add column cod_fornecedor integer;

-- Index: oficina.idx_tb_produto_cod_fornecedor

-- DROP INDEX oficina.idx_tb_produto_cod_fornecedor;

CREATE INDEX idx_tb_produto_cod_fornecedor
  ON oficina.tb_produto
  USING btree
  (cod_fornecedor); 

COMMIT;