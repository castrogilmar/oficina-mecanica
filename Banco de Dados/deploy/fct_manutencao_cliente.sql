-- Deploy oficina:table_tb_senha to pg

BEGIN;

  
CREATE OR REPLACE FUNCTION oficina.fct_manutencao_cliente(
    v_cod_cliente integer,
    v_tp_cliente character varying,
    v_dt_nascimento date,
    v_fl_sexo character varying,
    v_txt_cliente character varying,
    v_nr_cep character varying,
    v_sg_uf character varying,
    v_txt_cidade character varying,
    v_txt_endereco character varying,
    v_txt_bairro character varying,
    v_txt_complemento character varying,
    v_txt_referencia character varying)
  RETURNS text AS
$BODY$
declare
   v_retorno text;
   v_tpo_cliente integer;
   v_cod_cidade integer;
begin
   v_tpo_cliente=(select cod_tipo from oficina.tb_tipo_cliente a where a.txt_tipo = v_tp_cliente);
   v_cod_cidade=(select cod_cidade from oficina.v_cidades a where a.txt_cidade = v_txt_cidade);

   if coalesce(v_cod_cliente,0) = 0 then
      begin
         insert into oficina.tb_cliente (txt_cliente, tpo_cliente, nr_cep, txt_endereco, txt_bairro, cod_cidade, txt_complemento,
                                         txt_referencia, dt_nascimento, dt_cadastro, fl_sexo, sg_uf)                                         
             select v_txt_cliente, v_tpo_cliente, v_nr_cep, v_txt_endereco, v_txt_bairro, v_cod_cidade, coalesce(v_txt_complemento,''),
                    coalesce(v_txt_referencia,''), v_dt_nascimento, current_date, case when v_fl_sexo = 'M' then false else true end, v_sg_uf
             returning 'CLIENTE CRIADO COM SUCESSO CODIGO [ ' || cod_cliente::text || ']!' into v_retorno;
      exception
         when others then
            raise exception '%', SQLERRM ;
            
      end;      
   else
      begin
         update oficina.tb_cliente set txt_cliente=v_txt_cliente, 
                                       nr_cep=v_nr_cep, 
                                       sg_ud=v_sg_uf,
                                       txt_endereco=v_txt_endereco,
                                       txt_bairro=v_txt_bairro, 
                                       cod_cidade=v_cod_cidade, 
                                       txt_complemento=v_txt_complemento,
                                       txt_referencia=v_txt_referencia, 
                                       dt_nascimento=v_dt_nascimento,
                                       fl_sexo=v_fl_sexo
             where cod_cliente = v_cod_cliente 
             returning 'MANUTENCAO NO CLIENTE CODIGO [ ' || cod_cliente::text || '] REALIZADA COM SUCESSO!' into v_retorno;
      exception
         when others then
            raise exception '%', SQLERRM ;
      end;
   end if; 
   return v_retorno;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION oficina.fct_manutencao_cliente(integer, character varying, date, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO oficina;


COMMIT;