﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FConsultaProdutos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DTG_PRODUTOS = New System.Windows.Forms.DataGridView()
        Me.cod_produto = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.txt_grupo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txt_produto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TPO_PRODUTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.vlr_estoque_atual = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txt_fornecedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VLR_CUSTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.vlr_avista = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.vlr_aprazo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CBO_GRUPO = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TXT_DESCRICAO = New System.Windows.Forms.TextBox()
        Me.BUT_CONSULTA = New System.Windows.Forms.Button()
        Me.BUT_NOVO_PRODUTO = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        CType(Me.DTG_PRODUTOS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'DTG_PRODUTOS
        '
        Me.DTG_PRODUTOS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DTG_PRODUTOS.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cod_produto, Me.txt_grupo, Me.txt_produto, Me.TPO_PRODUTO, Me.vlr_estoque_atual, Me.txt_fornecedor, Me.VLR_CUSTO, Me.vlr_avista, Me.vlr_aprazo})
        Me.DTG_PRODUTOS.Location = New System.Drawing.Point(6, 19)
        Me.DTG_PRODUTOS.Name = "DTG_PRODUTOS"
        Me.DTG_PRODUTOS.Size = New System.Drawing.Size(1042, 472)
        Me.DTG_PRODUTOS.TabIndex = 5
        '
        'cod_produto
        '
        Me.cod_produto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.cod_produto.DataPropertyName = "cod_produto"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.cod_produto.DefaultCellStyle = DataGridViewCellStyle1
        Me.cod_produto.HeaderText = "CÓDIGO"
        Me.cod_produto.Name = "cod_produto"
        Me.cod_produto.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cod_produto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.cod_produto.Width = 74
        '
        'txt_grupo
        '
        Me.txt_grupo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_grupo.DataPropertyName = "txt_grupo"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        Me.txt_grupo.DefaultCellStyle = DataGridViewCellStyle2
        Me.txt_grupo.HeaderText = "GRUPO"
        Me.txt_grupo.Name = "txt_grupo"
        Me.txt_grupo.Width = 71
        '
        'txt_produto
        '
        Me.txt_produto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_produto.DataPropertyName = "txt_produto"
        Me.txt_produto.HeaderText = "DESCRIÇÃO"
        Me.txt_produto.Name = "txt_produto"
        Me.txt_produto.Width = 94
        '
        'TPO_PRODUTO
        '
        Me.TPO_PRODUTO.DataPropertyName = "tpo_produto"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.TPO_PRODUTO.DefaultCellStyle = DataGridViewCellStyle3
        Me.TPO_PRODUTO.HeaderText = "TP"
        Me.TPO_PRODUTO.Name = "TPO_PRODUTO"
        '
        'vlr_estoque_atual
        '
        Me.vlr_estoque_atual.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.vlr_estoque_atual.DataPropertyName = "vlr_estoque_atual"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.vlr_estoque_atual.DefaultCellStyle = DataGridViewCellStyle4
        Me.vlr_estoque_atual.HeaderText = "ESTOQUE ATUAL"
        Me.vlr_estoque_atual.Name = "vlr_estoque_atual"
        Me.vlr_estoque_atual.Width = 112
        '
        'txt_fornecedor
        '
        Me.txt_fornecedor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_fornecedor.DataPropertyName = "txt_fornecedor"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        Me.txt_fornecedor.DefaultCellStyle = DataGridViewCellStyle5
        Me.txt_fornecedor.HeaderText = "FORNECEDOR"
        Me.txt_fornecedor.Name = "txt_fornecedor"
        Me.txt_fornecedor.ReadOnly = True
        Me.txt_fornecedor.Width = 107
        '
        'VLR_CUSTO
        '
        Me.VLR_CUSTO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.VLR_CUSTO.DataPropertyName = "vlr_custo"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.VLR_CUSTO.DefaultCellStyle = DataGridViewCellStyle6
        Me.VLR_CUSTO.HeaderText = "VLR CUSTO"
        Me.VLR_CUSTO.Name = "VLR_CUSTO"
        Me.VLR_CUSTO.Width = 86
        '
        'vlr_avista
        '
        Me.vlr_avista.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.vlr_avista.DataPropertyName = "vlr_avista"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.vlr_avista.DefaultCellStyle = DataGridViewCellStyle7
        Me.vlr_avista.HeaderText = "VALOR À VISTA"
        Me.vlr_avista.Name = "vlr_avista"
        Me.vlr_avista.Width = 75
        '
        'vlr_aprazo
        '
        Me.vlr_aprazo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.vlr_aprazo.DataPropertyName = "vlr_aprazo"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.vlr_aprazo.DefaultCellStyle = DataGridViewCellStyle8
        Me.vlr_aprazo.HeaderText = "VALOR A PRAZO"
        Me.vlr_aprazo.Name = "vlr_aprazo"
        Me.vlr_aprazo.Width = 108
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.CBO_GRUPO)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TXT_DESCRICAO)
        Me.GroupBox1.Controls.Add(Me.BUT_CONSULTA)
        Me.GroupBox1.Controls.Add(Me.BUT_NOVO_PRODUTO)
        Me.GroupBox1.Location = New System.Drawing.Point(5, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1056, 116)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(116, 15)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Descrição do Grupo"
        '
        'CBO_GRUPO
        '
        Me.CBO_GRUPO.BackColor = System.Drawing.SystemColors.Info
        Me.CBO_GRUPO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBO_GRUPO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBO_GRUPO.FormattingEnabled = True
        Me.CBO_GRUPO.Location = New System.Drawing.Point(6, 33)
        Me.CBO_GRUPO.Name = "CBO_GRUPO"
        Me.CBO_GRUPO.Size = New System.Drawing.Size(607, 21)
        Me.CBO_GRUPO.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(174, 15)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Descrição do Produto / Serviço"
        '
        'TXT_DESCRICAO
        '
        Me.TXT_DESCRICAO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_DESCRICAO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_DESCRICAO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_DESCRICAO.Location = New System.Drawing.Point(6, 77)
        Me.TXT_DESCRICAO.Name = "TXT_DESCRICAO"
        Me.TXT_DESCRICAO.Size = New System.Drawing.Size(607, 22)
        Me.TXT_DESCRICAO.TabIndex = 2
        '
        'BUT_CONSULTA
        '
        Me.BUT_CONSULTA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_CONSULTA.Image = Global.oficina_mecanica.My.Resources.Resources.Visualpharm_Must_Have_Search
        Me.BUT_CONSULTA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_CONSULTA.Location = New System.Drawing.Point(822, 19)
        Me.BUT_CONSULTA.Name = "BUT_CONSULTA"
        Me.BUT_CONSULTA.Size = New System.Drawing.Size(106, 46)
        Me.BUT_CONSULTA.TabIndex = 4
        Me.BUT_CONSULTA.Text = "CONSULTAR"
        Me.BUT_CONSULTA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_CONSULTA.UseVisualStyleBackColor = True
        '
        'BUT_NOVO_PRODUTO
        '
        Me.BUT_NOVO_PRODUTO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_NOVO_PRODUTO.Image = Global.oficina_mecanica.My.Resources.Resources.Visualpharm_Must_Have_Add
        Me.BUT_NOVO_PRODUTO.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_NOVO_PRODUTO.Location = New System.Drawing.Point(715, 19)
        Me.BUT_NOVO_PRODUTO.Name = "BUT_NOVO_PRODUTO"
        Me.BUT_NOVO_PRODUTO.Size = New System.Drawing.Size(101, 46)
        Me.BUT_NOVO_PRODUTO.TabIndex = 3
        Me.BUT_NOVO_PRODUTO.Text = "NOVO PRODUTO"
        Me.BUT_NOVO_PRODUTO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_NOVO_PRODUTO.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.DTG_PRODUTOS)
        Me.GroupBox2.Location = New System.Drawing.Point(5, 130)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1056, 497)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        '
        'FConsultaProdutos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(960, 632)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FConsultaProdutos"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        CType(Me.DTG_PRODUTOS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DTG_PRODUTOS As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CBO_GRUPO As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TXT_DESCRICAO As System.Windows.Forms.TextBox
    Friend WithEvents BUT_CONSULTA As System.Windows.Forms.Button
    Friend WithEvents BUT_NOVO_PRODUTO As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cod_produto As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents txt_grupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txt_produto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TPO_PRODUTO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents vlr_estoque_atual As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txt_fornecedor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VLR_CUSTO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents vlr_avista As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents vlr_aprazo As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
