﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FCadastroCargo
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FCadastroCargo))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TXT_COD_CARGO = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TXT_DESCRICAO_CARGO = New System.Windows.Forms.TextBox()
        Me.BUT_SALVAR = New System.Windows.Forms.Button()
        Me.BUT_EXCLUIR = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 15)
        Me.Label1.TabIndex = 35
        Me.Label1.Text = "Código"
        '
        'TXT_COD_CARGO
        '
        Me.TXT_COD_CARGO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_COD_CARGO.Enabled = False
        Me.TXT_COD_CARGO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_COD_CARGO.Location = New System.Drawing.Point(15, 28)
        Me.TXT_COD_CARGO.Name = "TXT_COD_CARGO"
        Me.TXT_COD_CARGO.Size = New System.Drawing.Size(100, 22)
        Me.TXT_COD_CARGO.TabIndex = 34
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(106, 15)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Descrição do Tipo"
        '
        'TXT_DESCRICAO_CARGO
        '
        Me.TXT_DESCRICAO_CARGO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_DESCRICAO_CARGO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_DESCRICAO_CARGO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_DESCRICAO_CARGO.Location = New System.Drawing.Point(12, 68)
        Me.TXT_DESCRICAO_CARGO.Name = "TXT_DESCRICAO_CARGO"
        Me.TXT_DESCRICAO_CARGO.Size = New System.Drawing.Size(607, 22)
        Me.TXT_DESCRICAO_CARGO.TabIndex = 31
        '
        'BUT_SALVAR
        '
        Me.BUT_SALVAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_SALVAR.Image = Global.oficina_mecanica.My.Resources.Resources.Hopstarter_Sleek_Xp_Basic_Ok
        Me.BUT_SALVAR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_SALVAR.Location = New System.Drawing.Point(224, 106)
        Me.BUT_SALVAR.Name = "BUT_SALVAR"
        Me.BUT_SALVAR.Size = New System.Drawing.Size(88, 46)
        Me.BUT_SALVAR.TabIndex = 32
        Me.BUT_SALVAR.Text = "SALVAR"
        Me.BUT_SALVAR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_SALVAR.UseVisualStyleBackColor = True
        '
        'BUT_EXCLUIR
        '
        Me.BUT_EXCLUIR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_EXCLUIR.Image = CType(resources.GetObject("BUT_EXCLUIR.Image"), System.Drawing.Image)
        Me.BUT_EXCLUIR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_EXCLUIR.Location = New System.Drawing.Point(318, 106)
        Me.BUT_EXCLUIR.Name = "BUT_EXCLUIR"
        Me.BUT_EXCLUIR.Size = New System.Drawing.Size(91, 46)
        Me.BUT_EXCLUIR.TabIndex = 33
        Me.BUT_EXCLUIR.Text = "EXCLUIR"
        Me.BUT_EXCLUIR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_EXCLUIR.UseVisualStyleBackColor = True
        '
        'FCadastroCargo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(631, 162)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TXT_COD_CARGO)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TXT_DESCRICAO_CARGO)
        Me.Controls.Add(Me.BUT_SALVAR)
        Me.Controls.Add(Me.BUT_EXCLUIR)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FCadastroCargo"
        Me.ShowIcon = False
        Me.Text = "FCadastroCargo"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents TXT_COD_CARGO As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TXT_DESCRICAO_CARGO As TextBox
    Friend WithEvents BUT_SALVAR As Button
    Friend WithEvents BUT_EXCLUIR As Button
End Class
