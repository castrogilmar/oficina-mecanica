﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FConsultaGrupos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RDO_TODOS = New System.Windows.Forms.RadioButton()
        Me.RDO_SERVICO = New System.Windows.Forms.RadioButton()
        Me.RDO_PRODUTO = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TXT_DESCRICAO = New System.Windows.Forms.TextBox()
        Me.BUT_CONSULTA = New System.Windows.Forms.Button()
        Me.BUT_NOVO_GRUPO = New System.Windows.Forms.Button()
        Me.DTG_GRUPOS = New System.Windows.Forms.DataGridView()
        Me.cod_grupo = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.txt_grupo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpo_grupo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DTG_GRUPOS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TXT_DESCRICAO)
        Me.GroupBox1.Controls.Add(Me.BUT_CONSULTA)
        Me.GroupBox1.Controls.Add(Me.BUT_NOVO_GRUPO)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 15)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(1176, 133)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RDO_TODOS)
        Me.GroupBox2.Controls.Add(Me.RDO_SERVICO)
        Me.GroupBox2.Controls.Add(Me.RDO_PRODUTO)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 14)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Size = New System.Drawing.Size(289, 53)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Tipo de Grupo"
        '
        'RDO_TODOS
        '
        Me.RDO_TODOS.AutoSize = True
        Me.RDO_TODOS.Checked = True
        Me.RDO_TODOS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RDO_TODOS.Location = New System.Drawing.Point(8, 22)
        Me.RDO_TODOS.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RDO_TODOS.Name = "RDO_TODOS"
        Me.RDO_TODOS.Size = New System.Drawing.Size(72, 22)
        Me.RDO_TODOS.TabIndex = 1
        Me.RDO_TODOS.TabStop = True
        Me.RDO_TODOS.Text = "Todos"
        Me.RDO_TODOS.UseVisualStyleBackColor = True
        '
        'RDO_SERVICO
        '
        Me.RDO_SERVICO.AutoSize = True
        Me.RDO_SERVICO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RDO_SERVICO.Location = New System.Drawing.Point(193, 22)
        Me.RDO_SERVICO.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RDO_SERVICO.Name = "RDO_SERVICO"
        Me.RDO_SERVICO.Size = New System.Drawing.Size(79, 22)
        Me.RDO_SERVICO.TabIndex = 3
        Me.RDO_SERVICO.Text = "Serviço"
        Me.RDO_SERVICO.UseVisualStyleBackColor = True
        '
        'RDO_PRODUTO
        '
        Me.RDO_PRODUTO.AutoSize = True
        Me.RDO_PRODUTO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RDO_PRODUTO.Location = New System.Drawing.Point(95, 22)
        Me.RDO_PRODUTO.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RDO_PRODUTO.Name = "RDO_PRODUTO"
        Me.RDO_PRODUTO.Size = New System.Drawing.Size(82, 22)
        Me.RDO_PRODUTO.TabIndex = 2
        Me.RDO_PRODUTO.Text = "Produto"
        Me.RDO_PRODUTO.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(4, 70)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(143, 18)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Descrição do Grupo"
        '
        'TXT_DESCRICAO
        '
        Me.TXT_DESCRICAO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_DESCRICAO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_DESCRICAO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_DESCRICAO.Location = New System.Drawing.Point(8, 92)
        Me.TXT_DESCRICAO.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TXT_DESCRICAO.Name = "TXT_DESCRICAO"
        Me.TXT_DESCRICAO.Size = New System.Drawing.Size(808, 26)
        Me.TXT_DESCRICAO.TabIndex = 4
        '
        'BUT_CONSULTA
        '
        Me.BUT_CONSULTA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_CONSULTA.Image = Global.oficina_mecanica.My.Resources.Resources.Visualpharm_Must_Have_Search
        Me.BUT_CONSULTA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_CONSULTA.Location = New System.Drawing.Point(1005, 36)
        Me.BUT_CONSULTA.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BUT_CONSULTA.Name = "BUT_CONSULTA"
        Me.BUT_CONSULTA.Size = New System.Drawing.Size(141, 57)
        Me.BUT_CONSULTA.TabIndex = 6
        Me.BUT_CONSULTA.Text = "CONSULTAR"
        Me.BUT_CONSULTA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_CONSULTA.UseVisualStyleBackColor = True
        '
        'BUT_NOVO_GRUPO
        '
        Me.BUT_NOVO_GRUPO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_NOVO_GRUPO.Image = Global.oficina_mecanica.My.Resources.Resources.Visualpharm_Must_Have_Add
        Me.BUT_NOVO_GRUPO.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_NOVO_GRUPO.Location = New System.Drawing.Point(837, 36)
        Me.BUT_NOVO_GRUPO.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BUT_NOVO_GRUPO.Name = "BUT_NOVO_GRUPO"
        Me.BUT_NOVO_GRUPO.Size = New System.Drawing.Size(160, 57)
        Me.BUT_NOVO_GRUPO.TabIndex = 5
        Me.BUT_NOVO_GRUPO.Text = "NOVO GRUPO"
        Me.BUT_NOVO_GRUPO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_NOVO_GRUPO.UseVisualStyleBackColor = True
        '
        'DTG_GRUPOS
        '
        Me.DTG_GRUPOS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DTG_GRUPOS.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cod_grupo, Me.txt_grupo, Me.tpo_grupo})
        Me.DTG_GRUPOS.Location = New System.Drawing.Point(16, 155)
        Me.DTG_GRUPOS.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DTG_GRUPOS.Name = "DTG_GRUPOS"
        Me.DTG_GRUPOS.Size = New System.Drawing.Size(1172, 550)
        Me.DTG_GRUPOS.TabIndex = 9
        '
        'cod_grupo
        '
        Me.cod_grupo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.cod_grupo.DataPropertyName = "cod_grupo"
        Me.cod_grupo.HeaderText = "CÓDIGO GRUPO"
        Me.cod_grupo.Name = "cod_grupo"
        Me.cod_grupo.ReadOnly = True
        Me.cod_grupo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cod_grupo.Width = 112
        '
        'txt_grupo
        '
        Me.txt_grupo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_grupo.DataPropertyName = "txt_grupo"
        Me.txt_grupo.HeaderText = "DESCRIÇÃO DO GRUPO"
        Me.txt_grupo.Name = "txt_grupo"
        Me.txt_grupo.ReadOnly = True
        Me.txt_grupo.Width = 133
        '
        'tpo_grupo
        '
        Me.tpo_grupo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.tpo_grupo.DataPropertyName = "tpo_grupo"
        Me.tpo_grupo.HeaderText = "TIPO DE GRUPO"
        Me.tpo_grupo.Name = "tpo_grupo"
        Me.tpo_grupo.ReadOnly = True
        Me.tpo_grupo.Width = 135
        '
        'FConsultaGrupos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1204, 718)
        Me.Controls.Add(Me.DTG_GRUPOS)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FConsultaGrupos"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FConsultaGrupos"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.DTG_GRUPOS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TXT_DESCRICAO As System.Windows.Forms.TextBox
    Friend WithEvents BUT_CONSULTA As System.Windows.Forms.Button
    Friend WithEvents BUT_NOVO_GRUPO As System.Windows.Forms.Button
    Friend WithEvents DTG_GRUPOS As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents RDO_TODOS As System.Windows.Forms.RadioButton
    Friend WithEvents RDO_SERVICO As System.Windows.Forms.RadioButton
    Friend WithEvents RDO_PRODUTO As System.Windows.Forms.RadioButton
    Friend WithEvents cod_grupo As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents txt_grupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpo_grupo As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
