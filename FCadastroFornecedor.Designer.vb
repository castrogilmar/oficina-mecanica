﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FCadastroFornecedor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FCadastroFornecedor))
        Me.TXT_NR_CEP = New System.Windows.Forms.MaskedTextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_referencia = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_complemento = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CBO_CIDADE = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TXT_COD_FORNECEDOR = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.CBO_UF = New System.Windows.Forms.ComboBox()
        Me.txt_bairro = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TXT_RAZAO_SOCIAL = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.BUT_TELEFONES = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TXT_FANTASIA = New System.Windows.Forms.TextBox()
        Me.txt_endereco = New System.Windows.Forms.TextBox()
        Me.BUT_DOCUMENTOS = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BUT_EXCLUIR = New System.Windows.Forms.Button()
        Me.BUT_SALVAR = New System.Windows.Forms.Button()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TXT_NR_CEP
        '
        Me.TXT_NR_CEP.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_NR_CEP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_NR_CEP.Location = New System.Drawing.Point(9, 43)
        Me.TXT_NR_CEP.Mask = "00.000-000"
        Me.TXT_NR_CEP.Name = "TXT_NR_CEP"
        Me.TXT_NR_CEP.Size = New System.Drawing.Size(88, 22)
        Me.TXT_NR_CEP.TabIndex = 8
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(6, 161)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(67, 15)
        Me.Label10.TabIndex = 50
        Me.Label10.Text = "Referência"
        '
        'txt_referencia
        '
        Me.txt_referencia.BackColor = System.Drawing.SystemColors.Info
        Me.txt_referencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_referencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_referencia.Location = New System.Drawing.Point(6, 179)
        Me.txt_referencia.Name = "txt_referencia"
        Me.txt_referencia.Size = New System.Drawing.Size(353, 22)
        Me.txt_referencia.TabIndex = 14
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(329, 114)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(85, 15)
        Me.Label9.TabIndex = 48
        Me.Label9.Text = "Complemento"
        '
        'txt_complemento
        '
        Me.txt_complemento.BackColor = System.Drawing.SystemColors.Info
        Me.txt_complemento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_complemento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_complemento.Location = New System.Drawing.Point(329, 132)
        Me.txt_complemento.Name = "txt_complemento"
        Me.txt_complemento.Size = New System.Drawing.Size(325, 22)
        Me.txt_complemento.TabIndex = 13
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(234, 25)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(46, 15)
        Me.Label8.TabIndex = 47
        Me.Label8.Text = "Cidade"
        '
        'CBO_CIDADE
        '
        Me.CBO_CIDADE.BackColor = System.Drawing.SystemColors.Info
        Me.CBO_CIDADE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBO_CIDADE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBO_CIDADE.FormattingEnabled = True
        Me.CBO_CIDADE.Location = New System.Drawing.Point(234, 43)
        Me.CBO_CIDADE.Name = "CBO_CIDADE"
        Me.CBO_CIDADE.Size = New System.Drawing.Size(423, 21)
        Me.CBO_CIDADE.TabIndex = 10
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 15)
        Me.Label1.TabIndex = 44
        Me.Label1.Text = "Código"
        '
        'TXT_COD_FORNECEDOR
        '
        Me.TXT_COD_FORNECEDOR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_COD_FORNECEDOR.Enabled = False
        Me.TXT_COD_FORNECEDOR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_COD_FORNECEDOR.Location = New System.Drawing.Point(6, 33)
        Me.TXT_COD_FORNECEDOR.Name = "TXT_COD_FORNECEDOR"
        Me.TXT_COD_FORNECEDOR.Size = New System.Drawing.Size(100, 22)
        Me.TXT_COD_FORNECEDOR.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(122, 27)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(23, 15)
        Me.Label7.TabIndex = 45
        Me.Label7.Text = "UF"
        '
        'CBO_UF
        '
        Me.CBO_UF.BackColor = System.Drawing.SystemColors.Info
        Me.CBO_UF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBO_UF.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBO_UF.FormattingEnabled = True
        Me.CBO_UF.Location = New System.Drawing.Point(125, 43)
        Me.CBO_UF.Name = "CBO_UF"
        Me.CBO_UF.Size = New System.Drawing.Size(89, 21)
        Me.CBO_UF.TabIndex = 9
        '
        'txt_bairro
        '
        Me.txt_bairro.BackColor = System.Drawing.SystemColors.Info
        Me.txt_bairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_bairro.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_bairro.Location = New System.Drawing.Point(6, 132)
        Me.txt_bairro.Name = "txt_bairro"
        Me.txt_bairro.Size = New System.Drawing.Size(274, 22)
        Me.txt_bairro.TabIndex = 12
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(9, 25)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 15)
        Me.Label5.TabIndex = 40
        Me.Label5.Text = "CEP"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 15)
        Me.Label2.TabIndex = 41
        Me.Label2.Text = "Razão Social"
        '
        'TXT_RAZAO_SOCIAL
        '
        Me.TXT_RAZAO_SOCIAL.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_RAZAO_SOCIAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_RAZAO_SOCIAL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_RAZAO_SOCIAL.Location = New System.Drawing.Point(6, 81)
        Me.TXT_RAZAO_SOCIAL.Name = "TXT_RAZAO_SOCIAL"
        Me.TXT_RAZAO_SOCIAL.Size = New System.Drawing.Size(642, 22)
        Me.TXT_RAZAO_SOCIAL.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 114)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 15)
        Me.Label6.TabIndex = 42
        Me.Label6.Text = "Bairro"
        '
        'BUT_TELEFONES
        '
        Me.BUT_TELEFONES.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_TELEFONES.Image = CType(resources.GetObject("BUT_TELEFONES.Image"), System.Drawing.Image)
        Me.BUT_TELEFONES.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_TELEFONES.Location = New System.Drawing.Point(459, 381)
        Me.BUT_TELEFONES.Name = "BUT_TELEFONES"
        Me.BUT_TELEFONES.Size = New System.Drawing.Size(91, 46)
        Me.BUT_TELEFONES.TabIndex = 19
        Me.BUT_TELEFONES.Text = "Telefones"
        Me.BUT_TELEFONES.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_TELEFONES.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 67)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 15)
        Me.Label3.TabIndex = 38
        Me.Label3.Text = "Endereço"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.TXT_FANTASIA)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.TXT_COD_FORNECEDOR)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.TXT_RAZAO_SOCIAL)
        Me.GroupBox2.Location = New System.Drawing.Point(2, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(674, 159)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 106)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 15)
        Me.Label4.TabIndex = 46
        Me.Label4.Text = "Fantasia"
        '
        'TXT_FANTASIA
        '
        Me.TXT_FANTASIA.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_FANTASIA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_FANTASIA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_FANTASIA.Location = New System.Drawing.Point(6, 124)
        Me.TXT_FANTASIA.Name = "TXT_FANTASIA"
        Me.TXT_FANTASIA.Size = New System.Drawing.Size(642, 22)
        Me.TXT_FANTASIA.TabIndex = 2
        '
        'txt_endereco
        '
        Me.txt_endereco.BackColor = System.Drawing.SystemColors.Info
        Me.txt_endereco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_endereco.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_endereco.Location = New System.Drawing.Point(6, 85)
        Me.txt_endereco.Name = "txt_endereco"
        Me.txt_endereco.Size = New System.Drawing.Size(648, 22)
        Me.txt_endereco.TabIndex = 11
        '
        'BUT_DOCUMENTOS
        '
        Me.BUT_DOCUMENTOS.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_DOCUMENTOS.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_DOCUMENTOS.Location = New System.Drawing.Point(348, 381)
        Me.BUT_DOCUMENTOS.Name = "BUT_DOCUMENTOS"
        Me.BUT_DOCUMENTOS.Size = New System.Drawing.Size(105, 46)
        Me.BUT_DOCUMENTOS.TabIndex = 18
        Me.BUT_DOCUMENTOS.Text = "Documentos"
        Me.BUT_DOCUMENTOS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_DOCUMENTOS.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TXT_NR_CEP)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txt_referencia)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txt_complemento)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.CBO_CIDADE)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.CBO_UF)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txt_bairro)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txt_endereco)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 165)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(674, 210)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'BUT_EXCLUIR
        '
        Me.BUT_EXCLUIR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_EXCLUIR.Image = CType(resources.GetObject("BUT_EXCLUIR.Image"), System.Drawing.Image)
        Me.BUT_EXCLUIR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_EXCLUIR.Location = New System.Drawing.Point(240, 381)
        Me.BUT_EXCLUIR.Name = "BUT_EXCLUIR"
        Me.BUT_EXCLUIR.Size = New System.Drawing.Size(91, 46)
        Me.BUT_EXCLUIR.TabIndex = 22
        Me.BUT_EXCLUIR.Text = "EXCLUIR"
        Me.BUT_EXCLUIR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_EXCLUIR.UseVisualStyleBackColor = True
        '
        'BUT_SALVAR
        '
        Me.BUT_SALVAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_SALVAR.Image = Global.oficina_mecanica.My.Resources.Resources.Hopstarter_Sleek_Xp_Basic_Ok
        Me.BUT_SALVAR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_SALVAR.Location = New System.Drawing.Point(146, 381)
        Me.BUT_SALVAR.Name = "BUT_SALVAR"
        Me.BUT_SALVAR.Size = New System.Drawing.Size(88, 46)
        Me.BUT_SALVAR.TabIndex = 21
        Me.BUT_SALVAR.Text = "SALVAR"
        Me.BUT_SALVAR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_SALVAR.UseVisualStyleBackColor = True
        '
        'FCadastroFornecedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(687, 439)
        Me.Controls.Add(Me.BUT_TELEFONES)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.BUT_DOCUMENTOS)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.BUT_EXCLUIR)
        Me.Controls.Add(Me.BUT_SALVAR)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FCadastroFornecedor"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FCadastroFornecedor"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TXT_NR_CEP As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txt_referencia As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_complemento As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents CBO_CIDADE As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TXT_COD_FORNECEDOR As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents CBO_UF As System.Windows.Forms.ComboBox
    Friend WithEvents txt_bairro As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TXT_RAZAO_SOCIAL As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents BUT_TELEFONES As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TXT_FANTASIA As System.Windows.Forms.TextBox
    Friend WithEvents txt_endereco As System.Windows.Forms.TextBox
    Friend WithEvents BUT_DOCUMENTOS As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BUT_EXCLUIR As System.Windows.Forms.Button
    Friend WithEvents BUT_SALVAR As System.Windows.Forms.Button
End Class
