﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FConsultaTipoDocumento
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TXT_DESCRICAO = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BUT_CONSULTA = New System.Windows.Forms.Button()
        Me.BUT_NOVO = New System.Windows.Forms.Button()
        Me.DTG_TIPOS_DOCUMENTOS = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CBO_TIPO_CLIENTE = New System.Windows.Forms.ComboBox()
        Me.cod_documento = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.txt_documento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpo_cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DTG_TIPOS_DOCUMENTOS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TXT_DESCRICAO
        '
        Me.TXT_DESCRICAO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_DESCRICAO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_DESCRICAO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_DESCRICAO.Location = New System.Drawing.Point(9, 71)
        Me.TXT_DESCRICAO.Name = "TXT_DESCRICAO"
        Me.TXT_DESCRICAO.Size = New System.Drawing.Size(607, 22)
        Me.TXT_DESCRICAO.TabIndex = 4
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.CBO_TIPO_CLIENTE)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TXT_DESCRICAO)
        Me.GroupBox1.Controls.Add(Me.BUT_CONSULTA)
        Me.GroupBox1.Controls.Add(Me.BUT_NOVO)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(882, 110)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(144, 15)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Descrição do documento"
        '
        'BUT_CONSULTA
        '
        Me.BUT_CONSULTA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_CONSULTA.Image = Global.oficina_mecanica.My.Resources.Resources.Visualpharm_Must_Have_Search
        Me.BUT_CONSULTA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_CONSULTA.Location = New System.Drawing.Point(763, 22)
        Me.BUT_CONSULTA.Name = "BUT_CONSULTA"
        Me.BUT_CONSULTA.Size = New System.Drawing.Size(106, 46)
        Me.BUT_CONSULTA.TabIndex = 6
        Me.BUT_CONSULTA.Text = "CONSULTAR"
        Me.BUT_CONSULTA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_CONSULTA.UseVisualStyleBackColor = True
        '
        'BUT_NOVO
        '
        Me.BUT_NOVO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_NOVO.Image = Global.oficina_mecanica.My.Resources.Resources.Visualpharm_Must_Have_Add
        Me.BUT_NOVO.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_NOVO.Location = New System.Drawing.Point(677, 22)
        Me.BUT_NOVO.Name = "BUT_NOVO"
        Me.BUT_NOVO.Size = New System.Drawing.Size(80, 46)
        Me.BUT_NOVO.TabIndex = 5
        Me.BUT_NOVO.Text = "NOVO"
        Me.BUT_NOVO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_NOVO.UseVisualStyleBackColor = True
        '
        'DTG_TIPOS_DOCUMENTOS
        '
        Me.DTG_TIPOS_DOCUMENTOS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DTG_TIPOS_DOCUMENTOS.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cod_documento, Me.txt_documento, Me.tpo_cliente})
        Me.DTG_TIPOS_DOCUMENTOS.Location = New System.Drawing.Point(12, 128)
        Me.DTG_TIPOS_DOCUMENTOS.Name = "DTG_TIPOS_DOCUMENTOS"
        Me.DTG_TIPOS_DOCUMENTOS.Size = New System.Drawing.Size(879, 418)
        Me.DTG_TIPOS_DOCUMENTOS.TabIndex = 13
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(89, 15)
        Me.Label4.TabIndex = 48
        Me.Label4.Text = "Tipo de Cliente"
        '
        'CBO_TIPO_CLIENTE
        '
        Me.CBO_TIPO_CLIENTE.BackColor = System.Drawing.SystemColors.Info
        Me.CBO_TIPO_CLIENTE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBO_TIPO_CLIENTE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBO_TIPO_CLIENTE.FormattingEnabled = True
        Me.CBO_TIPO_CLIENTE.Location = New System.Drawing.Point(9, 29)
        Me.CBO_TIPO_CLIENTE.Name = "CBO_TIPO_CLIENTE"
        Me.CBO_TIPO_CLIENTE.Size = New System.Drawing.Size(186, 21)
        Me.CBO_TIPO_CLIENTE.TabIndex = 47
        '
        'cod_documento
        '
        Me.cod_documento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.cod_documento.DataPropertyName = "cod_documento"
        Me.cod_documento.HeaderText = "CÓDIGO"
        Me.cod_documento.Name = "cod_documento"
        Me.cod_documento.ReadOnly = True
        Me.cod_documento.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cod_documento.Width = 55
        '
        'txt_documento
        '
        Me.txt_documento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_documento.DataPropertyName = "txt_documento"
        Me.txt_documento.HeaderText = "DESCRIÇÃO"
        Me.txt_documento.Name = "txt_documento"
        Me.txt_documento.ReadOnly = True
        Me.txt_documento.Width = 94
        '
        'tpo_cliente
        '
        Me.tpo_cliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.tpo_cliente.DataPropertyName = "tpo_cliente"
        Me.tpo_cliente.HeaderText = "TPO CLIENTE"
        Me.tpo_cliente.Name = "tpo_cliente"
        Me.tpo_cliente.Width = 102
        '
        'FConsultaTipoDocumento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(902, 557)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.DTG_TIPOS_DOCUMENTOS)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FConsultaTipoDocumento"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FConsultaTipoDocumentovb"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DTG_TIPOS_DOCUMENTOS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TXT_DESCRICAO As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents BUT_CONSULTA As System.Windows.Forms.Button
    Friend WithEvents BUT_NOVO As System.Windows.Forms.Button
    Friend WithEvents DTG_TIPOS_DOCUMENTOS As System.Windows.Forms.DataGridView
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents CBO_TIPO_CLIENTE As System.Windows.Forms.ComboBox
    Friend WithEvents cod_documento As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents txt_documento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpo_cliente As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
