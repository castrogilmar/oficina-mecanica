﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FConsultaTipoOrdem
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FConsultaTipoOrdem))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TXT_DESCRICAO = New System.Windows.Forms.TextBox()
        Me.BUT_CONSULTA = New System.Windows.Forms.Button()
        Me.BUT_NOVO = New System.Windows.Forms.Button()
        Me.DTG_TIPOS_CLIENTE = New System.Windows.Forms.DataGridView()
        Me.cod_tipo = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.txt_tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DTG_TIPOS_CLIENTE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TXT_DESCRICAO)
        Me.GroupBox1.Controls.Add(Me.BUT_CONSULTA)
        Me.GroupBox1.Controls.Add(Me.BUT_NOVO)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 6)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1176, 100)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 20)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(130, 18)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Descrição do Tipo"
        '
        'TXT_DESCRICAO
        '
        Me.TXT_DESCRICAO.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_DESCRICAO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_DESCRICAO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_DESCRICAO.Location = New System.Drawing.Point(12, 42)
        Me.TXT_DESCRICAO.Margin = New System.Windows.Forms.Padding(4)
        Me.TXT_DESCRICAO.Name = "TXT_DESCRICAO"
        Me.TXT_DESCRICAO.Size = New System.Drawing.Size(808, 26)
        Me.TXT_DESCRICAO.TabIndex = 4
        '
        'BUT_CONSULTA
        '
        Me.BUT_CONSULTA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_CONSULTA.Image = Global.oficina_mecanica.My.Resources.Resources.Visualpharm_Must_Have_Search
        Me.BUT_CONSULTA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_CONSULTA.Location = New System.Drawing.Point(1017, 27)
        Me.BUT_CONSULTA.Margin = New System.Windows.Forms.Padding(4)
        Me.BUT_CONSULTA.Name = "BUT_CONSULTA"
        Me.BUT_CONSULTA.Size = New System.Drawing.Size(141, 57)
        Me.BUT_CONSULTA.TabIndex = 6
        Me.BUT_CONSULTA.Text = "CONSULTAR"
        Me.BUT_CONSULTA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_CONSULTA.UseVisualStyleBackColor = True
        '
        'BUT_NOVO
        '
        Me.BUT_NOVO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_NOVO.Image = Global.oficina_mecanica.My.Resources.Resources.Visualpharm_Must_Have_Add
        Me.BUT_NOVO.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_NOVO.Location = New System.Drawing.Point(849, 27)
        Me.BUT_NOVO.Margin = New System.Windows.Forms.Padding(4)
        Me.BUT_NOVO.Name = "BUT_NOVO"
        Me.BUT_NOVO.Size = New System.Drawing.Size(160, 57)
        Me.BUT_NOVO.TabIndex = 5
        Me.BUT_NOVO.Text = "NOVO TIPO"
        Me.BUT_NOVO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_NOVO.UseVisualStyleBackColor = True
        '
        'DTG_TIPOS_CLIENTE
        '
        Me.DTG_TIPOS_CLIENTE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DTG_TIPOS_CLIENTE.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cod_tipo, Me.txt_tipo})
        Me.DTG_TIPOS_CLIENTE.Location = New System.Drawing.Point(3, 113)
        Me.DTG_TIPOS_CLIENTE.Margin = New System.Windows.Forms.Padding(4)
        Me.DTG_TIPOS_CLIENTE.Name = "DTG_TIPOS_CLIENTE"
        Me.DTG_TIPOS_CLIENTE.Size = New System.Drawing.Size(1172, 550)
        Me.DTG_TIPOS_CLIENTE.TabIndex = 13
        '
        'cod_tipo
        '
        Me.cod_tipo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.cod_tipo.DataPropertyName = "cod_tipo"
        Me.cod_tipo.HeaderText = "CÓDIGO"
        Me.cod_tipo.Name = "cod_tipo"
        Me.cod_tipo.ReadOnly = True
        Me.cod_tipo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cod_tipo.Width = 69
        '
        'txt_tipo
        '
        Me.txt_tipo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_tipo.DataPropertyName = "txt_tipo"
        Me.txt_tipo.HeaderText = "DESCRIÇÃO"
        Me.txt_tipo.Name = "txt_tipo"
        Me.txt_tipo.ReadOnly = True
        Me.txt_tipo.Width = 116
        '
        'FConsultaTipoOrdem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1179, 669)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.DTG_TIPOS_CLIENTE)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FConsultaTipoOrdem"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FConsultaTipoOrdem"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DTG_TIPOS_CLIENTE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TXT_DESCRICAO As TextBox
    Friend WithEvents BUT_CONSULTA As Button
    Friend WithEvents BUT_NOVO As Button
    Friend WithEvents DTG_TIPOS_CLIENTE As DataGridView
    Friend WithEvents cod_tipo As DataGridViewButtonColumn
    Friend WithEvents txt_tipo As DataGridViewTextBoxColumn
End Class
