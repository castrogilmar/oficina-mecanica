﻿Imports Npgsql
Imports System.Data

Public Class FCadastroTipoCliente
    Private Sub ManutencaoTpoCliente()
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction
        Dim vL_DataRead As NpgsqlDataReader

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            With vL_Cmd
                .Connection = vL_Conn
                .CommandType = CommandType.Text
                .CommandText = "select cod_tipo from oficina.tb_tipo_cliente where txt_tipo = '" + TXT_DESCRICAO.Text.Trim + "'"
                vL_DataRead = .ExecuteReader
            End With

            If vL_DataRead.HasRows Then
                MessageBox.Show("O tipo informado já foi cadastrado anteriormente!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                vL_Conn.Close()
                Exit Sub
            End If

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = IIf(TXT_COD_TIPO.Text <> Nothing, "update oficina.tb_tipo_cliente set txt_tipo ='" + TXT_DESCRICAO.Text.Trim + _
                                       "' where cod_tipo=" + TXT_COD_TIPO.Text.Trim + " RETURNING cod_tipo", _
                                       "insert into oficina.tb_tipo_cliente (txt_tipo) values('" + TXT_DESCRICAO.Text.Trim + "') " + _
                                       " RETURNING cod_tipo")
                    vL_DataRead = .ExecuteReader()
                End With

                vL_Trans.Commit()
                vL_DataRead.Read()

                MessageBox.Show("Manutenção realizada com sucesso. Código do tipo: " + vL_DataRead.Item("cod_tipo").ToString.Trim, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Catch ex As Exception
                vL_Trans.Rollback()
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Sub

    Private Function ExclusaoTpoCliente() As Boolean
        Dim vL_Conn As New NpgsqlConnection()
        Dim vL_Cmd As New NpgsqlCommand
        Dim vL_Trans As NpgsqlTransaction

        Try
            With vL_Conn
                .ConnectionString = vG_StrConn
                .Open()
                vL_Trans = .BeginTransaction
            End With

            Try
                With vL_Cmd
                    .Connection = vL_Conn
                    .Transaction = vL_Trans
                    .CommandText = "delete from oficina.tb_tipo_cliente where cod_tipo=" + TXT_COD_TIPO.Text.Trim + "::integer"
                    .ExecuteNonQuery()
                End With

                vL_Trans.Commit()

                MessageBox.Show("Tipo excluído com sucesso!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                ExclusaoTpoCliente = True
            Catch ex As Exception
                vL_Trans.Rollback()
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                ExclusaoTpoCliente = False
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            ExclusaoTpoCliente = False
        Finally
            If Not vL_Conn.State = ConnectionState.Closed Then
                vL_Conn.Close()
            End If
        End Try
    End Function

    Private Sub FCadastroTipoCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = vG_Rotulo + " - CADASTRO DE TIPOS DE CLIENTES"

        If TXT_COD_TIPO.Text = Nothing Then
            BUT_EXCLUIR.Enabled = False
        End If
    End Sub

    Private Sub BUT_SALVAR_Click(sender As Object, e As EventArgs) Handles BUT_SALVAR.Click
        If TXT_DESCRICAO.Text = Nothing Then
            MessageBox.Show("Descrição do tipo de cliente não informada!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        ManutencaoTpoCliente()
    End Sub

    Private Sub BUT_EXCLUIR_Click(sender As Object, e As EventArgs) Handles BUT_EXCLUIR.Click
        If MessageBox.Show("Deseja realmente excluir o tipo de cliente?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        If ExclusaoTpoCliente() Then
            Me.Close()
        End If
    End Sub
End Class