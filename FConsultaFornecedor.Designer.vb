﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FConsultaFornecedor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TXT_NOME = New System.Windows.Forms.TextBox()
        Me.BUT_NOVO = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BUT_CONSULTA = New System.Windows.Forms.Button()
        Me.DTG_FORNECEDORES = New System.Windows.Forms.DataGridView()
        Me.cod_fornecedor = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.txt_fantasia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txt_razao_social = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nr_cep = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sg_uf = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txt_cidade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txt_endereco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txt_bairro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txt_complemento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txt_referencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DTG_FORNECEDORES, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 15)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Nome Fantasia"
        '
        'TXT_NOME
        '
        Me.TXT_NOME.BackColor = System.Drawing.SystemColors.Info
        Me.TXT_NOME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TXT_NOME.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXT_NOME.Location = New System.Drawing.Point(9, 34)
        Me.TXT_NOME.Name = "TXT_NOME"
        Me.TXT_NOME.Size = New System.Drawing.Size(607, 22)
        Me.TXT_NOME.TabIndex = 4
        '
        'BUT_NOVO
        '
        Me.BUT_NOVO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_NOVO.Image = Global.oficina_mecanica.My.Resources.Resources.Visualpharm_Must_Have_Add
        Me.BUT_NOVO.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_NOVO.Location = New System.Drawing.Point(933, 22)
        Me.BUT_NOVO.Name = "BUT_NOVO"
        Me.BUT_NOVO.Size = New System.Drawing.Size(80, 46)
        Me.BUT_NOVO.TabIndex = 5
        Me.BUT_NOVO.Text = "NOVO"
        Me.BUT_NOVO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_NOVO.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TXT_NOME)
        Me.GroupBox1.Controls.Add(Me.BUT_CONSULTA)
        Me.GroupBox1.Controls.Add(Me.BUT_NOVO)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1230, 80)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        '
        'BUT_CONSULTA
        '
        Me.BUT_CONSULTA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BUT_CONSULTA.Image = Global.oficina_mecanica.My.Resources.Resources.Visualpharm_Must_Have_Search
        Me.BUT_CONSULTA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BUT_CONSULTA.Location = New System.Drawing.Point(1019, 22)
        Me.BUT_CONSULTA.Name = "BUT_CONSULTA"
        Me.BUT_CONSULTA.Size = New System.Drawing.Size(106, 46)
        Me.BUT_CONSULTA.TabIndex = 6
        Me.BUT_CONSULTA.Text = "CONSULTAR"
        Me.BUT_CONSULTA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BUT_CONSULTA.UseVisualStyleBackColor = True
        '
        'DTG_FORNECEDORES
        '
        Me.DTG_FORNECEDORES.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DTG_FORNECEDORES.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cod_fornecedor, Me.txt_fantasia, Me.txt_razao_social, Me.nr_cep, Me.sg_uf, Me.txt_cidade, Me.txt_endereco, Me.txt_bairro, Me.txt_complemento, Me.txt_referencia})
        Me.DTG_FORNECEDORES.Location = New System.Drawing.Point(12, 92)
        Me.DTG_FORNECEDORES.Name = "DTG_FORNECEDORES"
        Me.DTG_FORNECEDORES.Size = New System.Drawing.Size(1230, 488)
        Me.DTG_FORNECEDORES.TabIndex = 17
        '
        'cod_fornecedor
        '
        Me.cod_fornecedor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.cod_fornecedor.DataPropertyName = "cod_fornecedor"
        Me.cod_fornecedor.HeaderText = "CÓDIGO"
        Me.cod_fornecedor.Name = "cod_fornecedor"
        Me.cod_fornecedor.ReadOnly = True
        Me.cod_fornecedor.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cod_fornecedor.Width = 55
        '
        'txt_fantasia
        '
        Me.txt_fantasia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_fantasia.DataPropertyName = "txt_fantasia"
        Me.txt_fantasia.HeaderText = "NOME FANTASIA"
        Me.txt_fantasia.Name = "txt_fantasia"
        Me.txt_fantasia.ReadOnly = True
        Me.txt_fantasia.Width = 109
        '
        'txt_razao_social
        '
        Me.txt_razao_social.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_razao_social.DataPropertyName = "txt_razao_social"
        Me.txt_razao_social.HeaderText = "RAZÃO SOCIAL"
        Me.txt_razao_social.Name = "txt_razao_social"
        Me.txt_razao_social.ReadOnly = True
        Me.txt_razao_social.Width = 101
        '
        'nr_cep
        '
        Me.nr_cep.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.nr_cep.DataPropertyName = "nr_cep"
        Me.nr_cep.HeaderText = "CEP"
        Me.nr_cep.Name = "nr_cep"
        Me.nr_cep.ReadOnly = True
        Me.nr_cep.Width = 53
        '
        'sg_uf
        '
        Me.sg_uf.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.sg_uf.DataPropertyName = "sg_uf"
        Me.sg_uf.HeaderText = "UF"
        Me.sg_uf.Name = "sg_uf"
        Me.sg_uf.ReadOnly = True
        Me.sg_uf.Width = 46
        '
        'txt_cidade
        '
        Me.txt_cidade.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_cidade.DataPropertyName = "txt_cidade"
        Me.txt_cidade.HeaderText = "CIDADE"
        Me.txt_cidade.Name = "txt_cidade"
        Me.txt_cidade.ReadOnly = True
        Me.txt_cidade.Width = 72
        '
        'txt_endereco
        '
        Me.txt_endereco.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_endereco.DataPropertyName = "txt_endereco"
        Me.txt_endereco.HeaderText = "ENDEREÇO"
        Me.txt_endereco.Name = "txt_endereco"
        Me.txt_endereco.ReadOnly = True
        Me.txt_endereco.Width = 92
        '
        'txt_bairro
        '
        Me.txt_bairro.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_bairro.DataPropertyName = "txt_bairro"
        Me.txt_bairro.HeaderText = "BAIRRO"
        Me.txt_bairro.Name = "txt_bairro"
        Me.txt_bairro.ReadOnly = True
        Me.txt_bairro.Width = 73
        '
        'txt_complemento
        '
        Me.txt_complemento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_complemento.DataPropertyName = "txt_complemento"
        Me.txt_complemento.HeaderText = "COMPLEMENTO"
        Me.txt_complemento.Name = "txt_complemento"
        Me.txt_complemento.ReadOnly = True
        Me.txt_complemento.Width = 115
        '
        'txt_referencia
        '
        Me.txt_referencia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.txt_referencia.DataPropertyName = "txt_referencia"
        Me.txt_referencia.HeaderText = "PONTO DE REFERÊNCIA"
        Me.txt_referencia.Name = "txt_referencia"
        Me.txt_referencia.ReadOnly = True
        Me.txt_referencia.Width = 145
        '
        'FConsultaFornecedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1254, 589)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.DTG_FORNECEDORES)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FConsultaFornecedor"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FConsultaFornecedor"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DTG_FORNECEDORES, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TXT_NOME As System.Windows.Forms.TextBox
    Friend WithEvents BUT_NOVO As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BUT_CONSULTA As System.Windows.Forms.Button
    Friend WithEvents DTG_FORNECEDORES As System.Windows.Forms.DataGridView
    Friend WithEvents cod_fornecedor As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents txt_fantasia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txt_razao_social As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nr_cep As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sg_uf As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txt_cidade As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txt_endereco As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txt_bairro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txt_complemento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txt_referencia As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
